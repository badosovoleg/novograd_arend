﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.IO;
using NovogradArend.WordWorker.Interfaces;

namespace NovogradArend.WordWorker
{
    public class WordWoker
    {
        public void OpenAndModify(string path, IEnumerable<IReplacableBookmark> bookmarks)
        {
            Application application;
            Document document = null;
            object _missingObj = System.Reflection.Missing.Value;
            object trueObj = true;
            object falseObj = false;

            //создаем обьект приложения word
            application = new ApplicationClass();

            if (!File.Exists(path))
                return;

            application.Visible = true;

            // если вылетим не этом этапе, приложение останется открытым
            try
            {
                document = application.Documents.Add(path, ref _missingObj, ref _missingObj, ref _missingObj);
            }
            catch (Exception error)
            {
                if (document != null)
                {
                    document.Close(ref falseObj, ref _missingObj, ref _missingObj);
                    application.Quit(ref _missingObj, ref _missingObj, ref _missingObj);
                }

                document = null;
                application = null;
                throw error;
            }

            document.SetTextByBookmarkName(bookmarks);
        }
    }
}
