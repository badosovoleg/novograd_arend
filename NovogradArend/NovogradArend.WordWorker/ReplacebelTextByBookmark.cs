﻿using System.Collections.Generic;
using NovogradArend.WordWorker.Interfaces;

namespace NovogradArend.WordWorker
{
    public class ReplacebelTextByBookmark : IReplacableBookmark
    {
        public string Bookmark { get; set; }
        public string Text;

        public ReplacebelTextByBookmark(string bookmark, string text)
        {
            Bookmark = bookmark;
            Text = text;
        }

        public ReplacebelTextByBookmark(string bookmark, int digit)
        {
            Bookmark = bookmark;
            Text = digit.ToString();
        }

        public ReplacebelTextByBookmark(string bookmark, decimal digit)
        {
            Bookmark = bookmark;
            Text = digit.ToString();
        }
    }

    public static class ReplacebelBookmarkCreator
    {
        public static IEnumerable<IReplacableBookmark> GetManyBookmarks(string value, params string[] bookmarks)
        {
            foreach(var bookmark in bookmarks)
            {
                yield return new ReplacebelTextByBookmark(bookmark, value);
            }
        }
    }
}
