﻿namespace NovogradArend.WordWorker.Interfaces
{
    public interface IReplacableBookmark
    {
        string Bookmark { get; set; }
    }
}
