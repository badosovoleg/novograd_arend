﻿namespace NovogradArend.WordWorker
{
    public sealed class WordBookmarks
    {
        public const string NumberBookmark = "number";

        public const string Number2Bookmark = "number2";

        public const string Number3Bookmark = "number3";

        public const string CountLandlordDayBookmark = "CountLandlordDay";
        public const string CountLandlordDayBookmark2 = "CountLandlordDay2";

        public const string CountRenterDayBookmark = "CountRenterDay";

        public const string CountRenterDay2Bookmark = "CountRenterDay2";

        public const string BeginDateBookmark = "beginDate";

        public const string BeginDate2Bookmark = "beginDate2";

        public const string BeginDate3Bookmark = "beginDate3";

        public const string LandlordNameBookmark = "landlordName";

        public const string LandlordRepresentedBookmark = "landlordRepresented";

        public const string LandlordAgreementBookmark = "landlordAgreement";

        public const string RenterNameBookmark = "renterName";

        public const string RenterRepresentedBookmark = "renterRepresented";

        public const string RenterAgreementBookmark = "renterAgreement";

        public const string BuildingAddressBookmark = "buildingAddress";

        public const string OfficeFloorBookmark = "officeFloor";

        public const string OfficeNumberOnFloorPlanBookmark = "officeNumberOnFloorPlan";

        public const string OfficeNumberBookmark = "officeNumber";

        public const string OfficeSquareBookmark = "officeSquare";

        public const string OfficeForPlacementBookmark = "officeForPlacement";

        public const string OfficeOwnershipBookmark = "officeOwnership";

        public const string OfficePriceBookmark = "officePrice";

        public const string TotalPriceBookmark = "totalPrice";

        public const string LandlordRequisitesBookmark = "landlordRequisites";

        public const string LandlordRequisitesEndBookmark = "landlordRequisitesEnd";

        public const string RenterRequisitesBookmark = "renterRequisites";

        public const string RenterRequisitesEndBookmark = "renterRequisitesEnd";

        public const string Act1BeginDateBookmark = "act1Date";

        public const string Act2BeginDateBookmark = "act2Date";

        public const string GaranteeBookmark = "garantee";

        public const string AdditinalDocumentHeaderBookmark = "additinalDocumentHeader";

        public const string LandlordAdditinalDocumentRequisitesBookmark = "landlordAdditinalDocumentRequisites";

        public const string LandlordAdditinalDocumentRequisitesEndBookmark = "landlordAdditinalDocumentRequisitesEnd";

        public const string RenterAdditinalDocumentRequisitesBookmark = "renterAdditinalDocumentRequisites";

        public const string RenterAdditinalDocumentRequisitesEndBookmark = "renterAdditinalDocumentRequisitesEnd";

        public const string ImagePlaceBookmark = "imagePlace";

        public const string LeaseContractEndDateBookmark = "leaseContractEndDate";

        public const string ChangeArendPriceNumberBookmark = "changeArendPriceNumber";

        public const string ChangeArendPriceNumber2Bookmark = "changeArendPriceNumber2";

        public const string ChangeArendPriceDateBookmark = "changeArendPriceDate";

        public const string TerminationDateBookmark = "TerminationDate";

        public const string CancelContractDateBookmark = "cancelContractDate";

        public const string MailNumberBookmark = "mailNumber";

        public const string MailDateBookmark = "mailDate";

        public const string LandlordSomeShortRequisitesBookmark = "landlordSomeShortRequisites";

        public const string RenterSomeShortRequisitesBookmark = "renterSomeShortRequisites";

        public const string UtilityServiceContractDateBookmark = "utilityServiceContractDate";

        public const string ManagerCompanyNameBookmark = "managerCompanyName";

        public const string ManagerCompanyRepresentedBookmark = "managerCompanyRepresented";

        public const string ManagerCompanyAgreementBookmark = "managerCompanyAgreement";

        public const string UtilityServiceContractTotalPriceBookmark = "utilityServiceContractTotalPrice";

        public const string ManagerCompanyRequisitesBookmark = "managerCompanyRequisites";

        public const string ManagerCompanyRequisitesEndBookmark = "managerCompanyRequisitesEnd";

        public const string leaseContractBeginDateBookmark = "leaseContractBeginDate";

        public const string leaseContractNumberBookmark = "leaseContractNumber";

        public const string utilityServiceNameBookmark = "utilityServiceName";

        public const string utilityServiceContractNumberBookmark = "utilityServiceContractNumber";

        public const string utilityServiceContractBeginDateBookmark = "utilityServiceContractBeginDate";

        public const string totalSquareBookmark = "totalSquare";

        public const string buildingNameBookmark = "buildingName";

        public const string buildingAdressBookmark = "buildingAdress";

        public const string numberAct1Bookmark = "numberAct1";

        public const string beginDateAct1Bookmark = "beginDateAct1";

        public const string leaseContractPriceBookmark = "leaseContractPrice";

        public const string leaseContracttotalPriceBookmark = "leaseContracttotalPrice";

        public const string utilityServiceContractPriceBookmark = "utilityServiceContractPrice";

        public const string arendaDebtBookmark = "arendaDebt";

        public const string utilityServiceDebtBookmark = "utilityServiceDebt";

        public const string ClaimDate2Bookmark = "ClaimDate2";

        public const string utilityServiceName2Bookmark = "utilityServiceName2";

        public const string landlordName2Bookmark = "landlordName2";

        public const string claimDate1Bookmark = "claimDate1";

        public const string beginDateTopBookmark = "beginDateTop";

    }
}
