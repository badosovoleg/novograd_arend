﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using NovogradArend.WordWorker.Interfaces;

namespace NovogradArend.WordWorker
{
    internal static class WordDocumentExtensions
    {
        private static void SetTextByBookmarkName(this Document document, string bookmarkName, string text) 
            => document.Bookmarks.get_Item(bookmarkName).Range.Text = text;

        private static void SetImageByBookmarkName(this Document document, string bookmarkName, string imagePath) 
            => document.Bookmarks.get_Item(bookmarkName).Range.InlineShapes.AddPicture(imagePath);

        public static void SetTextByBookmarkName(this Document document, IEnumerable<IReplacableBookmark> items)
        {
            foreach (var item in items)
            {
                try
                {
                    if (item is ReplacebelImageByBookmark)
                    {
                        var images = ((ReplacebelImageByBookmark)item).Images;
                        foreach (var image in images)
                        {
                            document.SetImageByBookmarkName(item.Bookmark, image);
                        }
                    }
                    if (item is ReplacebelTextByBookmark)
                        document.SetTextByBookmarkName(item.Bookmark, ((ReplacebelTextByBookmark)item).Text);
                }
                catch (Exception ex)
                {
                    throw new Exception($"Не удалось обработать данные по закладке '{item.Bookmark}'", ex);
                }
            }
        }
    }
}
