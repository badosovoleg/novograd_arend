﻿using NovogradArend.WordWorker.Interfaces;

namespace NovogradArend.WordWorker
{
    public class ReplacebelImageByBookmark : IReplacableBookmark
    {
        public string Bookmark { get; set; }
        public string[] Images { get; private set; }

        public ReplacebelImageByBookmark(string bookmark, params string[] images)
        {
            Bookmark = bookmark;
            Images = images;
        }
    }
}
