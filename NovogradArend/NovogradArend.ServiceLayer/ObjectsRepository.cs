﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using NovogradArend.EF;
using NovogradArend.Model;
using NovogradArend.Model.Services;

namespace NovogradArend.ServiceLayer
{
    public class ObjectsRepository : IObjectsRepository
    {
        public SQLiteDatabaseContext _context;

        public ObjectsRepository(string connectionString)
        {
            var optionsBuilder = new DbContextOptionsBuilder<SQLiteDatabaseContext>();
            optionsBuilder.UseSqlite(connectionString);
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            _context = new SQLiteDatabaseContext(optionsBuilder.Options);
        }

        public Task<int> AddAct1ContractAsync(Act1Contract contract) => _context.AddAct1ContractAsync(contract);

        public Task<int> AddAct2ContractAsync(Act2Contract contract) => _context.AddAct2ContractAsync(contract);

        public Task<int> AddCancelContractAsync(CancelContract contract) => _context.AddCancelContractAsync(contract);

        public Task<int> AddChangeArendPriceContractAsync(ChangeArendPriceContract contract) => _context.AddChangeArendPriceContractAsync(contract);

        public Task<int> AddClaimContractAsync(ClaimContract contract) => _context.AddClaimContractAsync(contract);

        public Task<int> AddEntityAsync(Entity entity) => _context.AddEntityAsync(entity);

        public Task<int> AddLeaseContractAsync(LeaseContract contract) => _context.AddLeaseContractAsync(contract);

        public Task<int> AddLeaseContractOfficeAsync(LeaseContractOffice contractOffices) => _context.AddLeaseContractOfficeAsync(contractOffices);

        public Task<int> AddTerminationContractAsync(TerminationContract contract) => _context.AddTerminationContractAsync(contract);

        public Task<int> AddUtilityServiceContractAsync(UtilityServiceContract contract) => _context.AddUtilityServiceContractAsync(contract);

        public Task<int> AddUtilityServiceContractOfficeAsync(UtilityServiceContractOffice contractOffices) => _context.AddUtilityServiceContractOfficeAsync(contractOffices);

        public Task<int> EditAct1ContractAsync(Act1Contract contract) => _context.EditAct1ContractAsync(contract);

        public Task<int> EditAct2ContractAsync(Act2Contract contract) => _context.EditAct2ContractAsync(contract);

        public Task<int> EditCancelContractAsync(CancelContract contract) => _context.EditCancelContractAsync(contract);

        public Task<int> EditChangeArendPriceContractAsync(ChangeArendPriceContract contract) => _context.EditChangeArendPriceContractAsync(contract);

        public Task<int> EditClaimContractAsync(ClaimContract contract) => _context.EditClaimContractAsync(contract);

        public Task<int> EditEntityAsync(Entity entity) => _context.EditEntityAsync(entity);

        public Task<int> EditLeaseContractAsync(LeaseContract contract) => _context.EditLeaseContractAsync(contract);

        public Task<int> RemoveLeaseContractOfficeAsync(LeaseContractOffice contractOffices) 
            => _context.RemoveLeaseContractOfficeAsync(contractOffices);

        public Task<int> EditTerminationContractAsync(TerminationContract contract) => _context.EditTerminationContractAsync(contract);

        public Task<int> EditUtilityServiceContractAsync(UtilityServiceContract contract) => _context.EditUtilityServiceContractAsync(contract);

        public Task<int> RemoveUtilityServiceContractOfficeAsync(UtilityServiceContractOffice contractOffices) => _context.RemoveUtilityServiceContractOfficeAsync(contractOffices);

        public Task<List<Act1Contract>> GetAct1ContractsAsync() => _context.GetAct1ContractsAsync();

        public Task<List<Act2Contract>> GetAct2ContractsAsync() => _context.GetAct2ContractsAsync();

        public Task<List<Building>> GetBuildingsAsync() => _context.GetBuildingsAsync();

        public Task<List<CancelContract>> GetCancelContractsAsync() => _context.GetCancelContractsAsync();

        public Task<List<ChangeArendPriceContract>> GetChangeArendPriceContractsAsync() => _context.GetChangeArendPriceContractsAsync();

        public Task<List<ClaimContract>> GetClaimContractsAsync() => _context.GetClaimContractsAsync();

        public Task<List<Entity>> GetEntitiesAsync() => _context.GetEntitiesAsync();

        public Task<List<Entity>> GetEntitiesByCondition(Expression<Func<Entity, bool>> predicate) => _context.GetEntitiesByCondition(predicate);

        public Task<List<EntityType>> GetEntityTypesAsync() => _context.GetEntityTypesAsync();

        public Task<LeaseContract> GetLeaseContractAsync(int Id) => _context.GetLeaseContractAsync(Id);

        public Task<List<LeaseContractOffice>> GetLeaseContractOfficesAsync() => _context.GetLeaseContractOfficesAsync();

        public Task<List<LeaseContract>> GetLeaseContractsAsync() => _context.GetLeaseContractsAsync();

        public Task<List<Office>> GetOfficesAsync() => _context.GetOfficesAsync();

        public Task<List<TerminationContract>> GetTerminationContractsAsync() => _context.GetTerminationContractsAsync();

        public Task<List<UtilityServiceContractOffice>> GetUtilityServiceContractOfficesAsync() => _context.GetUtilityServiceContractOfficesAsync();

        public Task<List<UtilityServiceContract>> GetUtilityServiceContractsAsync() => _context.GetUtilityServiceContractsAsync();
    }
}
