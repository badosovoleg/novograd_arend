﻿using Microsoft.EntityFrameworkCore;
using NovogradArend.EF.Configurations;
using NovogradArend.Model;
using NovogradArend.Model.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace NovogradArend.EF
{
    public class SQLiteDatabaseContext : DbContext, IObjectsRepository
    {
        #region Defenition of collections

        public DbSet<Entity> Entities { get; set; }

        public DbSet<EntityType> EntityTypes { get; set; }

        public DbSet<Building> Buildings { get; set; }

        public DbSet<Office> Offices { get; set; }

        public DbSet<LeaseContract> LeaseContracts { get; set; }

        public DbSet<Act1Contract> Act1Contracts { get; set; }

        public DbSet<Act2Contract> Act2Contracts { get; set; }

        public DbSet<LeaseContractOffice> LeaseContractOffices { get; set; }

        public DbSet<UtilityServiceContract> UtilityServiceContracts { get; set; }

        public DbSet<UtilityServiceContractOffice> UtilityServiceContractOffices { get; set; }

        public DbSet<ClaimContract> ClaimContracts { get; set; }

        public DbSet<ChangeArendPriceContract> ChangeArendPriceContracts { get; set; }

        public DbSet<TerminationContract> TerminationContracts { get; set; }

        public DbSet<CancelContract> CancelContracts { get; set; }

        #endregion Defenition of collections

        public SQLiteDatabaseContext(DbContextOptions<SQLiteDatabaseContext> dbContextOptions)
            : base(dbContextOptions) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new EntityConfiguration());
            modelBuilder.ApplyConfiguration(new EntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new BuildingConfiguration());
            modelBuilder.ApplyConfiguration(new OfficeConfiguration());
            modelBuilder.ApplyConfiguration(new LeaseContractConfiguration());
            modelBuilder.ApplyConfiguration(new LeaseContractOfficeConfiguration());
            modelBuilder.ApplyConfiguration(new Act1ContractConfiguration());
            modelBuilder.ApplyConfiguration(new Act2ContractConfiguration());
            modelBuilder.ApplyConfiguration(new UtilityServiceContractConfiguration());
            modelBuilder.ApplyConfiguration(new UtilityServiceContractOfficeConfiguration());
            modelBuilder.ApplyConfiguration(new ClaimContractConfiguration());
            modelBuilder.ApplyConfiguration(new ChangeArendPriceContractConfiguration());
            modelBuilder.ApplyConfiguration(new TerminationContractConfiguration());
            modelBuilder.ApplyConfiguration(new CancelContractConfiguration());
        }

        #region 'Get' methods for collections

        public Task<List<Office>> GetOfficesAsync() => Offices.ToListAsync();

        public Task<List<Building>> GetBuildingsAsync() => Buildings.ToListAsync();

        public Task<List<Entity>> GetEntitiesAsync() => Entities.ToListAsync();

        public Task<List<EntityType>> GetEntityTypesAsync() => EntityTypes.ToListAsync();

        public Task<List<LeaseContract>> GetLeaseContractsAsync() => LeaseContracts.ToListAsync();

        public Task<List<LeaseContractOffice>> GetLeaseContractOfficesAsync() => LeaseContractOffices.ToListAsync();

        public Task<List<Act1Contract>> GetAct1ContractsAsync() => Act1Contracts.ToListAsync();

        public Task<List<Act2Contract>> GetAct2ContractsAsync() => Act2Contracts.ToListAsync();

        public Task<List<UtilityServiceContract>> GetUtilityServiceContractsAsync() => UtilityServiceContracts.ToListAsync();

        public Task<List<UtilityServiceContractOffice>> GetUtilityServiceContractOfficesAsync() => UtilityServiceContractOffices.ToListAsync();

        public Task<List<ClaimContract>> GetClaimContractsAsync() => ClaimContracts.ToListAsync();

        public Task<List<ChangeArendPriceContract>> GetChangeArendPriceContractsAsync() => ChangeArendPriceContracts.ToListAsync();

        public Task<List<TerminationContract>> GetTerminationContractsAsync() => TerminationContracts.ToListAsync();

        public Task<List<CancelContract>> GetCancelContractsAsync() => CancelContracts.ToListAsync();

        #endregion 'Get' methods for collections

        #region Work with 'Entity'

        private string EntityTableName => NovogradArend.Model.ModelExtensions.GetTableNameForType(nameof(Entity));

        private Dictionary<string, object> EntityValues(Entity entity)
        {
            var values = new Dictionary<string, object>
            {
                { nameof(Entity.Name), entity.Name },
                { nameof(Entity.Represented), entity.Represented },
                { nameof(Entity.Agreement), entity.Agreement },
                { nameof(Entity.ShortName), entity.ShortName },
                { nameof(Entity.EntityTypeId), entity.EntityTypeId },
                { nameof(Entity.Initials), entity.Initials },
            };

            if (entity.Requisites != null) values.Add(nameof(Entity.Requisites), entity.Requisites);
            if (entity.INN != null) values.Add(nameof(Entity.INN), entity.INN);
            if (entity.OGRN != null) values.Add(nameof(Entity.OGRN), entity.OGRN);
            if (entity.RS != null) values.Add(nameof(Entity.RS), entity.RS);
            if (entity.BIK != null) values.Add(nameof(Entity.BIK), entity.BIK);
            if (entity.Email != null) values.Add(nameof(Entity.Email), entity.Email);
            if (entity.Phone != null) values.Add(nameof(Entity.Phone), entity.Phone);
            if (entity.BaseOn != null) values.Add(nameof(Entity.BaseOn), entity.BaseOn);
            if (entity.Address != null) values.Add(nameof(Entity.Address), entity.Address);
            if (entity.ShortRequisites != null) values.Add(nameof(Entity.ShortRequisites), entity.ShortRequisites);

            return values;
        }

        public async Task<int> AddEntityAsync(Entity entity)
            => await Database.AddValues(EntityTableName, EntityValues(entity));

        public async Task<int> EditEntityAsync(Entity entity)
            => await Database.EditValues(EntityTableName, entity.GetObjectDatabaseId(), EntityValues(entity));

        public async Task<List<Entity>> GetEntitiesByCondition(Expression<Func<Entity, bool>> predicate)
            => await Entities.AsQueryable().Where(predicate).ToListAsync();

        #endregion Work with 'Entity'

        #region Work with 'LeaseContract'

        private string LeaseContractTableName => NovogradArend.Model.ModelExtensions.GetTableNameForType(nameof(LeaseContract));

        private Dictionary<string, object> LeaseContractValues(LeaseContract leaseContract)
        {
            var values = new Dictionary<string, object>
            {
                { nameof(LeaseContract.Number), leaseContract.Number },
                { nameof(LeaseContract.BeginDate), leaseContract.BeginDate },
                { nameof(LeaseContract.EndDate), leaseContract.EndDate },
                { nameof(LeaseContract.LandLordId), leaseContract.LandLordId },
                { nameof(LeaseContract.RenterId), leaseContract.RenterId },
                { nameof(LeaseContract.BuildingId), leaseContract.BuildingId },
                { nameof(LeaseContract.CountLandLordDay), leaseContract.CountLandLordDay },
                { nameof(LeaseContract.CountRenterDay), leaseContract.CountRenterDay },
                { nameof(LeaseContract.ForPlacement), leaseContract.ForPlacement },
                { nameof(LeaseContract.Price), leaseContract.Price },
                { nameof(LeaseContract.TotalPrice), leaseContract.TotalPrice },
                { nameof(LeaseContract.TotalSquare), leaseContract.TotalSquare },
                { nameof(LeaseContract.Act1BeginDate), leaseContract.Act1BeginDate },
                { nameof(LeaseContract.IsActual), leaseContract.IsActual },
            };

            if (leaseContract.GaranteeDate != null && leaseContract.GaranteeNumber != null)
            {
                values.Add(nameof(LeaseContract.GaranteeNumber), leaseContract.GaranteeNumber);
                values.Add(nameof(LeaseContract.GaranteeDate), leaseContract.GaranteeDate);
            }

            return values;
        }

        public async Task<int> AddLeaseContractAsync(LeaseContract contract)
            => await Database.AddValues(LeaseContractTableName, LeaseContractValues(contract));

        public async Task<int> EditLeaseContractAsync(LeaseContract contract)
            => await Database.EditValues(LeaseContractTableName, contract.GetObjectDatabaseId(), LeaseContractValues(contract));

        public async Task<LeaseContract> GetLeaseContractAsync(int Id)
            => await LeaseContracts.SingleAsync(c => c.Id == Id);

        #endregion Work with 'LeaseContract'

        #region Work with 'LeaseContractOffice'

        private string LeaseContractOfficeTableName => NovogradArend.Model.ModelExtensions.GetTableNameForType(nameof(LeaseContractOffice));

        private Dictionary<string, object> LeaseContractOfficeValues(LeaseContractOffice leaseContractOffice)
            => new Dictionary<string, object>
            {
                { nameof(LeaseContractOffice.LeaseContractId), leaseContractOffice.LeaseContractId },
                { nameof(LeaseContractOffice.OfficeId), leaseContractOffice.OfficeId }
            };

        public async Task<int> AddLeaseContractOfficeAsync(LeaseContractOffice contractOffice)
            => await Database.AddValues(LeaseContractOfficeTableName, LeaseContractOfficeValues(contractOffice));

        public async Task<int> RemoveLeaseContractOfficeAsync(LeaseContractOffice contractOffice)
            => await Database.Remove(LeaseContractOfficeTableName, contractOffice.GetObjectDatabaseId());

        #endregion Work with 'LeaseContractOffice'

        #region Work with 'Act1Contract'

        private string Act1ContractTableName => NovogradArend.Model.ModelExtensions.GetTableNameForType(nameof(Act1Contract));

        private Dictionary<string, object> Act1ContractValues(Act1Contract act1Contract)
            => new Dictionary<string, object>
            {
                { nameof(Act1Contract.Number), act1Contract.Number },
                { nameof(Act1Contract.LeaseContractId), act1Contract.LeaseContractId },
                { nameof(Act1Contract.BeginDate), act1Contract.BeginDate }
            };

        public async Task<int> AddAct1ContractAsync(Act1Contract contract)
            => await Database.AddValues(Act1ContractTableName, Act1ContractValues(contract));

        public async Task<int> EditAct1ContractAsync(Act1Contract contract)
            => await Database.EditValues(Act1ContractTableName, contract.GetObjectDatabaseId(), Act1ContractValues(contract));

        #endregion Work with 'Act1Contract'

        #region Work with 'Act2Contract'

        private string Act2ContractTableName => NovogradArend.Model.ModelExtensions.GetTableNameForType(nameof(Act2Contract));

        private Dictionary<string, object> Act2ContractValues(Act2Contract act2Contract)
            => new Dictionary<string, object>
            {
                { nameof(Act2Contract.Number), act2Contract.Number },
                { nameof(Act2Contract.LeaseContractId), act2Contract.LeaseContractId },
                { nameof(Act2Contract.BeginDate), act2Contract.BeginDate }
            };

        public async Task<int> AddAct2ContractAsync(Act2Contract contract)
            => await Database.AddValues(Act2ContractTableName, Act2ContractValues(contract));

        public async Task<int> EditAct2ContractAsync(Act2Contract contract)
            => await Database.EditValues(Act2ContractTableName, contract.GetObjectDatabaseId(), Act2ContractValues(contract));

        #endregion Work with 'Act2Contract'

        #region Work with 'UtilityServiceContract'

        private string UtilityServiceContractTableName => NovogradArend.Model.ModelExtensions.GetTableNameForType(nameof(UtilityServiceContract));

        private Dictionary<string, object> UtilityServiceContractValues(UtilityServiceContract utilityServiceContract)
            => new Dictionary<string, object>
            {
                { nameof(UtilityServiceContract.Number), utilityServiceContract.Number },
                { nameof(UtilityServiceContract.BeginDate), utilityServiceContract.BeginDate },
                { nameof(UtilityServiceContract.LeaseContractId), utilityServiceContract.LeaseContractId },
                { nameof(UtilityServiceContract.ManagerCompanyId), utilityServiceContract.ManagerCompanyId },
                { nameof(UtilityServiceContract.RenterId), utilityServiceContract.RenterId },
                { nameof(UtilityServiceContract.UtilityServicePrice), utilityServiceContract.UtilityServicePrice },
                { nameof(UtilityServiceContract.UtilityServiceTotalPrice), utilityServiceContract.UtilityServiceTotalPrice },
            };

        public async Task<int> AddUtilityServiceContractAsync(UtilityServiceContract contract)
            => await Database.AddValues(UtilityServiceContractTableName, UtilityServiceContractValues(contract));

        public async Task<int> EditUtilityServiceContractAsync(UtilityServiceContract contract)
            => await Database.EditValues(UtilityServiceContractTableName, contract.GetObjectDatabaseId(), UtilityServiceContractValues(contract));

        #endregion Work with 'UtilityServiceContract'

        #region Work with 'UtilityServiceContractOffice'

        private string UtilityServiceContractOfficeTableName => NovogradArend.Model.ModelExtensions.GetTableNameForType(nameof(UtilityServiceContractOffice));

        private Dictionary<string, object> UtilityServiceContractOfficeValues(UtilityServiceContractOffice utilityServiceContractOffice)
            => new Dictionary<string, object>
            {
                { nameof(UtilityServiceContractOffice.OfficeId), utilityServiceContractOffice.OfficeId },
                { nameof(UtilityServiceContractOffice.UtilityServiceContractId), utilityServiceContractOffice.UtilityServiceContractId },
            };

        public async Task<int> AddUtilityServiceContractOfficeAsync(UtilityServiceContractOffice utilityServiceContractOffice)
            => await Database.AddValues(UtilityServiceContractOfficeTableName, UtilityServiceContractOfficeValues(utilityServiceContractOffice));

        public async Task<int> RemoveUtilityServiceContractOfficeAsync(UtilityServiceContractOffice utilityServiceContractOffice)
            => await Database.Remove(UtilityServiceContractOfficeTableName, utilityServiceContractOffice.GetObjectDatabaseId());

        #endregion Work with 'UtilityServiceContractOffice'

        #region Work with 'ClaimContract'

        private string ClaimContractTableName => NovogradArend.Model.ModelExtensions.GetTableNameForType(nameof(ClaimContract));

        private Dictionary<string, object> ClaimContractValues(ClaimContract claimContract)
            => new Dictionary<string, object>
            {
                { nameof(ClaimContract.Number), claimContract.Number },
                { nameof(ClaimContract.BeginDate), claimContract.BeginDate },
                { nameof(ClaimContract.LeaseContractId), claimContract.LeaseContractId },
                { nameof(ClaimContract.UtilityServiceContractId), claimContract.UtilityServiceContractId },
                { nameof(ClaimContract.UtilityServiceDebt), claimContract.UtilityServiceDebt },
                { nameof(ClaimContract.ArendDebt), claimContract.ArendDebt },
                { nameof(ClaimContract.ClaimDate), claimContract.ClaimDate },
            };

        public async Task<int> AddClaimContractAsync(ClaimContract contract)
            => await Database.AddValues(ClaimContractTableName, ClaimContractValues(contract));

        public async Task<int> EditClaimContractAsync(ClaimContract contract)
            => await Database.EditValues(ClaimContractTableName, contract.GetObjectDatabaseId(), ClaimContractValues(contract));

        #endregion Work with 'ClaimContract'

        #region Work with 'ChangeArendPriceContract'

        private string ChangeArendPriceContractTableName => NovogradArend.Model.ModelExtensions.GetTableNameForType(nameof(ChangeArendPriceContract));

        private Dictionary<string, object> ChangeArendPriceContractValues(ChangeArendPriceContract changeArendPriceContract)
            => new Dictionary<string, object>
            {
                { nameof(ChangeArendPriceContract.Number), changeArendPriceContract.Number },
                { nameof(ChangeArendPriceContract.BeginDate), changeArendPriceContract.BeginDate },
                { nameof(ChangeArendPriceContract.LeaseContractId), changeArendPriceContract.LeaseContractId },
                { nameof(ChangeArendPriceContract.NewOfficePrice), changeArendPriceContract.NewOfficePrice },
                { nameof(ChangeArendPriceContract.NewOfficeTotalPrice), changeArendPriceContract.NewOfficeTotalPrice },
            };

        public async Task<int> AddChangeArendPriceContractAsync(ChangeArendPriceContract contract)
            => await Database.AddValues(ChangeArendPriceContractTableName, ChangeArendPriceContractValues(contract));

        public async Task<int> EditChangeArendPriceContractAsync(ChangeArendPriceContract contract)
            => await Database.EditValues(ChangeArendPriceContractTableName, contract.GetObjectDatabaseId(), ChangeArendPriceContractValues(contract));

        #endregion Work with 'ChangeArendPriceContract'

        #region Work with 'TerminationContract'

        private string TerminationContractTableName => NovogradArend.Model.ModelExtensions.GetTableNameForType(nameof(TerminationContract));

        private Dictionary<string, object> TerminationContractValues(TerminationContract terminationContract)
            => new Dictionary<string, object>
            {
                { nameof(TerminationContract.BeginDate), terminationContract.BeginDate },
                { nameof(TerminationContract.LeaseContractId), terminationContract.LeaseContractId },
            };

        public async Task<int> AddTerminationContractAsync(TerminationContract contract)
            => await Database.AddValues(TerminationContractTableName, TerminationContractValues(contract));

        public async Task<int> EditTerminationContractAsync(TerminationContract contract)
            => await Database.EditValues(TerminationContractTableName, contract.GetObjectDatabaseId(), TerminationContractValues(contract));

        #endregion Work with 'TerminationContract'

        #region Work with 'CancelContract'

        private string CancelContractTableName => NovogradArend.Model.ModelExtensions.GetTableNameForType(nameof(CancelContract));

        private Dictionary<string, object> CancelContractValues(CancelContract cancelContract)
            => new Dictionary<string, object>
            {
                { nameof(CancelContract.BeginDate), cancelContract.BeginDate },
                { nameof(CancelContract.LeaseContractId), cancelContract.LeaseContractId },
                { nameof(CancelContract.MailNumber), cancelContract.MailNumber },
                { nameof(CancelContract.MailDate), cancelContract.MailDate },
                { nameof(CancelContract.LandLordId), cancelContract.LandLordId },
                { nameof(CancelContract.RenterId), cancelContract.RenterId },
                { nameof(CancelContract.BuildingId), cancelContract.BuildingId },
            };

        public async Task<int> AddCancelContractAsync(CancelContract contract)
            => await Database.AddValues(CancelContractTableName, CancelContractValues(contract));

        public async Task<int> EditCancelContractAsync(CancelContract contract)
            => await Database.EditValues(CancelContractTableName, contract.GetObjectDatabaseId(), CancelContractValues(contract));

        #endregion Work with 'CancelContract'
    }
}