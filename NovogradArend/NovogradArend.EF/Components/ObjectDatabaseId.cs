﻿using NovogradArend.Model;

namespace NovogradArend.EF
{
    public class ObjectDatabaseId
    {
        public readonly int Value;
        public readonly string ColumnName;

        public ObjectDatabaseId(string columnName, int value)
        {
            Value = value;
            ColumnName = columnName;
        }
    }

    public static class ObjectDatabaseIdExtensions
    {
        public static ObjectDatabaseId GetObjectDatabaseId(this IModelWithId modelWithId)
        {
            return new ObjectDatabaseId(nameof(IModelWithId.Id), modelWithId.Id);
        }
    }
}