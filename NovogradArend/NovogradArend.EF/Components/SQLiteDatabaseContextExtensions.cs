﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using NovogradArend.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NovogradArend.EF
{
    internal static class SQLiteDatabaseContextExtensions
    {
        public static Task<int> AddValues(this DatabaseFacade databaseFacade, string tableName, Dictionary<string, object> values)
        {
            var args = values.Keys.ToArray();

            var addString = $"insert into {tableName} ({string.Join(", ", args)}) Values(@{string.Join(", @", args)})";

            var parameters = values.Select(value => new SqliteParameter($"@{value.Key}", value.Value));

#pragma warning disable EF1000 // Possible SQL injection vulnerability.
            return databaseFacade.ExecuteSqlCommandAsync(addString, parameters);
#pragma warning restore EF1000 // Possible SQL injection vulnerability.
        }

        public static Task<int> EditValues(this DatabaseFacade databaseFacade, string tableName, ObjectDatabaseId objectDatabaseId, Dictionary<string, object> values)
        {
            var args = values.Keys.ToArray();

            var addString = $"update {tableName} set {string.Join(", ", args.Select((a) => $"{a}=@{a}"))} where {objectDatabaseId.ColumnName}=@{objectDatabaseId.ColumnName}";

            var parameters = values.Select(value => new SqliteParameter($"@{value.Key}", value.Value)).ToList();
            parameters.Add(new SqliteParameter($"@{objectDatabaseId.ColumnName}", objectDatabaseId.Value));

#pragma warning disable EF1000 // Possible SQL injection vulnerability.
            return databaseFacade.ExecuteSqlCommandAsync(addString, parameters);
#pragma warning restore EF1000 // Possible SQL injection vulnerability.
        }

        public static Task<int> Remove(this DatabaseFacade databaseFacade, string tableName, ObjectDatabaseId objectDatabaseId)
        {
            var removeString = $"delete from {tableName} where {objectDatabaseId.ColumnName}=@{objectDatabaseId.ColumnName};";

            var parameters = new[] { new SqliteParameter($"@{objectDatabaseId.ColumnName}", objectDatabaseId.Value) };

#pragma warning disable EF1000 // Possible SQL injection vulnerability.
            return databaseFacade.ExecuteSqlCommandAsync(removeString, parameters);
#pragma warning restore EF1000 // Possible SQL injection vulnerability.
        }
    }
}