﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NovogradArend.Model;

namespace NovogradArend.EF.Configurations
{
    class UtilityServiceContractOfficeConfiguration : IEntityTypeConfiguration<UtilityServiceContractOffice>
    {
        public void Configure(EntityTypeBuilder<UtilityServiceContractOffice> builder)
        {
            builder.ToTable(Model.ModelExtensions.GetTableNameForType(nameof(UtilityServiceContractOffice)));
        }
    }
}
