﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NovogradArend.Model;

namespace NovogradArend.EF.Configurations
{
    class TerminationContractConfiguration : IEntityTypeConfiguration<TerminationContract>
    {
        public void Configure(EntityTypeBuilder<TerminationContract> builder)
        {
            builder.ToTable(Model.ModelExtensions.GetTableNameForType(nameof(TerminationContract)));
        }
    }
}
