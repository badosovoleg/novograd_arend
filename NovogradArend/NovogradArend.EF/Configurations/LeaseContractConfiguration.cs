﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NovogradArend.Model;

namespace NovogradArend.EF.Configurations
{
    class LeaseContractConfiguration : IEntityTypeConfiguration<LeaseContract>
    {
        public void Configure(EntityTypeBuilder<LeaseContract> builder)
        {
            builder.ToTable(Model.ModelExtensions.GetTableNameForType(nameof(LeaseContract)));

            //builder.Property(e => e.IsActual).HasConversion(e => e ? 1 : 0, e => e == 1 ? true : false);
        }
    }
}
