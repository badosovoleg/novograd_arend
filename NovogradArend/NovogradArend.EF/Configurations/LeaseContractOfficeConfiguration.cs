﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NovogradArend.Model;

namespace NovogradArend.EF.Configurations
{
    class LeaseContractOfficeConfiguration : IEntityTypeConfiguration<LeaseContractOffice>
    {
        public void Configure(EntityTypeBuilder<LeaseContractOffice> builder)
        {
            builder.ToTable(Model.ModelExtensions.GetTableNameForType(nameof(LeaseContractOffice)));
        }
    }
}
