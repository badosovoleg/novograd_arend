﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NovogradArend.Model;

namespace NovogradArend.EF.Configurations
{
    class UtilityServiceContractConfiguration : IEntityTypeConfiguration<UtilityServiceContract>
    {
        public void Configure(EntityTypeBuilder<UtilityServiceContract> builder)
        {
            builder.ToTable(Model.ModelExtensions.GetTableNameForType(nameof(UtilityServiceContract)));
        }
    }
}
