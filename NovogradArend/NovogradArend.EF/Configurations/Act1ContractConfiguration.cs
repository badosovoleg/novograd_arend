﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NovogradArend.Model;

namespace NovogradArend.EF.Configurations
{
    class Act1ContractConfiguration : IEntityTypeConfiguration<Act1Contract>
    {
        public void Configure(EntityTypeBuilder<Act1Contract> builder)
        {
            builder.ToTable(Model.ModelExtensions.GetTableNameForType(nameof(Act1Contract)));
        }
    }
}
