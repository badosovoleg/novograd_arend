﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NovogradArend.Model;

namespace NovogradArend.EF.Configurations
{
    class Act2ContractConfiguration : IEntityTypeConfiguration<Act2Contract>
    {
        public void Configure(EntityTypeBuilder<Act2Contract> builder)
        {
            builder.ToTable(Model.ModelExtensions.GetTableNameForType(nameof(Act2Contract)));
        }
    }
}
