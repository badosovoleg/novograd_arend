﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NovogradArend.Model;

namespace NovogradArend.EF.Configurations
{
    class ChangeArendPriceContractConfiguration : IEntityTypeConfiguration<ChangeArendPriceContract>
    {
        public void Configure(EntityTypeBuilder<ChangeArendPriceContract> builder)
        {
            builder.ToTable(Model.ModelExtensions.GetTableNameForType(nameof(ChangeArendPriceContract)));
        }
    }
}
