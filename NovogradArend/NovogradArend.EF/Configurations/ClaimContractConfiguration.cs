﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NovogradArend.Model;

namespace NovogradArend.EF.Configurations
{
    class ClaimContractConfiguration : IEntityTypeConfiguration<ClaimContract>
    {
        public void Configure(EntityTypeBuilder<ClaimContract> builder)
        {
            builder.ToTable(Model.ModelExtensions.GetTableNameForType(nameof(ClaimContract)));
        }
    }
}
