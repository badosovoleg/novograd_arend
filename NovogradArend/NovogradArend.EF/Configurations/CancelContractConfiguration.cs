﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NovogradArend.Model;

namespace NovogradArend.EF.Configurations
{
    class CancelContractConfiguration : IEntityTypeConfiguration<CancelContract>
    {
        public void Configure(EntityTypeBuilder<CancelContract> builder)
        {
            builder.ToTable(Model.ModelExtensions.GetTableNameForType(nameof(CancelContract)));
        }
    }
}
