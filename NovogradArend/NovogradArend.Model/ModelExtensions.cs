﻿using System.Collections.Generic;

namespace NovogradArend.Model
{
    public static class ModelExtensions
    {
        private static readonly Dictionary<string, string> Map 
            = new Dictionary<string, string>
            {
                {nameof(Act1Contract), "Act1Contract" },
                {nameof(Act2Contract), "Act2Contract" },
                {nameof(Building), "Buildings" },
                {nameof(CancelContract), "CancelContract" },
                {nameof(ChangeArendPriceContract), "ChangeArendPriceContract" },
                {nameof(ClaimContract), "ClaimContract" },
                {nameof(Entity), "Entities" },
                {nameof(EntityType), "EntityTypes" },
                {nameof(LeaseContract), "LeaseContract" },
                {nameof(LeaseContractOffice), "LeaseContractOffice" },
                {nameof(Office), "Offices" },
                {nameof(TerminationContract), "TerminationContract" },
                {nameof(UtilityServiceContract), "UtilityServiceContract" },
                {nameof(UtilityServiceContractOffice), "UtilityServiceContractOffice" }
            };

        public static string GetTableNameForType(string typeName) => Map[typeName];
    }
}
