﻿namespace NovogradArend.Model
{
    /// <summary>
    /// Модель данных здания
    /// </summary>
    public class Building : IModelWithId
    {
        /// <summary>
        /// Ид в бд
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        public string Address { get; set; }

        public int ManagerCompanyId { get; set; }

        public string Code { get; set; }

        public override string ToString() => $"{Name}, {Address}";
    }
}
