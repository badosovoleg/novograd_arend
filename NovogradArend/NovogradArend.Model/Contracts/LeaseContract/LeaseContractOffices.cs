﻿namespace NovogradArend.Model
{
    public class LeaseContractOffice : IModelWithId
    {
        public int Id { get; set; }
        public int LeaseContractId { get; set; }
        public int OfficeId { get; set; }
    }
}
