﻿namespace NovogradArend.Model
{
    public class LeaseContract : IModelWithId, IModelWithBeginDate, IModelWithNumber
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string BeginDate { get; set; }
        public string EndDate { get; set; }
        public int LandLordId { get; set; }
        public int RenterId { get; set; }
        public int BuildingId { get; set; }
        public int CountLandLordDay { get; set; }
        public int CountRenterDay { get; set; }
        public string GaranteeNumber { get; set; }
        public string GaranteeDate { get; set; }
        public string ForPlacement { get; set; }
        public decimal Price { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal TotalSquare { get; set; }
        public string Act1BeginDate { get; set; }
        public bool IsActual { get; set; }

        public override string ToString() => $"{Number}, {BeginDate}";
    }
}