﻿namespace NovogradArend.Model
{
    public class ChangeArendPriceContract : IModelWithLeaseContract, IModelWithNumber
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string BeginDate { get; set; }
        public int LeaseContractId { get; set; }
        public decimal NewOfficePrice { get; set; }
        public decimal NewOfficeTotalPrice { get; set; }
    }
}