﻿namespace NovogradArend.Model
{
    public interface IModelWithId
    {
        /// <summary>
        /// Ид в бд
        /// </summary>
        int Id { get; set; }
    }

    public interface IModelWithBeginDate
    {
        string BeginDate { get; set; }
    }

    public interface IModelWithLeaseContract :
        IModelWithId,
        IModelWithBeginDate
    {
        int LeaseContractId { get; set; }
    }

    public interface IModelWithNumber
    {
        /// <summary>
        /// Номер договора
        /// </summary>
        string Number { get; set; }
    }
}