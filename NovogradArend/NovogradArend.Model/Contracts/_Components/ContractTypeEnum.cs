﻿using System;

namespace NovogradArend.Model
{
    public sealed class ContractTypeEnum
    {
        private readonly string name;
        private readonly string value;
        private readonly int id;

        /// <summary>
        /// "Договор аренды"
        /// </summary>
        public static readonly ContractTypeEnum Lease 
            = new ContractTypeEnum(nameof(LeaseContract), "Договор аренды");

        public static readonly ContractTypeEnum LeaseDialogTitle
            = new ContractTypeEnum(nameof(LeaseContract), "договора аренды");

        /// <summary>
        /// "АКТ приема-передачи №1"
        /// </summary>
        public static readonly ContractTypeEnum ACT1 
            = new ContractTypeEnum(nameof(Act1Contract), "АКТ приема-передачи №1");

        public static readonly ContractTypeEnum ACT1DialogTitle
            = new ContractTypeEnum(nameof(Act1Contract), "АКТа №1");

        /// <summary>
        /// "АКТ приема-передачи №2"
        /// </summary>
        public static readonly ContractTypeEnum ACT2 
            = new ContractTypeEnum(nameof(Act2Contract), "АКТ приема-передачи №2");

        public static readonly ContractTypeEnum ACT2DialogTitle
            = new ContractTypeEnum(nameof(Act2Contract), "АКТа №2");

        /// <summary>
        /// "Договор изменения арендной платы"
        /// </summary>
        public static readonly ContractTypeEnum ChangeArendPrice 
            = new ContractTypeEnum(nameof(ChangeArendPriceContract), "Изменение арендной платы");

        public static readonly ContractTypeEnum ChangeArendPriceDialogTitle
            = new ContractTypeEnum(nameof(ChangeArendPriceContract), "договора изменения арендной платы");

        /// <summary>
        /// "Договор коммунальное обслуживание"
        /// </summary>
        public static readonly ContractTypeEnum UtilityService 
            = new ContractTypeEnum(nameof(UtilityServiceContract), "Договор на обслуживание");

        public static readonly ContractTypeEnum UtilityServiceDialogTitle
            = new ContractTypeEnum(nameof(UtilityServiceContract), "договора на обслуживание");

        /// <summary>
        /// "Документ отказа"
        /// </summary>
        public static readonly ContractTypeEnum Cancel 
            = new ContractTypeEnum(nameof(CancelContract), "Письмо отказа");

        public static readonly ContractTypeEnum CancelDialogTitle
            = new ContractTypeEnum(nameof(CancelContract), "письма отказа");

        /// <summary>
        /// "Краткое содержание"
        /// </summary>
        public static readonly ContractTypeEnum ShortContent 
            = new ContractTypeEnum("SHORT", "Краткое содержание");

        /// <summary>
        /// "Претензия"
        /// </summary>
        public static readonly ContractTypeEnum Claim 
            = new ContractTypeEnum(nameof(ClaimContract), "Досудебная претензия");

        public static readonly ContractTypeEnum ClaimDialogTitle
            = new ContractTypeEnum(nameof(ClaimContract), "досуденой претензии");

        /// <summary>
        /// "Соглашение о расторжении"
        /// </summary>
        public static readonly ContractTypeEnum Termination 
            = new ContractTypeEnum(nameof(TerminationContract), "Соглашение о расторжении");

        public static readonly ContractTypeEnum TerminationDialogTitle
            = new ContractTypeEnum(nameof(TerminationContract), "соглашения о расторжении");

        private ContractTypeEnum(string value, string name, int id = 0)
        {
            this.name = name;
            this.value = value;
            this.id = id;
        }

        public Type GetContractType() => Type.GetType(value);

        public override string ToString() => name;

        public static implicit operator string(ContractTypeEnum type) => type.name;

        public static ContractTypeEnum Parse(string value)
        {
            if (value == Lease.name) return Lease;
            else if (value == ACT1.name) return ACT1;
            else if (value == ACT2.name) return ACT2;
            else if (value == ChangeArendPrice.name) return ChangeArendPrice;
            else if (value == UtilityService.name) return UtilityService;
            else if (value == Cancel.name) return Cancel;
            else if (value == ShortContent.name) return ShortContent;
            else if (value == Claim.name) return Claim;
            else if (value == Termination.name) return Termination;
            return null;
        }

        private ContractTypeEnum Copy() => new ContractTypeEnum(value, name);
    }
}