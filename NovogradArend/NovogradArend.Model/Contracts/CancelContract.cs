﻿using System;

namespace NovogradArend.Model
{
    public class CancelContract : IModelWithLeaseContract
    {
        public int Id { get; set; }
        public string BeginDate { get; set; }
        public string MailNumber { get; set; }
        public string MailDate { get; set; }
        public int LandLordId { get; set; }
        public int RenterId { get; set; }
        public int BuildingId { get; set; }
        public int LeaseContractId { get; set; }
    }
}