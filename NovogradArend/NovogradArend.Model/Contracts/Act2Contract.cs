﻿namespace NovogradArend.Model
{
    public class Act2Contract : IModelWithLeaseContract, IModelWithNumber
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string BeginDate { get; set; }
        public int LeaseContractId { get; set; }
    }
}