﻿using System;

namespace NovogradArend.Model
{
    public class TerminationContract : IModelWithLeaseContract
    {
        public int Id { get; set; }
        public string BeginDate { get; set; }
        public int LeaseContractId { get; set; }
    }
}