﻿namespace NovogradArend.Model
{
    public class ClaimContract : IModelWithLeaseContract, IModelWithNumber
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string BeginDate { get; set; }
        public int LeaseContractId { get; set; }
        public int UtilityServiceContractId { get; set; }
        public decimal ArendDebt { get; set; }
        public decimal UtilityServiceDebt { get; set; }
        public string ClaimDate { get; set; }
    }
}