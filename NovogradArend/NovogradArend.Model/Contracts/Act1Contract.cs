﻿namespace NovogradArend.Model
{
    public class Act1Contract : IModelWithLeaseContract, IModelWithNumber
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string BeginDate { get; set; }
        public int LeaseContractId { get; set; }
    }
}