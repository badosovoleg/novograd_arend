﻿namespace NovogradArend.Model
{
    public class UtilityServiceContract : IModelWithLeaseContract, IModelWithNumber
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string BeginDate { get; set; }
        public int LeaseContractId { get; set; }
        public int ManagerCompanyId { get; set; }
        public int RenterId { get; set; }
        public decimal UtilityServicePrice { get; set; }
        public decimal UtilityServiceTotalPrice { get; set; }

        public override string ToString() => $"{Number} от {BeginDate}";
    }
}