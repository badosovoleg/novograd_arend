﻿namespace NovogradArend.Model
{
    public class UtilityServiceContractOffice : IModelWithId
    {
        public int Id { get; set; }
        public int UtilityServiceContractId { get; set; }
        public int OfficeId { get; set; }
    }
}
