﻿namespace NovogradArend.Model
{
    public sealed class EntityTypeEnum
    {
        public readonly string Value;
        public readonly int Id;

        public static readonly EntityTypeEnum Landlord = new EntityTypeEnum(1, "Арендодатель");

        public static readonly EntityTypeEnum Renter = new EntityTypeEnum(2, "Арендатор");

        public static readonly EntityTypeEnum ManagementCompany = new EntityTypeEnum(3, "Управляющая компания");

        private EntityTypeEnum(int value, string name)
        {
            Value = name;
            Id = value;
        }

        public override string ToString() => Value;

        public static EntityTypeEnum Parse(string value)
        {
            if (value == Landlord.Value) return Landlord;
            else if (value == Renter.Value) return Renter;
            else if (value == ManagementCompany.Value) return ManagementCompany;
            return null;
        }
    }
}
