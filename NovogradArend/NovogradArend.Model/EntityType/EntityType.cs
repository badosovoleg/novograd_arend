﻿namespace NovogradArend.Model
{
    /// <summary>
    /// Модель типа юр. лица
    /// </summary>
    public class EntityType : IModelWithId
    {
        /// <summary>
        /// Ид в бд
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Тип юр. лица <see cref="Type" />
        /// </summary>
        public string Type { get; set; }
    }
}