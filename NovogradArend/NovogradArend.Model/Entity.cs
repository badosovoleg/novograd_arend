﻿using System.ComponentModel;

namespace NovogradArend.Model
{
    /// <summary>
    /// Модель данных юр. лица
    /// </summary>
    public class Entity : IModelWithId
    {
        /// <summary>
        /// Ид в бд
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// "В лице <see cref="Represented"/>"
        /// </summary>
        public string Represented { get; set; }

        /// <summary>
        /// "Действующего на основании <see cref="Agreement"/>"
        /// </summary>
        public string Agreement { get; set; }

        /// <summary>
        /// Ид типа юр. лица из бд
        /// </summary>
        public int EntityTypeId { get; set; }

        /// <summary>
        /// Реквизиты
        /// </summary>
        public string Requisites { get; set; }

        public string ShortRequisites { get; set; }

        public string INN { get; set; }

        public string OGRN { get; set; }

        public string RS { get; set; }

        public string BIK { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string ShortName { get; set; }

        public string Initials { get; set; }

        public string BaseOn { get; set; }

        public string Address { get; set; }

        public override string ToString() => $"{Name}";
    }
}
