﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NovogradArend.Model
{
    /// <summary>
    /// Модель данных офиса
    /// </summary>
    public class Office : IModelWithId
    {
        /// <summary>
        /// Ид в бд
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Ид здания из бд
        /// </summary>
        public int BuildingId { get; set; }

        /// <summary>
        /// Номер помещения
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Этаж
        /// </summary>
        public int Floor { get; set; }

        /// <summary>
        /// Номер помещения на поэтажном плане
        /// </summary>
        public string NumberOnFloorPlan { get; set; }

        public int OwnCadatrlOutType { get; set; }

        /// <summary>
        /// Право собственности
        /// </summary>
        public string Ownership { get; set; }

        public string RegistrationCertificate { get; set; }

        /// <summary>
        /// Кадастровый номер
        /// </summary>
        public string СadastralNumber { get; set; }

        /// <summary>
        /// Ид собственника из бд
        /// </summary>
        public int EntityId { get; set; }

        /// <summary>
        /// Площадь в кв. м.
        /// </summary>
        public decimal Square { get; set; }

        /// <summary>
        /// Цена за кв. м.
        /// </summary>
        public decimal Price { get; set; }

        public decimal ServicePrice { get; set; }

        /// <summary>
        /// Изображение-схема помещения
        /// </summary>
        public string ImagePath { get; set; }

        public override string ToString() => $"№ {Number}, Этаж - {Floor}";
    }
}
