﻿using NovogradArend.Model;
using NovogradArend.Model.Services;
using NovogradArend.ServiceLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace NovogradArend.Desktop.Services
{
    public class ObjectsService : IObjectsService
    {
        private readonly IObjectsRepository _repository;

        public ObjectsService()
        {
            var connectionString = ConfigurationManager.AppSettings["DBConnectionString"];
            _repository = new ObjectsRepository(connectionString);
        }

        public Task<int> AddAct1ContractAsync(Act1Contract contract) => _repository.AddAct1ContractAsync(contract);

        public Task<int> AddAct2ContractAsync(Act2Contract contract) => _repository.AddAct2ContractAsync(contract);

        public Task<int> AddCancelContractAsync(CancelContract contract) => _repository.AddCancelContractAsync(contract);

        public Task<int> AddChangeArendPriceContractAsync(ChangeArendPriceContract contract) => _repository.AddChangeArendPriceContractAsync(contract);

        public Task<int> AddClaimContractAsync(ClaimContract contract) => _repository.AddClaimContractAsync(contract);

        public Task<int> AddEntityAsync(Entity entity) => _repository.AddEntityAsync(entity);

        public Task<int> AddLeaseContractAsync(LeaseContract contract) => _repository.AddLeaseContractAsync(contract);

        public Task<int> AddLeaseContractOfficeAsync(LeaseContractOffice contractOffices) => _repository.AddLeaseContractOfficeAsync(contractOffices);

        public Task<int> AddTerminationContractAsync(TerminationContract contract) => _repository.AddTerminationContractAsync(contract);

        public Task<int> AddUtilityServiceContractAsync(UtilityServiceContract contract) => _repository.AddUtilityServiceContractAsync(contract);

        public Task<int> AddUtilityServiceContractOfficeAsync(UtilityServiceContractOffice contractOffices) => _repository.AddUtilityServiceContractOfficeAsync(contractOffices);

        public Task<int> EditAct1ContractAsync(Act1Contract contract) => _repository.EditAct1ContractAsync(contract);

        public Task<int> EditAct2ContractAsync(Act2Contract contract) => _repository.EditAct2ContractAsync(contract);

        public Task<int> EditCancelContractAsync(CancelContract contract) => _repository.EditCancelContractAsync(contract);

        public Task<int> EditChangeArendPriceContractAsync(ChangeArendPriceContract contract) => _repository.EditChangeArendPriceContractAsync(contract);

        public Task<int> EditClaimContractAsync(ClaimContract contract) => _repository.EditClaimContractAsync(contract);

        public Task<int> EditEntityAsync(Entity entity) => _repository.EditEntityAsync(entity);

        public Task<int> EditLeaseContractAsync(LeaseContract contract) => _repository.EditLeaseContractAsync(contract);

        public Task<int> RemoveLeaseContractOfficeAsync(LeaseContractOffice contractOffice) => _repository.RemoveLeaseContractOfficeAsync(contractOffice);

        public Task<int> EditTerminationContractAsync(TerminationContract contract) => _repository.EditTerminationContractAsync(contract);

        public Task<int> EditUtilityServiceContractAsync(UtilityServiceContract contract) => _repository.EditUtilityServiceContractAsync(contract);

        public Task<int> RemoveUtilityServiceContractOfficeAsync(UtilityServiceContractOffice contractOffice) => _repository.RemoveUtilityServiceContractOfficeAsync(contractOffice);

        public Task<List<Act1Contract>> GetAct1ContractsAsync() => _repository.GetAct1ContractsAsync();

        public Task<List<Act2Contract>> GetAct2ContractsAsync() => _repository.GetAct2ContractsAsync();

        public Task<List<Building>> GetBuildingsAsync() => _repository.GetBuildingsAsync();

        public Task<List<CancelContract>> GetCancelContractsAsync() => _repository.GetCancelContractsAsync();

        public Task<List<ChangeArendPriceContract>> GetChangeArendPriceContractsAsync() => _repository.GetChangeArendPriceContractsAsync();

        public Task<List<ClaimContract>> GetClaimContractsAsync() => _repository.GetClaimContractsAsync();

        public Task<List<Entity>> GetEntitiesAsync() => _repository.GetEntitiesAsync();

        public Task<List<Entity>> GetEntitiesByCondition(Expression<Func<Entity, bool>> predicate) => _repository.GetEntitiesByCondition(predicate);

        public Task<List<EntityType>> GetEntityTypesAsync() => _repository.GetEntityTypesAsync();

        public Task<LeaseContract> GetLeaseContractAsync(int Id) => _repository.GetLeaseContractAsync(Id);

        public Task<List<LeaseContractOffice>> GetLeaseContractOfficesAsync() => _repository.GetLeaseContractOfficesAsync();

        public Task<List<LeaseContract>> GetLeaseContractsAsync() => _repository.GetLeaseContractsAsync();

        public Task<List<Office>> GetOfficesAsync() => _repository.GetOfficesAsync();

        public Task<List<TerminationContract>> GetTerminationContractsAsync() => _repository.GetTerminationContractsAsync();

        public Task<List<UtilityServiceContractOffice>> GetUtilityServiceContractOfficesAsync() => _repository.GetUtilityServiceContractOfficesAsync();

        public Task<List<UtilityServiceContract>> GetUtilityServiceContractsAsync() => _repository.GetUtilityServiceContractsAsync();
    }
}