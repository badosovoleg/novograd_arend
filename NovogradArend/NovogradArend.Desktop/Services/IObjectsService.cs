﻿using NovogradArend.Model;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace NovogradArend.Desktop.Services
{
    public interface IObjectsService
    {
        Task<List<Office>> GetOfficesAsync();

        Task<List<Building>> GetBuildingsAsync();

        Task<List<Entity>> GetEntitiesAsync();

        Task<List<Entity>> GetEntitiesByCondition(Expression<Func<Entity, bool>> predicate);

        Task<List<EntityType>> GetEntityTypesAsync();

        Task<int> AddEntityAsync(Entity entity);

        Task<int> EditEntityAsync(Entity entity);

        Task<int> AddLeaseContractAsync(LeaseContract contract);

        Task<int> EditLeaseContractAsync(LeaseContract contract);

        Task<LeaseContract> GetLeaseContractAsync(int Id);

        Task<List<LeaseContract>> GetLeaseContractsAsync();

        Task<int> AddLeaseContractOfficeAsync(LeaseContractOffice contractOffice);

        Task<int> RemoveLeaseContractOfficeAsync(LeaseContractOffice contractOffice);

        Task<List<LeaseContractOffice>> GetLeaseContractOfficesAsync();

        Task<int> AddAct1ContractAsync(Act1Contract contract);

        Task<int> EditAct1ContractAsync(Act1Contract contract);

        Task<List<Act1Contract>> GetAct1ContractsAsync();

        Task<int> AddAct2ContractAsync(Act2Contract contract);

        Task<int> EditAct2ContractAsync(Act2Contract contract);

        Task<List<Act2Contract>> GetAct2ContractsAsync();

        Task<int> AddUtilityServiceContractAsync(UtilityServiceContract contract);

        Task<int> EditUtilityServiceContractAsync(UtilityServiceContract contract);

        Task<List<UtilityServiceContract>> GetUtilityServiceContractsAsync();

        Task<int> AddUtilityServiceContractOfficeAsync(UtilityServiceContractOffice contractOffice);

        Task<int> RemoveUtilityServiceContractOfficeAsync(UtilityServiceContractOffice contractOffice);

        Task<List<UtilityServiceContractOffice>> GetUtilityServiceContractOfficesAsync();

        Task<int> AddClaimContractAsync(ClaimContract contract);

        Task<int> EditClaimContractAsync(ClaimContract contract);

        Task<List<ClaimContract>> GetClaimContractsAsync();

        Task<int> AddChangeArendPriceContractAsync(ChangeArendPriceContract contract);

        Task<int> EditChangeArendPriceContractAsync(ChangeArendPriceContract contract);

        Task<List<ChangeArendPriceContract>> GetChangeArendPriceContractsAsync();

        Task<int> AddTerminationContractAsync(TerminationContract contract);

        Task<int> EditTerminationContractAsync(TerminationContract contract);

        Task<List<TerminationContract>> GetTerminationContractsAsync();

        Task<int> AddCancelContractAsync(CancelContract contract);

        Task<int> EditCancelContractAsync(CancelContract contract);

        Task<List<CancelContract>> GetCancelContractsAsync();
    }
}