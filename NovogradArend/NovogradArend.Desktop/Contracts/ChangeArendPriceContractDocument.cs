﻿using NovogradArend.Desktop.Contracts.Components;
using NovogradArend.Desktop.Extensions;
using NovogradArend.Model;
using NovogradArend.WordWorker;
using System;
using System.Collections.Generic;
using NovogradArend.WordWorker.Interfaces;
using static NovogradArend.WordWorker.WordBookmarks;

namespace NovogradArend.Desktop.Contracts
{
    /// <summary>
    /// Договор изменения арендной платы
    /// </summary>
    public class ChangeArendPriceContractDocument : BaseContractDocument
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public DateTime BeginDate { get; set; }
        public LeaseСontractDocument LeaseContract { get; set; }
        public decimal NewOfficePrice { get; set; }
        public decimal NewOfficeTotalPrice { get; set; }

        public override ContractTypeEnum ContractType => ContractTypeEnum.ChangeArendPrice;

        private string _nameOfDOCXTemplate = null;

        protected override string NameOfDOCXTemplate
        {
            get
            {
                if (_nameOfDOCXTemplate == null) _nameOfDOCXTemplate = "ChangeArendPriceContractTemplate";
                return _nameOfDOCXTemplate;
            }
        }

        public override IEnumerable<IReplacableBookmark> GetParametresForReplace()
        {
            var Landlord = LeaseContract.Landlord;

            yield return new ReplacebelTextByBookmark(LandlordNameBookmark, Landlord.Name);
            yield return new ReplacebelTextByBookmark(LandlordRepresentedBookmark, Landlord.Represented);
            yield return new ReplacebelTextByBookmark(LandlordAgreementBookmark, Landlord.Agreement);

            var Renter = LeaseContract.Renter;

            yield return new ReplacebelTextByBookmark(RenterNameBookmark, Renter.Name);
            yield return new ReplacebelTextByBookmark(RenterRepresentedBookmark, Renter.Represented);
            yield return new ReplacebelTextByBookmark(RenterAgreementBookmark, Renter.Agreement);

            var thisContractNumber = new[]
            {
                ChangeArendPriceNumberBookmark,
                ChangeArendPriceNumber2Bookmark
            };

            foreach (var bookmark in ReplacebelBookmarkCreator.GetManyBookmarks(Number, thisContractNumber))
                yield return bookmark;

            var leaseContractNubmer = new[]
            {
                NumberBookmark,
                Number2Bookmark,
                Number3Bookmark
            };

            foreach (var bookmark in ReplacebelBookmarkCreator.GetManyBookmarks(LeaseContract.Number, leaseContractNubmer))
                yield return bookmark;

            var leaseContactBeginDate = new[]
            {
                BeginDateBookmark,
                BeginDate2Bookmark,
                BeginDate3Bookmark
            };

            foreach (var bookmark in ReplacebelBookmarkCreator.GetManyBookmarks(LeaseContract.BeginDate.ToFormatDate(), leaseContactBeginDate))
                yield return bookmark;

            yield return new ReplacebelTextByBookmark(beginDateTopBookmark, LeaseContract.BeginDate.ToFormatDate());

            yield return new ReplacebelTextByBookmark(OfficePriceBookmark, RusNumber.BuildString(NewOfficePrice));
            yield return new ReplacebelTextByBookmark(TotalPriceBookmark, RusNumber.BuildString(NewOfficeTotalPrice));

            var landlordRequisites = new RequisitesBuilder(Landlord);
            var renterRequisites = new RequisitesBuilder(Renter);

            yield return new ReplacebelTextByBookmark(LandlordRequisitesBookmark, landlordRequisites.HeaderOfRequisites);
            yield return new ReplacebelTextByBookmark(LandlordRequisitesEndBookmark, landlordRequisites.EndOfRequisites);

            yield return new ReplacebelTextByBookmark(RenterRequisitesBookmark, renterRequisites.HeaderOfRequisites);
            yield return new ReplacebelTextByBookmark(RenterRequisitesEndBookmark, renterRequisites.EndOfRequisites);

            yield return new ReplacebelTextByBookmark(AdditinalDocumentHeaderBookmark, $@"Приложение №1
к Договору изменения арендной платы № {Number}
 от {BeginDate.ToFormatDate()}
");

            yield return new ReplacebelTextByBookmark(LandlordAdditinalDocumentRequisitesBookmark, landlordRequisites.HeaderOfRequisites);
            yield return new ReplacebelTextByBookmark(LandlordAdditinalDocumentRequisitesEndBookmark, landlordRequisites.EndOfRequisites);

            yield return new ReplacebelTextByBookmark(RenterAdditinalDocumentRequisitesBookmark, renterRequisites.HeaderOfRequisites);
            yield return new ReplacebelTextByBookmark(RenterAdditinalDocumentRequisitesEndBookmark, renterRequisites.EndOfRequisites);

            var images = new List<string>();

            foreach (var office in LeaseContract.Offices)
            {
                images.Add($"{Environment.CurrentDirectory}\\..\\..\\Images\\{office.ImagePath}");
            }

            yield return new ReplacebelImageByBookmark(ImagePlaceBookmark, images.ToArray());
        }
    }
}