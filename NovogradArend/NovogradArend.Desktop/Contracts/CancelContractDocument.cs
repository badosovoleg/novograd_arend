﻿using NovogradArend.Desktop.Contracts.Components;
using NovogradArend.Desktop.Extensions;
using NovogradArend.Model;
using NovogradArend.WordWorker;
using System;
using System.Collections.Generic;
using NovogradArend.WordWorker.Interfaces;
using static NovogradArend.WordWorker.WordBookmarks;

namespace NovogradArend.Desktop.Contracts
{
    /// <summary>
    /// Документ отказа
    /// </summary>
    public class CancelContractDocument : BaseContractDocument
    {
        public override ContractTypeEnum ContractType => ContractTypeEnum.Cancel;

        private string _nameOfDOCXTemplate = null;

        protected override string NameOfDOCXTemplate
        {
            get
            {
                if (_nameOfDOCXTemplate == null) _nameOfDOCXTemplate = "CancelContractTemplate";
                return _nameOfDOCXTemplate;
            }
        }

        public int Id { get; set; }
        public DateTime BeginDate { get; set; }
        public string MailNumber { get; set; }
        public DateTime MailDate { get; set; }
        public Entity Landlord { get; set; }
        public Entity Renter { get; set; }
        public Building Building { get; set; }

        public LeaseСontractDocument LeaseContract { get; set; }

        public override IEnumerable<IReplacableBookmark> GetParametresForReplace()
        {
            yield return new ReplacebelTextByBookmark(LandlordNameBookmark, Landlord.Name);
            yield return new ReplacebelTextByBookmark(LandlordRepresentedBookmark, Landlord.Represented);
            yield return new ReplacebelTextByBookmark(LandlordAgreementBookmark, Landlord.Agreement);

            yield return new ReplacebelTextByBookmark(RenterNameBookmark, Renter.Name);
            yield return new ReplacebelTextByBookmark(RenterRepresentedBookmark, Renter.Represented);
            yield return new ReplacebelTextByBookmark(RenterAgreementBookmark, Renter.Agreement);

            yield return new ReplacebelTextByBookmark(MailDateBookmark, MailDate.ToShortDateString());
            yield return new ReplacebelTextByBookmark(MailNumberBookmark, MailNumber);

            yield return new ReplacebelTextByBookmark(beginDateTopBookmark, BeginDate.ToFormatDate());

            var landlordRequisites = new RequisitesBuilder(Landlord);
            var renterRequisites = new RequisitesBuilder(Renter);

            yield return new ReplacebelTextByBookmark(LandlordSomeShortRequisitesBookmark, landlordRequisites.HeaderOfShortRequisites);

            yield return new ReplacebelTextByBookmark(RenterSomeShortRequisitesBookmark, renterRequisites.HeaderOfShortRequisites);
            yield return new ReplacebelTextByBookmark(LandlordRequisitesEndBookmark, renterRequisites.EndOfRequisites);

            yield return new ReplacebelTextByBookmark(BuildingAddressBookmark, Building.Address);
        }
    }
}