﻿using System;
using System.Text;

namespace NovogradArend.Desktop.Contracts.Components
{
    public class RusNumber
    {
        /// <summary>
        /// Перевод в строку числа с учётом падежного окончания относящегося к числу существительного
        /// </summary>
        /// <param name="val">Число</param>
        /// <param name="male">Род существительного, которое относится к числу</param>
        /// <param name="one">Форма существительного в единственном числе</param>
        /// <param name="two">Форма существительного от двух до четырёх</param>
        /// <param name="five">Форма существительного от пяти и больше</param>
        /// <returns></returns>
        private static string Str(int val, bool male, string one, string two, string five)
        {
            int num = val % 1000;
            if (0 == num) return "";
            if (num < 0) throw new ArgumentOutOfRangeException("val", "Параметр не может быть отрицательным");

            //Наименование едениц
            string[] units =
            {
                "", "один ", "два ", "три ", "четыре ", "пять ", "шесть ",
                "семь ", "восемь ", "девять ", "десять ", "одиннадцать ",
                "двенадцать ", "тринадцать ", "четырнадцать ", "пятнадцать ",
                "шестнадцать ", "семнадцать ", "восемнадцать ", "девятнадцать "
            };

            //Наименования десятков
            string[] tens =
            {
                "", "десять ", "двадцать ", "тридцать ", "сорок ", "пятьдесят ",
                "шестьдесят ", "семьдесят ", "восемьдесят ", "девяносто "
            };

            //Наименования сотен
            string[] hunds =
            {
                "", "сто ", "двести ", "триста ", "четыреста ",
                "пятьсот ", "шестьсот ", "семьсот ", "восемьсот ", "девятьсот "
            };

            if (!male)
            {
                units[1] = "одна ";
                units[2] = "две ";
            }

            StringBuilder r = new StringBuilder(hunds[num / 100]);

            if (num % 100 < 20)
            {
                r.Append(units[num % 100]);
            }
            else
            {
                r.Append(tens[num % 100 / 10]);
                r.Append(units[num % 10]);
            }

            r.Append(Case(num, one, two, five));

            if (r.Length != 0) r.Append(" ");
            return r.ToString();
        }

        /// <summary>
        /// Выбор правильного падежного окончания сущесвительного
        /// </summary>
        /// <param name="val">Число</param>
        /// <param name="one">Форма существительного в единственном числе</param>
        /// <param name="two">Форма существительного от двух до четырёх</param>
        /// <param name="five">Форма существительного от пяти и больше</param>
        /// <returns>Возвращает существительное с падежным окончанием, которое соответсвует числу</returns>
        private static string Case(int val, string one, string two, string five)
        {
            int t = (val % 100 > 20) ? val % 10 : val % 20;

            switch (t)
            {
                case 1: return one;
                case 2: case 3: case 4: return two;
                default: return five;
            }
        }

        /// <summary>
        /// Перевод целого числа в строку
        /// </summary>
        /// <param name="val">Число</param>
        /// <returns>Возвращает строковую запись числа</returns>
        private static string Str(int val)
        {
            bool minus = false;
            if (val < 0) { val = -val; minus = true; }

            int n = (int)val;

            StringBuilder r = new StringBuilder();

            if (0 == n) r.Append("0 ");
            if (n % 1000 != 0)
                r.Append(RusNumber.Str(n, true, "", "", ""));

            n /= 1000;

            r.Insert(0, RusNumber.Str(n, false, "тысяча", "тысячи", "тысяч"));
            n /= 1000;

            r.Insert(0, RusNumber.Str(n, true, "миллион", "миллиона", "миллионов"));
            n /= 1000;

            r.Insert(0, RusNumber.Str(n, true, "миллиард", "миллиарда", "миллиардов"));
            n /= 1000;

            r.Insert(0, RusNumber.Str(n, true, "триллион", "триллиона", "триллионов"));
            n /= 1000;

            r.Insert(0, RusNumber.Str(n, true, "триллиард", "триллиарда", "триллиардов"));
            if (minus) r.Insert(0, "минус ");

            //Делаем первую букву заглавной
            r[0] = char.ToUpper(r[0]);

            // убираем пробелы в конце строки
            var count = 0;
            for (int i = r.Length - 1; char.IsWhiteSpace(r[i]); i--, count++) ;
            r.Remove(r.Length - count, count);

            return r.ToString();
        }

        /// <summary>
        /// Перевод целого числа в строку формата ЧИСЛО (ТЕКСТ ТЕКСТ ...) РУБЛЬ(ЕЙ)
        /// </summary>
        /// <param name="val">Число</param>
        /// <returns>Возвращает строковую запись числа</returns>
        public static string BuildString(decimal val)
        {
            var frac = (int)(Math.Round(val - Math.Truncate(val), 2) * 100);
            string stringfrac = frac.ToString();
            if (frac == 0) {
                stringfrac = "00";
            }

            bool minus = false;
            if (val < 0) { val = -val; minus = true; }

            val = Math.Truncate(val);

            int n = (int)val;

            StringBuilder r = new StringBuilder();

            if (0 == n) r.Append("0 ");
            if (n % 1000 != 0)
                r.Append(Str(n, true, "", "", ""));

            n /= 1000;

            r.Insert(0, Str(n, false, "тысяча", "тысячи", "тысяч"));
            n /= 1000;

            r.Insert(0, Str(n, true, "миллион", "миллиона", "миллионов"));
            n /= 1000;

            r.Insert(0, Str(n, true, "миллиард", "миллиарда", "миллиардов"));
            n /= 1000;

            r.Insert(0, Str(n, true, "триллион", "триллиона", "триллионов"));
            n /= 1000;

            r.Insert(0, Str(n, true, "триллиард", "триллиарда", "триллиардов"));
            if (minus) r.Insert(0, "минус ");

            //Делаем первую букву заглавной
            //r[0] = char.ToUpper(r[0]);

            // убираем пробелы в конце строки
            var count = 0;
            for (int i = r.Length - 1; char.IsWhiteSpace(r[i]); i--, count++) ;
            r.Remove(r.Length - count, count);

            // нужно выбрать нужную приставку для рублей (Рубль, Рубля, Рублей)
            var startIndexOfLastWord = 0;
            for (startIndexOfLastWord = r.Length - 1; startIndexOfLastWord > 0 && !char.IsWhiteSpace(r[startIndexOfLastWord - 1]); startIndexOfLastWord--) ;

            var lastWord = new char[r.Length - startIndexOfLastWord];

            r.CopyTo(startIndexOfLastWord, lastWord, 0, lastWord.Length);

            var appendedString = "рублей";

            var lastWordAsString = new string(lastWord);

            if (lastWordAsString.Equals("один"))
                appendedString = "рубль";
            else if (lastWordAsString.Equals("два") || lastWordAsString.Equals("три") || lastWordAsString.Equals("четыре"))
                appendedString = "рубля";

            
            return $"{val} ({r.ToString()}) {appendedString} {stringfrac} коп.";
        }
    }
}