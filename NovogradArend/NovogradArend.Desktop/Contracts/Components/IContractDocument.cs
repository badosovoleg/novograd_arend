﻿namespace NovogradArend.Desktop.Contracts.Components
{
    public interface IContractDocument
    {
        void OpenAsFinalWordDocument();
    }
}
