﻿using NovogradArend.Model;
using NovogradArend.WordWorker;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using NovogradArend.WordWorker.Interfaces;

namespace NovogradArend.Desktop.Contracts.Components
{
    public abstract class BaseContractDocument : IContractDocument
    {
        private readonly WordWoker _wordWorker;

        protected BaseContractDocument()
        {
            _wordWorker = new WordWoker();
        }

        public void OpenAsFinalWordDocument()
        {
            var templatePathObj = Path.GetFullPath($"{ConfigurationManager.AppSettings["TemplatesPath"]}{NameOfDOCXTemplate}.docx");
            _wordWorker.OpenAndModify(templatePathObj, GetParametresForReplace());
        }

        protected abstract string NameOfDOCXTemplate { get; }

        public abstract ContractTypeEnum ContractType { get; }

        public abstract IEnumerable<IReplacableBookmark> GetParametresForReplace();
    }
}
