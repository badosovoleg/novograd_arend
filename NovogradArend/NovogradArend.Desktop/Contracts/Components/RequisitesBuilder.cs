﻿using NovogradArend.Desktop.ViewModels.Dialogs;
using NovogradArend.Model;
using System;

namespace NovogradArend.Desktop.Contracts.Components
{
    public class RequisitesBuilder
    {
        public string HeaderOfRequisites { get; }

        public string HeaderOfShortRequisites { get; }

        public string EndOfRequisites { get; }

        public string EndOfShortRequisites { get; }

        public string SomeShortRequisites { get; }

        public RequisitesBuilder(Entity entity)
        {
            HeaderOfRequisites =
                $"{entity.ShortName}" + Environment.NewLine +
                (entity.INN != null ? $"ИНН {entity.INN}" + Environment.NewLine : "") +
                (entity.OGRN != null ? $"ОГРН {entity.OGRN}" + Environment.NewLine : "") +
                (entity.Address != null ? $"{entity.Address}" + Environment.NewLine : "") +
                (entity.RS != null ? $"р/с {entity.RS}" + Environment.NewLine : "") +
                (entity.BIK != null ? $"БИК {entity.BIK}" + Environment.NewLine : "") +
                (entity.Email != null ? $"E-mail: {entity.Email}" + Environment.NewLine : "") +
                (entity.Phone != null ? $"Тел. {entity.Phone}" + Environment.NewLine : "");

            EndOfRequisites =
                $"{entity.BaseOn}" + Environment.NewLine +
                $"__________________/{entity.Initials}/" + Environment.NewLine + "м.п";

            HeaderOfShortRequisites =
                $"{entity.ShortName}" + Environment.NewLine +
                $"{entity.BaseOn}" + Environment.NewLine;

            EndOfShortRequisites = $"__________________/{entity.Initials}/" + Environment.NewLine + "м.п";

            SomeShortRequisites = $"{entity.ShortName}" + Environment.NewLine +
                (entity.Address != null ? $"{entity.Address}" + Environment.NewLine : "") +
                (entity.Phone != null ? $"Тел. {entity.Phone}" + Environment.NewLine : "");
        }

        public RequisitesBuilder(AddEditEntityDialogViewModel entity)
        {
            HeaderOfRequisites =
                $"{entity.ShortName}" + Environment.NewLine +
                (entity.INN != null ? $"ИНН {entity.INN}" + Environment.NewLine : "") +
                (entity.OGRN != null ? $"ОГРН {entity.OGRN}" + Environment.NewLine : "") +
                (entity.Address != null ? $"{entity.Address}" + Environment.NewLine : "") +
                (entity.RS != null ? $"р/с {entity.RS}" + Environment.NewLine : "") +
                (entity.BIK != null ? $"БИК {entity.BIK}" + Environment.NewLine : "") +
                (entity.Email != null ? $"E-mail: {entity.Email}" + Environment.NewLine : "") +
                (entity.Phone != null ? $"Тел. {entity.Phone}" + Environment.NewLine : "");

            EndOfRequisites =
                $"{entity.BaseOn}" + Environment.NewLine +
                $"__________________/{entity.Initials}/" + Environment.NewLine + "м.п";

            HeaderOfShortRequisites =
                $"{entity.ShortName}" + Environment.NewLine +
                $"{entity.BaseOn}" + Environment.NewLine;

            EndOfShortRequisites = $"__________________/{entity.Initials}/" + Environment.NewLine + "м.п";

            SomeShortRequisites = $"{entity.ShortName}" + Environment.NewLine +
               (entity.Address != null ? $"{entity.Address}" + Environment.NewLine : "") +
               (entity.Phone != null ? $"Тел. {entity.Phone}" + Environment.NewLine : "");
        }

        public string GetFullRequisites() => $"{HeaderOfRequisites}{EndOfRequisites}";

        public string GetShortRequisites() => $"{HeaderOfShortRequisites}{EndOfShortRequisites}";
    }
}