﻿using NovogradArend.Desktop.Services;
using NovogradArend.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NovogradArend.Desktop.Contracts.Components
{
    public static class ContractsExtensions
    {
        #region Helpers

        private static List<LeaseContractOffice> GetLeaseContractOffices(this IObjectsService _service, LeaseContract contract)
            => _service
              .GetLeaseContractOfficesAsync()
              .Result
              .Where(o => o.LeaseContractId == contract.Id)
              .ToList();

        private static List<UtilityServiceContractOffice> GetUtilityServiceContractOffices(this IObjectsService _service, UtilityServiceContract contract)
           => _service
             .GetUtilityServiceContractOfficesAsync()
             .Result
             .Where(o => o.UtilityServiceContractId == contract.Id)
             .ToList();

        private static List<Office> GetOffices(this IObjectsService _service, LeaseContract contract)
        {
            var contractOffices = _service.GetLeaseContractOffices(contract);
            return _service.GetOfficesAsync().Result.Where(o => contractOffices.Any(item => item.OfficeId == o.Id)).ToList();
        }

        private static List<Office> GetOffices(this IObjectsService _service, UtilityServiceContract contract)
        {
            var contractOffices = _service.GetUtilityServiceContractOffices(contract);
            return _service.GetOfficesAsync().Result.Where(o => contractOffices.Any(item => item.OfficeId == o.Id)).ToList();
        }

        #endregion Helpers

        public static List<Office> GetFreeOfices(this IObjectsService _service)
        {
            var actualLeasecontract = _service.GetLeaseContractsAsync().Result.Where(o => o.IsActual == true).ToList();

            //занятые офисы по договорам
            var freeOfficesIds = _service.GetLeaseContractOfficesAsync().Result
                .Where(o => actualLeasecontract.Select(t => t.Id).Contains(o.LeaseContractId))
                .Select(p => p.OfficeId).ToList();

            return _service
                .GetOfficesAsync()
                .Result.Where(o => !freeOfficesIds.Any(p => p == o.Id))
                .ToList();
        }

        public static void UpdateLeaseContractOffices(this IObjectsService _service, LeaseContract contract, Office[] offices)
        {
            // 1 получаем коллекцию офисов которые притянуты к этому контракту 2 делаем пересечение,
            // и сооответственно либо добавляем в базу новый офис, либо удаляем из базы офис которого
            // уже нет.

            var currentOffices = _service.GetLeaseContractOffices(contract);

            List<LeaseContractOffice> officesForRemove = new List<LeaseContractOffice>();

            // проверка на то что мы удалили офис при обновлении состояния LeaseContract
            foreach (var office in currentOffices)
            {
                var isContains = offices.SingleOrDefault(o => o.Id == office.OfficeId && office.LeaseContractId == contract.Id);
                if (isContains == null) officesForRemove.Add(office);
            }

            foreach (var office in officesForRemove)
            {
                var result = _service.RemoveLeaseContractOfficeAsync(office).Result;
            }

            // актуализируем состояние после удаления (можно и не делать)
            currentOffices = _service.GetLeaseContractOffices(contract);

            List<LeaseContractOffice> officesForAdd = new List<LeaseContractOffice>();

            // проверяем есть ли у нас новый офис в сравнении с состоянием базы
            foreach (var office in offices)
            {
                var isContains = currentOffices.SingleOrDefault(o => o.LeaseContractId == contract.Id && o.OfficeId == office.Id);
                if (isContains == null) officesForAdd.Add(new LeaseContractOffice { LeaseContractId = contract.Id, OfficeId = office.Id });
            }

            foreach (var office in officesForAdd)
            {
                var result = _service.AddLeaseContractOfficeAsync(office).Result;
            }
        }

        public static void UpdateUtilityServiceOffices(this IObjectsService _service, UtilityServiceContract contract, Office[] offices)
        {
            // 1 получаем коллекцию офисов которые притянуты к этому контракту 2 делаем пересечение,
            // и сооответственно либо добавляем в базу новый офис, либо удаляем из базы офис которого
            // уже нет.

            var currentOffices = _service.GetUtilityServiceContractOffices(contract);

            List<UtilityServiceContractOffice> officesForRemove = new List<UtilityServiceContractOffice>();

            // проверка на то что мы удалили офис при обновлении состояния UtilityServiceContract
            foreach (var office in currentOffices)
            {
                var isContains = offices.SingleOrDefault(o => o.Id == office.OfficeId && office.UtilityServiceContractId == contract.Id);
                if (isContains == null) officesForRemove.Add(office);
            }

            foreach (var office in officesForRemove)
            {
                var result = _service.RemoveUtilityServiceContractOfficeAsync(office).Result;
            }

            // актуализируем состояние после удаления (можно и не делать)
            currentOffices = _service.GetUtilityServiceContractOffices(contract);

            List<UtilityServiceContractOffice> officesForAdd = new List<UtilityServiceContractOffice>();

            // проверяем есть ли у нас новый офис в сравнении с состоянием базы
            foreach (var office in offices)
            {
                var isContains = currentOffices.SingleOrDefault(o => o.UtilityServiceContractId == contract.Id && o.OfficeId == office.Id);
                if (isContains == null) officesForAdd.Add(new UtilityServiceContractOffice { UtilityServiceContractId = contract.Id, OfficeId = office.Id });
            }

            foreach (var office in officesForAdd)
            {
                var result = _service.AddUtilityServiceContractOfficeAsync(office).Result;
            }
        }

        #region Models to documents

        public static LeaseСontractDocument GetLeaseСontractDocument(this IObjectsService _service, LeaseContract contract)
        {
            var building = _service.GetBuildingsAsync().Result.Single(e => e.Id == contract.BuildingId);

            var entities = _service.GetEntitiesAsync().Result;

            var offices = _service.GetOffices(contract);

            return new LeaseСontractDocument()
            {
                Id = contract.Id,
                Number = contract.Number,
                BeginDate = DateTime.Parse(contract.BeginDate),
                Landlord = entities.Single(e => e.Id == contract.LandLordId),
                Renter = entities.Single(e => e.Id == contract.RenterId),
                Building = building,
                CountLandlordDay = contract.CountLandLordDay,
                CountRenterDay = contract.CountRenterDay,
                Garantee = contract.GaranteeNumber != null ?
                            new Garantee(contract.GaranteeNumber, DateTime.Parse(contract.GaranteeDate), building.Address, contract.TotalSquare)
                            : null,
                ForPlacement = contract.ForPlacement,
                TotalPrice = contract.TotalPrice,
                TotalSquare = contract.TotalSquare,
                Act1ContractDocument = new Act1ContractDocument() { BeginDate = DateTime.Parse(contract.Act1BeginDate) },
                Offices = offices.ToArray(),
                PriceForOneSquareMeter = contract.Price,
                EndDate = DateTime.Parse(contract.EndDate),
                IsActual = contract.IsActual
            };
        }

        public static UtilityServiceContractDocument GetUtilityServiceContractDocument(this IObjectsService _service, UtilityServiceContract contract)
        {
            var leaseContractInfo = _service.GetLeaseContractAsync(contract.LeaseContractId).Result;

            var entities = _service.GetEntitiesAsync().Result;

            var offices = _service.GetOffices(contract);

            return new UtilityServiceContractDocument
            {
                Id = contract.Id,
                BeginDate = DateTime.Parse(contract.BeginDate),
                LeaseContract = _service.GetLeaseСontractDocument(leaseContractInfo),
                ManagerCompany = entities.Single(e => contract.ManagerCompanyId == e.Id),
                Number = contract.Number,
                Renter = entities.Single(e => contract.RenterId == e.Id),
                UtilityServiceOffices = offices.ToArray(),
                UtilityServicePrice = contract.UtilityServicePrice,
                UtilityServiceTotalPrice = contract.UtilityServiceTotalPrice
            };
        }

        public static Act2ContractDocument GetAct2ContractDocument(this IObjectsService _service, Act2Contract contract)
        {
            var leaseContractInfo = _service.GetLeaseContractAsync(contract.LeaseContractId).Result;

            return new Act2ContractDocument
            {
                Id = contract.Id,
                BeginDate = DateTime.Parse(contract.BeginDate),
                LeaseContract = _service.GetLeaseСontractDocument(leaseContractInfo),
                Number = contract.Number
            };
        }

        public static Act1ContractDocument GetAct1ContractDocument(this IObjectsService _service, Act1Contract contract)
        {
            var leaseContractInfo = _service.GetLeaseContractAsync(contract.LeaseContractId).Result;

            return new Act1ContractDocument()
            {
                Id = contract.Id,
                BeginDate = DateTime.Parse(contract.BeginDate),
                LeaseContract = _service.GetLeaseСontractDocument(leaseContractInfo),
                Number = contract.Number
            };
        }

        public static ClaimContractDocument GetClaimContractDocument(this IObjectsService _service, ClaimContract contract)
        {
            var leaseContractInfo = _service.GetLeaseContractAsync(contract.LeaseContractId).Result;

            var utilityServiceContractInfo = _service.GetUtilityServiceContractsAsync().Result.Single(c => c.Id == contract.UtilityServiceContractId);

            return new ClaimContractDocument
            {
                Id = contract.Id,
                Number = contract.Number,
                BeginDate = DateTime.Parse(contract.BeginDate),
                LeaseContract = _service.GetLeaseСontractDocument(leaseContractInfo),
                ArendDebt = contract.ArendDebt,
                UtilityServiceContract = _service.GetUtilityServiceContractDocument(utilityServiceContractInfo),
                UtilityServiceDebt = contract.UtilityServiceDebt
            };
        }

        public static ChangeArendPriceContractDocument GetChangeArendPriceContractDocument(this IObjectsService _service, ChangeArendPriceContract contract)
        {
            var leaseContractInfo = _service.GetLeaseContractAsync(contract.LeaseContractId).Result;

            return new ChangeArendPriceContractDocument
            {
                Id = contract.Id,
                BeginDate = DateTime.Parse(contract.BeginDate),
                Number = contract.Number,
                LeaseContract = _service.GetLeaseСontractDocument(leaseContractInfo),
                NewOfficePrice = contract.NewOfficePrice,
                NewOfficeTotalPrice = contract.NewOfficeTotalPrice
            };
        }

        public static TerminationContractDocument GetTerminationContractDocument(this IObjectsService _service, TerminationContract contract)
        {
            var leaseContractInfo = _service.GetLeaseContractAsync(contract.LeaseContractId).Result;

            return new TerminationContractDocument
            {
                Id = contract.Id,
                BeginDate = DateTime.Parse(contract.BeginDate),
                LeaseContract = _service.GetLeaseСontractDocument(leaseContractInfo)
            };
        }

        public static CancelContractDocument GetCancelContractDocument(this IObjectsService _service, CancelContract contract)
        {
            var leaseContractInfo = _service.GetLeaseContractAsync(contract.LeaseContractId).Result;

            var buildings = _service.GetBuildingsAsync().Result;

            var entities = _service.GetEntitiesAsync().Result;

            return new CancelContractDocument
            {
                Id = contract.Id,
                BeginDate = DateTime.Parse(contract.BeginDate),
                Building = buildings.Single(b => b.Id == contract.BuildingId),
                Renter = entities.Single(e => e.Id == contract.RenterId),
                Landlord = entities.Single(e => e.Id == contract.LandLordId),
                MailDate = DateTime.Parse(contract.MailDate),
                MailNumber = contract.MailNumber,
                LeaseContract = _service.GetLeaseСontractDocument(leaseContractInfo)
            };
        }

        public static LeaseContract DocumentToModel(LeaseСontractDocument leaseСontractDocument)
            => new LeaseContract
            {
                Id = leaseСontractDocument.Id,
                Number = leaseСontractDocument.Number,
                IsActual = leaseСontractDocument.IsActual,
                BeginDate = leaseСontractDocument.BeginDate.ToShortDateString(),
                BuildingId = leaseСontractDocument.Building.Id,
                CountLandLordDay = leaseСontractDocument.CountLandlordDay,
                Act1BeginDate = leaseСontractDocument.Act1ContractDocument.BeginDate.ToShortDateString(),
                CountRenterDay = leaseСontractDocument.CountRenterDay,
                EndDate = leaseСontractDocument.EndDate.ToShortDateString(),
                ForPlacement = leaseСontractDocument.ForPlacement,
                GaranteeDate = leaseСontractDocument.Garantee?.Date.ToShortDateString(),
                GaranteeNumber = leaseСontractDocument.Garantee?.Number,
                LandLordId = leaseСontractDocument.Landlord.Id,
                Price = leaseСontractDocument.PriceForOneSquareMeter,
                RenterId = leaseСontractDocument.Renter.Id,
                TotalPrice = leaseСontractDocument.TotalPrice,
                TotalSquare = leaseСontractDocument.TotalSquare
            };

        #endregion Models to documents
    }
}