﻿using NovogradArend.Desktop.Contracts.Components;
using NovogradArend.Desktop.Extensions;
using NovogradArend.Model;
using NovogradArend.WordWorker;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using NovogradArend.WordWorker.Interfaces;

namespace NovogradArend.Desktop.Contracts
{
    /// <summary>
    /// Договор аренды
    /// </summary>
    public class LeaseСontractDocument : BaseContractDocument
    {
        public string Number { get; set; }

        public int Id { get; set; }

        public DateTime BeginDate { get; set; } = DateTime.MinValue;

        private DateTime _endDate;
        public DateTime EndDate
        {
            get
            {
                if (_endDate != null) return _endDate;

                if (BeginDate == DateTime.MinValue)
                    return DateTime.MinValue;
                else
                {
                    var date = BeginDate;
                    date.AddMonths(11);
                    return date;
                }
            }
            set
            {
                _endDate = value;
            }
        }

        public Entity Landlord { get; set; }

        public Entity Renter { get; set; }

        public Office[] Offices { get; set; }

        public string OfficesAsString => string.Join(", ", Offices.Select(o => o.Number));

        public Building Building { get; set; }

        public int CountLandlordDay { get; set; }

        public int CountRenterDay { get; set; }

        public Garantee Garantee { get; set; } = null;

        public string ForPlacement { get; set; }

        public decimal TotalPrice { get; set; }

        public decimal PriceForOneSquareMeter { get; set; }

        public decimal TotalSquare { get; set; }

        public Act1ContractDocument Act1ContractDocument { get; set; }

        public bool IsActual { get; set; }

        private string _nameOfDOCXTemplate = null;
        

        protected override string NameOfDOCXTemplate
        {
            get
            {
                if (_nameOfDOCXTemplate == null)
                    if (Offices.Length > 1) _nameOfDOCXTemplate = "LeaseContractTemplateForMany";
                    else _nameOfDOCXTemplate = "LeaseContractTemplate";
                return _nameOfDOCXTemplate;
            }
        }

        public override ContractTypeEnum ContractType => ContractTypeEnum.Lease;

        public override IEnumerable<IReplacableBookmark> GetParametresForReplace()
        {
            yield return new ReplacebelTextByBookmark(WordBookmarks.NumberBookmark, Number);

            yield return new ReplacebelTextByBookmark(WordBookmarks.BeginDateBookmark, BeginDate.ToFormatDate());
            yield return new ReplacebelTextByBookmark(WordBookmarks.beginDateTopBookmark, BeginDate.ToFormatDate());

            yield return new ReplacebelTextByBookmark(WordBookmarks.LandlordNameBookmark, Landlord.Name);
            yield return new ReplacebelTextByBookmark(WordBookmarks.LandlordRepresentedBookmark, Landlord.Represented);
            yield return new ReplacebelTextByBookmark(WordBookmarks.LandlordAgreementBookmark, Landlord.Agreement);

            yield return new ReplacebelTextByBookmark(WordBookmarks.RenterNameBookmark, Renter.Name);
            yield return new ReplacebelTextByBookmark(WordBookmarks.RenterRepresentedBookmark, Renter.Represented);
            yield return new ReplacebelTextByBookmark(WordBookmarks.RenterAgreementBookmark, Renter.Agreement);

            yield return new ReplacebelTextByBookmark(WordBookmarks.BuildingAddressBookmark, Building.Address);

            string floor = Offices[0].Floor.ToString();

            yield return new ReplacebelTextByBookmark(WordBookmarks.OfficeFloorBookmark, Offices[0].Floor);

            string numbersOnFloorPlan = Offices.First().NumberOnFloorPlan.ToString();

            for (int i = 1; i < Offices.Length; i++)
            {
                numbersOnFloorPlan += $", {Offices[i].NumberOnFloorPlan}";
            }

            yield return new ReplacebelTextByBookmark(WordBookmarks.OfficeNumberOnFloorPlanBookmark,
                numbersOnFloorPlan);

            string numbers = Offices.First().Number.ToString();

            for (int i = 1; i < Offices.Length; i++)
            {
                numbers += $", {Offices[i].Number}";
            }

            yield return new ReplacebelTextByBookmark(WordBookmarks.OfficeNumberBookmark, numbers);

            yield return new ReplacebelTextByBookmark(WordBookmarks.OfficeSquareBookmark, TotalSquare);

            yield return new ReplacebelTextByBookmark(WordBookmarks.OfficeForPlacementBookmark, ForPlacement);

            var officeInfo = Offices.First();
            var ownershipAndCadastralString = string.Empty;

            if (officeInfo.OwnCadatrlOutType == 1)
            {
                ownershipAndCadastralString =
                    $@"Указанное помещение принадлежит арендодателю на праве собственности, что подтверждается Свидетельством о государственной регистрации права {officeInfo.Ownership}, о чем в Едином государственном реестре прав на недвижимое имущество и сделок с ним сделана запись регистрации {officeInfo.RegistrationCertificate} (кадастровый номер: {officeInfo.СadastralNumber})";
            }
            else if (officeInfo.OwnCadatrlOutType == 2)
            {
                ownershipAndCadastralString =
                    $@"Арендодатель имеет право распоряжения помещением на основании {officeInfo.Ownership} собственников помещений здания по адресу: г.Новосибирск, Красный проспект, д.182 {officeInfo.RegistrationCertificate}";
            }

            yield return new ReplacebelTextByBookmark(WordBookmarks.OfficeOwnershipBookmark, ownershipAndCadastralString);

            yield return new ReplacebelTextByBookmark(WordBookmarks.OfficePriceBookmark, RusNumber.BuildString(TotalPrice / TotalSquare));

            yield return new ReplacebelTextByBookmark(WordBookmarks.TotalPriceBookmark, RusNumber.BuildString(TotalPrice));

            var landlordRequisites = new RequisitesBuilder(Landlord);
            var renterRequisites = new RequisitesBuilder(Renter);

            yield return new ReplacebelTextByBookmark(WordBookmarks.LandlordRequisitesBookmark, landlordRequisites.HeaderOfRequisites);
            yield return new ReplacebelTextByBookmark(WordBookmarks.LandlordRequisitesEndBookmark, landlordRequisites.EndOfRequisites);

            yield return new ReplacebelTextByBookmark(WordBookmarks.RenterRequisitesBookmark, renterRequisites.HeaderOfRequisites);
            yield return new ReplacebelTextByBookmark(WordBookmarks.RenterRequisitesEndBookmark, renterRequisites.EndOfRequisites);

            yield return new ReplacebelTextByBookmark(WordBookmarks.CountRenterDayBookmark, CountRenterDay);

            yield return new ReplacebelTextByBookmark(WordBookmarks.CountRenterDay2Bookmark, CountRenterDay);

            yield return new ReplacebelTextByBookmark(WordBookmarks.CountLandlordDayBookmark, CountLandlordDay);
            yield return new ReplacebelTextByBookmark(WordBookmarks.CountLandlordDayBookmark2, CountLandlordDay);

            yield return new ReplacebelTextByBookmark(WordBookmarks.Act1BeginDateBookmark, Act1ContractDocument.BeginDate.ToFormatDate());

            if (Garantee != null)
                yield return new ReplacebelTextByBookmark(WordBookmarks.GaranteeBookmark, Garantee.Result);

            yield return new ReplacebelTextByBookmark(WordBookmarks.AdditinalDocumentHeaderBookmark, $@"Приложение №1
к Договору аренды № {Number}
 от {BeginDate.ToFormatDate()}
");
            yield return new ReplacebelTextByBookmark(WordBookmarks.LandlordAdditinalDocumentRequisitesBookmark, landlordRequisites.HeaderOfShortRequisites);
            yield return new ReplacebelTextByBookmark(WordBookmarks.LandlordAdditinalDocumentRequisitesEndBookmark, landlordRequisites.EndOfShortRequisites);

            yield return new ReplacebelTextByBookmark(WordBookmarks.RenterAdditinalDocumentRequisitesBookmark, renterRequisites.HeaderOfShortRequisites);
            yield return new ReplacebelTextByBookmark(WordBookmarks.RenterAdditinalDocumentRequisitesEndBookmark, renterRequisites.EndOfShortRequisites);

            //yield return new ReplacebelImageByBookmark(WordBookmarks.ImagePlaceBookmark, Offices.Select(office => Path.GetFullPath($"{ConfigurationManager.AppSettings["ImagesPath"]}{office.ImagePath}")).ToArray());
        }

        public override string ToString() => $"{Number} от {BeginDate.ToFormatDate()}";
    }
}