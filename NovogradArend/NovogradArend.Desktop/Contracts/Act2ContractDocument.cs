﻿using NovogradArend.Desktop.Contracts.Components;
using NovogradArend.Desktop.Extensions;
using NovogradArend.Model;
using NovogradArend.WordWorker;
using System;
using System.Collections.Generic;
using NovogradArend.WordWorker.Interfaces;

namespace NovogradArend.Desktop.Contracts
{
    /// <summary>
    /// АКТ №2 приема-передачи нежилого помещения к Договору аренды
    /// </summary>
    public class Act2ContractDocument : BaseContractDocument
    {
        public int Id { get; set; }
      
        public string Number { get; set; }

        public DateTime BeginDate { get; set; } = DateTime.MinValue;

        public LeaseСontractDocument LeaseContract { get; set; }

        private Entity Landlord => LeaseContract.Landlord;

        private Entity Renter => LeaseContract.Renter;

        private DateTime LeaseContractEndDate => LeaseContract.EndDate;

        public override ContractTypeEnum ContractType => ContractTypeEnum.ACT2;

        private string _nameOfDOCXTemplate = null;
        protected override string NameOfDOCXTemplate
        {
            get
            {
                if (_nameOfDOCXTemplate == null) _nameOfDOCXTemplate = "Act2ContractTemplate";
                return _nameOfDOCXTemplate;
            }
        }

        public override IEnumerable<IReplacableBookmark> GetParametresForReplace()
        {
            var beginDate = LeaseContract.BeginDate.ToFormatDate();

            yield return new ReplacebelTextByBookmark(WordBookmarks.NumberBookmark, LeaseContract.Number);
            yield return new ReplacebelTextByBookmark(WordBookmarks.BeginDateBookmark, beginDate);

            yield return new ReplacebelTextByBookmark(WordBookmarks.Number2Bookmark, LeaseContract.Number);
            yield return new ReplacebelTextByBookmark(WordBookmarks.BeginDate2Bookmark, beginDate);

            yield return new ReplacebelTextByBookmark(WordBookmarks.beginDateTopBookmark, BeginDate.ToFormatDate());

            yield return new ReplacebelTextByBookmark(WordBookmarks.LandlordNameBookmark, Landlord.Name);
            yield return new ReplacebelTextByBookmark(WordBookmarks.LandlordRepresentedBookmark, Landlord.Represented);
            yield return new ReplacebelTextByBookmark(WordBookmarks.LandlordAgreementBookmark, Landlord.Agreement);

            yield return new ReplacebelTextByBookmark(WordBookmarks.RenterNameBookmark, Renter.Name);
            yield return new ReplacebelTextByBookmark(WordBookmarks.RenterRepresentedBookmark, Renter.Represented);
            yield return new ReplacebelTextByBookmark(WordBookmarks.RenterAgreementBookmark, Renter.Agreement);

            yield return new ReplacebelTextByBookmark(WordBookmarks.LeaseContractEndDateBookmark, LeaseContractEndDate.ToShortDateString());

            var landlordRequisites = new RequisitesBuilder(Landlord);
            var renterRequisites = new RequisitesBuilder(Renter);

            yield return new ReplacebelTextByBookmark(WordBookmarks.LandlordRequisitesBookmark, landlordRequisites.HeaderOfRequisites);
            yield return new ReplacebelTextByBookmark(WordBookmarks.LandlordRequisitesEndBookmark, landlordRequisites.EndOfRequisites);

            yield return new ReplacebelTextByBookmark(WordBookmarks.RenterRequisitesBookmark, renterRequisites.HeaderOfRequisites);
            yield return new ReplacebelTextByBookmark(WordBookmarks.RenterRequisitesEndBookmark, renterRequisites.EndOfRequisites);
        }
    }
}