﻿using NovogradArend.Desktop.Contracts.Components;
using NovogradArend.Desktop.Extensions;
using NovogradArend.Model;
using NovogradArend.WordWorker;
using System;
using System.Collections.Generic;
using NovogradArend.WordWorker.Interfaces;
using static NovogradArend.WordWorker.WordBookmarks;

namespace NovogradArend.Desktop.Contracts
{
    /// <summary>
    /// Соглашение о расторжении
    /// </summary>
    public class TerminationContractDocument : BaseContractDocument
    {
        public override ContractTypeEnum ContractType => ContractTypeEnum.Termination;

        private string _nameOfDOCXTemplate = null;
        protected override string NameOfDOCXTemplate
        {
            get
            {
                if (_nameOfDOCXTemplate == null) _nameOfDOCXTemplate = "TerminationContractTemplate";
                return _nameOfDOCXTemplate;
            }
        }

        public int Id { get; set; }
        public DateTime BeginDate { get; set; }
        public LeaseСontractDocument LeaseContract { get; set; }

        public override IEnumerable<IReplacableBookmark> GetParametresForReplace()
        {
            var Landlord = LeaseContract.Landlord;

            yield return new ReplacebelTextByBookmark(LandlordNameBookmark, Landlord.Name);
            yield return new ReplacebelTextByBookmark(LandlordRepresentedBookmark, Landlord.Represented);
            yield return new ReplacebelTextByBookmark(LandlordAgreementBookmark, Landlord.Agreement);

            var Renter = LeaseContract.Renter;

            yield return new ReplacebelTextByBookmark(RenterNameBookmark, Renter.Name);
            yield return new ReplacebelTextByBookmark(RenterRepresentedBookmark, Renter.Represented);
            yield return new ReplacebelTextByBookmark(RenterAgreementBookmark, Renter.Agreement);

            var leaseContractNubmer = new[]
          {
                NumberBookmark,
                Number2Bookmark
            };

            foreach (var bookmark in ReplacebelBookmarkCreator.GetManyBookmarks(LeaseContract.Number, leaseContractNubmer))
                yield return bookmark;


            var leaseContactBeginDate = new[]
            {
                BeginDateBookmark,
                BeginDate2Bookmark
            };

            foreach (var bookmark in ReplacebelBookmarkCreator.GetManyBookmarks(LeaseContract.BeginDate.ToFormatDate(), leaseContactBeginDate))
                yield return bookmark;

            yield return new ReplacebelTextByBookmark(WordBookmarks.beginDateTopBookmark, BeginDate.ToFormatDate());
            var landlordRequisites = new RequisitesBuilder(Landlord);
            var renterRequisites = new RequisitesBuilder(Renter);

            yield return new ReplacebelTextByBookmark(LandlordRequisitesBookmark, landlordRequisites.HeaderOfRequisites);
            yield return new ReplacebelTextByBookmark(LandlordRequisitesEndBookmark, landlordRequisites.EndOfRequisites);

            yield return new ReplacebelTextByBookmark(RenterRequisitesBookmark, renterRequisites.HeaderOfRequisites);
            yield return new ReplacebelTextByBookmark(RenterRequisitesEndBookmark, renterRequisites.EndOfRequisites);
        }
    }
}