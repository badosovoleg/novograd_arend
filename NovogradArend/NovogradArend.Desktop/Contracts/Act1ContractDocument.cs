﻿using NovogradArend.Desktop.Contracts.Components;
using NovogradArend.Desktop.Extensions;
using NovogradArend.Model;
using NovogradArend.WordWorker;
using System;
using System.Collections.Generic;
using System.Linq;
using NovogradArend.WordWorker.Interfaces;

namespace NovogradArend.Desktop.Contracts
{
    /// <summary>
    /// АКТ №1 приема-передачи нежилого помещения к Договору аренды
    /// </summary>
    public class Act1ContractDocument : BaseContractDocument
    {
        public int Id { get; set; }

        public string Number { get; set; }

        public DateTime BeginDate { get; set; } = DateTime.MinValue;

        public override ContractTypeEnum ContractType => ContractTypeEnum.ACT1;

        public LeaseСontractDocument LeaseContract { get; set; }

        private Entity Landlord => LeaseContract.Landlord;

        private Entity Renter => LeaseContract.Renter;

        private int OfficeFloor => LeaseContract.Offices.First().Floor;

        private decimal TotalOfficeSquare => LeaseContract.TotalSquare;

        private Building Building => LeaseContract.Building;

        private string ForPlacement => LeaseContract.ForPlacement;

        private string _nameOfDOCXTemplate = null;
        protected override string NameOfDOCXTemplate
        {
            get
            {
                if (_nameOfDOCXTemplate == null) _nameOfDOCXTemplate = "Act1ContractTemplate";
                return _nameOfDOCXTemplate;
            }
        }

        public override IEnumerable<IReplacableBookmark> GetParametresForReplace()
        {
            var beginDate = LeaseContract.BeginDate.ToFormatDate();

            yield return new ReplacebelTextByBookmark(WordBookmarks.NumberBookmark, LeaseContract.Number);
            yield return new ReplacebelTextByBookmark(WordBookmarks.BeginDateBookmark, beginDate);

            yield return new ReplacebelTextByBookmark(WordBookmarks.Number2Bookmark, LeaseContract.Number);
            yield return new ReplacebelTextByBookmark(WordBookmarks.BeginDate2Bookmark, beginDate);

            yield return new ReplacebelTextByBookmark(WordBookmarks.BeginDate3Bookmark, beginDate);

            yield return new ReplacebelTextByBookmark(WordBookmarks.beginDateTopBookmark, BeginDate.ToFormatDate());

            yield return new ReplacebelTextByBookmark(WordBookmarks.LandlordNameBookmark, Landlord.Name);
            yield return new ReplacebelTextByBookmark(WordBookmarks.LandlordRepresentedBookmark, Landlord.Represented);
            yield return new ReplacebelTextByBookmark(WordBookmarks.LandlordAgreementBookmark, Landlord.Agreement);

            yield return new ReplacebelTextByBookmark(WordBookmarks.RenterNameBookmark, Renter.Name);
            yield return new ReplacebelTextByBookmark(WordBookmarks.RenterRepresentedBookmark, Renter.Represented);
            yield return new ReplacebelTextByBookmark(WordBookmarks.RenterAgreementBookmark, Renter.Agreement);

            yield return new ReplacebelTextByBookmark(WordBookmarks.BuildingAddressBookmark, Building.Address);

            yield return new ReplacebelTextByBookmark(WordBookmarks.OfficeFloorBookmark, OfficeFloor);

            yield return new ReplacebelTextByBookmark(WordBookmarks.OfficeSquareBookmark, TotalOfficeSquare);

            yield return new ReplacebelTextByBookmark(WordBookmarks.OfficeForPlacementBookmark, ForPlacement);

            var landlordRequisites = new RequisitesBuilder(Landlord);
            var renterRequisites = new RequisitesBuilder(Renter);

            yield return new ReplacebelTextByBookmark(WordBookmarks.LandlordRequisitesBookmark, landlordRequisites.HeaderOfRequisites);
            yield return new ReplacebelTextByBookmark(WordBookmarks.LandlordRequisitesEndBookmark, landlordRequisites.EndOfRequisites);

            yield return new ReplacebelTextByBookmark(WordBookmarks.RenterRequisitesBookmark, renterRequisites.HeaderOfRequisites);
            yield return new ReplacebelTextByBookmark(WordBookmarks.RenterRequisitesEndBookmark, renterRequisites.EndOfRequisites);
        }
    }
}