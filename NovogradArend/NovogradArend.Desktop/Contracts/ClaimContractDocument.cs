﻿using NovogradArend.Desktop.Contracts.Components;
using NovogradArend.Desktop.Extensions;
using NovogradArend.Model;
using NovogradArend.WordWorker;
using System;
using System.Collections.Generic;
using NovogradArend.WordWorker.Interfaces;
using static NovogradArend.WordWorker.WordBookmarks;

namespace NovogradArend.Desktop.Contracts
{
    /// <summary>
    /// Претензия
    /// </summary>
    public class ClaimContractDocument : BaseContractDocument
    {

        public override ContractTypeEnum ContractType => ContractTypeEnum.Claim;

        private string _nameOfDOCXTemplate = null;
        protected override string NameOfDOCXTemplate
        {
            get
            {
                if (_nameOfDOCXTemplate == null) _nameOfDOCXTemplate = "ClaimContractTemplate";
                return _nameOfDOCXTemplate;
            }
        }

        public int Id { get; set; }
        public string Number { get; set; }
        public DateTime BeginDate { get; set; }
        public LeaseСontractDocument LeaseContract { get; set; }
        public UtilityServiceContractDocument UtilityServiceContract { get; set; }
        public decimal ArendDebt { get; set; }
        public decimal UtilityServiceDebt { get; set; }

        public override IEnumerable<IReplacableBookmark> GetParametresForReplace()
        {
            var landlordRequisites = new RequisitesBuilder(LeaseContract.Landlord);
            var renterRequisites = new RequisitesBuilder(LeaseContract.Renter);

            yield return new ReplacebelTextByBookmark(RenterAgreementBookmark, LeaseContract.Renter.Agreement);
            yield return new ReplacebelTextByBookmark(totalSquareBookmark, LeaseContract.TotalSquare);

            yield return new ReplacebelTextByBookmark(NumberBookmark, Number);

            yield return new ReplacebelTextByBookmark(BeginDateBookmark, BeginDate.ToFormatDate());

            yield return new ReplacebelTextByBookmark(leaseContractBeginDateBookmark, LeaseContract.BeginDate.ToFormatDate());

            yield return new ReplacebelTextByBookmark(RenterNameBookmark, LeaseContract.Renter.Name);

            yield return new ReplacebelTextByBookmark(RenterRepresentedBookmark, LeaseContract.Renter.Represented);

            yield return new ReplacebelTextByBookmark(RenterSomeShortRequisitesBookmark, renterRequisites.SomeShortRequisites);

            yield return new ReplacebelTextByBookmark(leaseContractNumberBookmark, LeaseContract.Number);
            
            yield return new ReplacebelTextByBookmark(utilityServiceNameBookmark, UtilityServiceContract.ManagerCompany.Name);


            yield return new ReplacebelTextByBookmark(utilityServiceContractNumberBookmark, UtilityServiceContract.Number);
            yield return new ReplacebelTextByBookmark(utilityServiceContractBeginDateBookmark, UtilityServiceContract.BeginDate.ToFormatDate());
            yield return new ReplacebelTextByBookmark(buildingNameBookmark, LeaseContract.Building.Name);
            yield return new ReplacebelTextByBookmark(buildingAdressBookmark, LeaseContract.Building.Address);

            yield return new ReplacebelTextByBookmark(numberAct1Bookmark, LeaseContract.Act1ContractDocument.Number);

            yield return new ReplacebelTextByBookmark(beginDateAct1Bookmark, LeaseContract.Act1ContractDocument.BeginDate.ToFormatDate());

            yield return new ReplacebelTextByBookmark(leaseContractPriceBookmark, RusNumber.BuildString(LeaseContract.PriceForOneSquareMeter));
            yield return new ReplacebelTextByBookmark(leaseContracttotalPriceBookmark, RusNumber.BuildString(LeaseContract.TotalPrice));
            yield return new ReplacebelTextByBookmark(utilityServiceContractPriceBookmark, RusNumber.BuildString(UtilityServiceContract.UtilityServiceTotalPrice));
            yield return new ReplacebelTextByBookmark(claimDate1Bookmark, BeginDate.ToFormatDate());
            yield return new ReplacebelTextByBookmark(arendaDebtBookmark, RusNumber.BuildString(ArendDebt));
            yield return new ReplacebelTextByBookmark(utilityServiceDebtBookmark, RusNumber.BuildString(UtilityServiceDebt));
            yield return new ReplacebelTextByBookmark(ClaimDate2Bookmark, BeginDate.ToFormatDate());
            
            yield return new ReplacebelTextByBookmark(landlordName2Bookmark, LeaseContract.Landlord.Name);
            yield return new ReplacebelTextByBookmark(utilityServiceName2Bookmark, UtilityServiceContract.ManagerCompany.Name);
            yield return new ReplacebelTextByBookmark(LandlordAdditinalDocumentRequisitesEndBookmark, landlordRequisites.EndOfRequisites);
        }
    }
}