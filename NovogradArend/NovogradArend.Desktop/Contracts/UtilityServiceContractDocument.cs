﻿using NovogradArend.Desktop.Contracts.Components;
using NovogradArend.Desktop.Extensions;
using NovogradArend.Model;
using NovogradArend.WordWorker;
using System;
using System.Collections.Generic;
using System.Linq;
using NovogradArend.WordWorker.Interfaces;
using static NovogradArend.WordWorker.WordBookmarks;

namespace NovogradArend.Desktop.Contracts
{
    /// <summary>
    /// Договор коммунальное обслуживание
    /// </summary>
    public class UtilityServiceContractDocument : BaseContractDocument
    {
        public override ContractTypeEnum ContractType => ContractTypeEnum.UtilityService;

        private string _nameOfDOCXTemplate = null;
        protected override string NameOfDOCXTemplate
        {
            get
            {
                if (_nameOfDOCXTemplate == null)
                    if (UtilityServiceOffices.Length > 1) _nameOfDOCXTemplate = "UtilityServiceContractTemplateForMany";
                    else _nameOfDOCXTemplate = "UtilityServiceContractTemplate";
                return _nameOfDOCXTemplate;
            }
        }

        public int Id { get; set; }
        public string Number { get; set; }
        public LeaseСontractDocument LeaseContract { get; set; }
        public DateTime BeginDate { get; set; }
        public Entity ManagerCompany { get; set; }
        public Entity Renter { get; set; }
        public decimal UtilityServicePrice { get; set; }
        public decimal UtilityServiceTotalPrice { get; set; }
        public Office[] UtilityServiceOffices { get; set; }
        //public DateTime DocumentDate { get; set; }

        public override IEnumerable<IReplacableBookmark> GetParametresForReplace()
        {
            yield return new ReplacebelTextByBookmark(NumberBookmark, Number);

            var leaseContactBeginDate = new[]
            {
                BeginDateBookmark,
                BeginDate2Bookmark    
            };

            foreach (var bookmark in ReplacebelBookmarkCreator.GetManyBookmarks(LeaseContract.BeginDate.ToFormatDate(), leaseContactBeginDate))
                yield return bookmark;

            yield return new ReplacebelTextByBookmark(beginDateTopBookmark, BeginDate.ToFormatDate());
            yield return new ReplacebelTextByBookmark(ManagerCompanyNameBookmark, ManagerCompany.Name);
            yield return new ReplacebelTextByBookmark(ManagerCompanyRepresentedBookmark, ManagerCompany.Represented);
            yield return new ReplacebelTextByBookmark(ManagerCompanyAgreementBookmark, ManagerCompany.Agreement);

            yield return new ReplacebelTextByBookmark(RenterNameBookmark, Renter.Name);
            yield return new ReplacebelTextByBookmark(RenterRepresentedBookmark, Renter.Represented);
            yield return new ReplacebelTextByBookmark(RenterAgreementBookmark, Renter.Agreement);


            string floor = UtilityServiceOffices[0].Floor.ToString();

            yield return new ReplacebelTextByBookmark(OfficeFloorBookmark, UtilityServiceOffices[0].Floor);

            string numbers = UtilityServiceOffices.First().Number.ToString();

            for (int i = 1; i < UtilityServiceOffices.Length; i++)
            {
                numbers += $", {UtilityServiceOffices[i].Number}";
            }

            yield return new ReplacebelTextByBookmark(OfficeNumberBookmark, numbers);

            yield return new ReplacebelTextByBookmark(BuildingAddressBookmark, LeaseContract.Building.Address);

            var totalSquare = decimal.Zero;
            foreach(var office in UtilityServiceOffices)
            {
                totalSquare += office.Square;
            }

            yield return new ReplacebelTextByBookmark(OfficeSquareBookmark, totalSquare);

            yield return new ReplacebelTextByBookmark(UtilityServiceContractTotalPriceBookmark, RusNumber.BuildString(UtilityServiceTotalPrice));

            var managerCompanyRequisites = new RequisitesBuilder(ManagerCompany);
            var renterRequisites = new RequisitesBuilder(Renter);

            yield return new ReplacebelTextByBookmark(ManagerCompanyRequisitesBookmark, managerCompanyRequisites.HeaderOfRequisites);
            yield return new ReplacebelTextByBookmark(ManagerCompanyRequisitesEndBookmark, managerCompanyRequisites.EndOfRequisites);

            yield return new ReplacebelTextByBookmark(RenterRequisitesBookmark, renterRequisites.HeaderOfRequisites);
            yield return new ReplacebelTextByBookmark(RenterRequisitesEndBookmark, renterRequisites.EndOfRequisites);
        }

        public override string ToString() => $"{Number} от {BeginDate.ToFormatDate()}";
    }
}