/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:NovogradArend.Desktop"
                           x:Key="Locator" />
  </Application.Resources>

  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using CommonServiceLocator;
using FluentValidation;
using GalaSoft.MvvmLight.Ioc;
using NovogradArend.Desktop.Services;
using NovogradArend.Desktop.ViewModels.Dialogs;
using NovogradArend.Desktop.ViewModels.Dialogs.Validators;

namespace NovogradArend.Desktop.ViewModels
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            //if (ViewModelBase.IsInDesignModeStatic)
            //{
            //    // Create design time view services and models
            //    SimpleIoc.Default.Register<IDataService, DesignDataService>();
            //}
            //else
            //{
            //    // Create run time view services and models
            //    SimpleIoc.Default.Register<IDataService, DataService>();
            //}

            //SimpleIoc.Default.Register(() =>
            //{
            //    var optionsBuilder = new DbContextOptionsBuilder<SQLiteDatabaseContext>();
            //    optionsBuilder.UseSqlite(@"Data Source=..\..\novograd_arend2.db");
            //    optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            //    var a = new SQLiteDatabaseContext(optionsBuilder.Options);

            //    return new ObjectsRepository() { _context = a };
            //});

            #region Services

            SimpleIoc.Default.Register<IObjectsService, ObjectsService>();

            #endregion Services

            #region ViewModels

            SimpleIoc.Default.Register<MainViewModel>();
            SimpleIoc.Default.Register<LeaseContractViewModel>();
            SimpleIoc.Default.Register<EntitiesViewModel>();
            SimpleIoc.Default.Register<ChooseDocumentForCreationViewModel>();
            SimpleIoc.Default.Register<AdditionalContractsViewModel>();

            #endregion ViewModels

            #region Validators

            SimpleIoc.Default
                    .Register<AbstractValidator<AddEditEntityDialogViewModel>, AddEditEntityDialogViewModelValidator>();

            SimpleIoc.Default
                    .Register<AbstractValidator<AddEditLeaseContractDialogViewModel>, AddEditLeaseContractDialogViewModelValidator>();

            SimpleIoc.Default
                    .Register<AbstractValidator<AddEditAct1ContractDialogViewModel>, AddEditAct1ContractDialogViewModelValidator>();

            SimpleIoc.Default
                    .Register<AbstractValidator<AddEditAct2ContractDialogViewModel>, AddEditAct2ContractDialogViewModelValidator>();

            SimpleIoc.Default
                    .Register<AbstractValidator<AddEditUtilityServiceContractDialogViewModel>, AddEditUtilityServiceContractDialogViewModelValidator>();

            SimpleIoc.Default
                .Register<AbstractValidator<AddEditClaimContractDialogViewModel>, AddEditClaimContractDialogViewModelValidator>();

            SimpleIoc.Default
                .Register<AbstractValidator<AddEditChangeArendPriceDialogViewModel>, AddEditChangeArendPriceDialogViewModelValidator>();

            SimpleIoc.Default
                .Register<AbstractValidator<AddEditTerminationDialogViewModel>, AddEditTerminationDialogViewModelValidator>();

            SimpleIoc.Default
                .Register<AbstractValidator<AddEditCancelContractDialogViewModel>, AddEditCancelContractDialogViewModelValidator>();

            #endregion Validators
        }

        public AdditionalContractsViewModel AdditionalContractsViewModel
            => ServiceLocator.Current.GetInstance<AdditionalContractsViewModel>();

        public ChooseDocumentForCreationViewModel ChooseDocumentForCreationViewModel
            => ServiceLocator.Current.GetInstance<ChooseDocumentForCreationViewModel>();

        public EntitiesViewModel EntitiesViewModel
            => ServiceLocator.Current.GetInstance<EntitiesViewModel>();

        public LeaseContractViewModel LeaseContractViewModel
            => ServiceLocator.Current.GetInstance<LeaseContractViewModel>();

        public MainViewModel MainViewModel
                                            => ServiceLocator.Current.GetInstance<MainViewModel>();

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}