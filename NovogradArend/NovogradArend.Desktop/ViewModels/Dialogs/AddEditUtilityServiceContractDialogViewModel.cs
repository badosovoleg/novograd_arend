﻿using FluentValidation;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using NovogradArend.Desktop.Contracts;
using NovogradArend.Model;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace NovogradArend.Desktop.ViewModels.Dialogs
{
    public sealed class AddEditUtilityServiceContractDialogViewModel : BaseDialogViewModel<AddEditUtilityServiceContractDialogViewModel>
    {
        public ObservableCollection<LeaseСontractDocument> LeaseContracts { get; set; }

        private LeaseСontractDocument _selectedLeaseContract;

        public ICommand PriceTextBoxUpCommand => new RelayCommand<string>((s) => UpdatePriceHandler(s));

     

        public LeaseСontractDocument SelectedLeaseContract
        {
            get => _selectedLeaseContract;
            set
            {
                Set(ref _selectedLeaseContract, value);
                SelectedLeaseContractChanged();
            }
        }

        private ObservableCollection<Office> _offices;
        public ObservableCollection<Office> Offices { get => _offices; set => Set(ref _offices, value); }

        private Office _selectedOffice;
        public Office SelectedOffice { get => _selectedOffice; set => Set(ref _selectedOffice, value); }

        private Office _selectedOfficeInTable;
        public Office SelectedOfficeInTable { get => _selectedOfficeInTable; set => Set(ref _selectedOfficeInTable, value); }

        private ObservableCollection<Office> _selectedOffices;

        public ObservableCollection<Office> SelectedOffices
        {
            get => _selectedOffices;
            set
            {
                Set(ref _selectedOffices, value);
                SelectedOfficesChanged();
            }
        }

        public ICommand AddOfficeCommand => new RelayCommand(() => AddOffice());

        public ICommand RemoveOfficeCommand => new RelayCommand(() => RemoveOffice());

        private string _number;
        public string Number { get => _number; set => Set(ref _number, value); }

        private DateTime _beginDate = DateTime.Today;
        public DateTime BeginDate { get => _beginDate; set => Set(ref _beginDate, value); }

        private string _managerCompany;
        public string ManagerCompany { get => _managerCompany; set => Set(ref _managerCompany, value); }

        private string _renterName;
        public string RenterName { get => _renterName; set => Set(ref _renterName, value); }

        private string _officesInfo;
        public string OfficesInfo { get => _officesInfo; set => Set(ref _officesInfo, value); }

        public decimal _priceForOneMeter;

        public decimal PriceForOneMeter
        {
            get => _priceForOneMeter;
            set
            {
                _priceForOneMeter = value;
                RaisePropertyChanged(() => PriceForOneMeter);
            }
        }

        public decimal _totalPrice;

        public decimal TotalPrice
        {
            get => _totalPrice;
            set
            {
                _totalPrice = value;
                RaisePropertyChanged(() => TotalPrice);
            }
        }

        public Func<int, string> GetCompanyName { get; set; }

        public ICommand LeaseContractsComboBoxKeyUpCommand => new RelayCommand<string>((s) => LeaseContractsComboBoxKeyUp(s));

        private decimal _square;

        public decimal Square
        {
            get => _square;
            set
            {
                _square = value;
                RaisePropertyChanged(() => Square);
            }
        }


        public ICommand SquareTextBoxKeyUpCommand => new RelayCommand<string>((s) => SquareTextBoxKeyUp(s));


        private bool _leaseContractsComboBoxDropDownOpen;
        public bool LeaseContractsComboBoxDropDownOpen { get => _leaseContractsComboBoxDropDownOpen; set => Set(ref _leaseContractsComboBoxDropDownOpen, value); }

        public AddEditUtilityServiceContractDialogViewModel() : base(SimpleIoc.Default.GetInstance<AbstractValidator<AddEditUtilityServiceContractDialogViewModel>>())
        {
        }

        private void RemoveOffice()
        {
            if (SelectedOfficeInTable == null)
            {
                MessageBox.Show("Выберите офис который хотите удалить!");
                return;
            }

            SelectedOffices.Remove(SelectedOfficeInTable);
            SelectedOfficesChanged();
        }

        private void AddOffice()
        {
            if (SelectedOffices == null)
                SelectedOffices = new ObservableCollection<Office>();

            if (SelectedOffices.Contains(SelectedOffice))
            {
                MessageBox.Show("Нельзя добавить одинковые офисы!");
                return;
            }

            SelectedOffices.Add(SelectedOffice);
            SelectedOfficesChanged();
        }

        private void SelectedLeaseContractChanged()
        {
            if (SelectedLeaseContract != null)
            {
                Offices = new ObservableCollection<Office>(SelectedLeaseContract.Offices);
                SelectedOffices = new ObservableCollection<Office>(SelectedLeaseContract.Offices);
            }
        }

        private void SelectedOfficesChanged()
        {
            PriceForOneMeter = Square = TotalPrice = decimal.Zero;
            Number = string.Empty;
            RenterName = null;
            ManagerCompany = null;

            if (SelectedLeaseContract != null)
            {
                ManagerCompany = GetCompanyName(SelectedLeaseContract.Building.ManagerCompanyId);

                RenterName = SelectedLeaseContract.Renter.ShortName;

                if (PriceForOneMeter == decimal.Zero)
                    PriceForOneMeter = SelectedOffices.First().ServicePrice;

                string numbers = string.Empty;

                foreach (var office in SelectedOffices)
                {
                    Square += office.Square;
                    numbers += $"{office.Number}, ";
                }

                Number = $"КО-{SelectedLeaseContract.Building.Code}/{SelectedOffices.First().Floor}/{SelectedOffices.First().Number}";

                TotalPrice = SelectedOffices.Select(o => PriceForOneMeter * o.Square).Sum();

                numbers = numbers.TrimEnd(',', ' ');

                OfficesInfo = SelectedLeaseContract.Building.Name + ", Офисы: " + numbers;
            }
        }

        private void LeaseContractsComboBoxKeyUp(string text)
        {
            if (text.Length > 0)
            {
                CollectionView itemsViewOriginal = (CollectionView)CollectionViewSource.GetDefaultView(LeaseContracts);

                itemsViewOriginal.Filter = s =>
                {
                    return ((LeaseСontractDocument)s).Number.ToLower().IndexOf(text, StringComparison.CurrentCultureIgnoreCase) >= 0;
                };

                itemsViewOriginal.Refresh();

                LeaseContractsComboBoxDropDownOpen = true;
            }
            else
            {
                CollectionView itemsViewOriginal = (CollectionView)CollectionViewSource.GetDefaultView(LeaseContracts);

                itemsViewOriginal.Filter = s => true;

                itemsViewOriginal.Refresh();

                LeaseContractsComboBoxDropDownOpen = true;
            }
        }

        private void SquareTextBoxKeyUp(string text)
        {
            var t = text.Replace('.', ',');

            if (t.Length > 0 && t.Last() != ',')
            {
                if (decimal.TryParse(t, out decimal newSquare))
                {
                    Square = newSquare;
                    TotalPrice = PriceForOneMeter * Square;
                }
            }
        }

        private void UpdatePriceHandler(string text)
        {
            var t = text.Replace('.', ',');

            if (t.Length > 0 && t.Last() != ',')
            {
                if (decimal.TryParse(t, out decimal newPrice))
                {
                    PriceForOneMeter = newPrice;
                    TotalPrice = PriceForOneMeter * Square;
                }
            }
        }
    }
}