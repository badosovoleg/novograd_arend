﻿using FluentValidation;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using NovogradArend.Desktop.Contracts;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace NovogradArend.Desktop.ViewModels.Dialogs
{
    public sealed class AddEditChangeArendPriceDialogViewModel : BaseDialogViewModel<AddEditChangeArendPriceDialogViewModel>
    {
        private string _number;
        public string Number { get => _number; set => Set(ref _number, value); }

        private DateTime _beginDate = DateTime.Today;
        public DateTime BeginDate { get => _beginDate; set => Set(ref _beginDate, value); }

        private ObservableCollection<LeaseСontractDocument> _leaseContracts;
        public ObservableCollection<LeaseСontractDocument> LeaseContracts { get => _leaseContracts; set => Set(ref _leaseContracts, value); }

        private LeaseСontractDocument _selectedLeaseContract;
        public LeaseСontractDocument SelectedLeaseContract
        {
            get => _selectedLeaseContract;
            set
            {
                Set(ref _selectedLeaseContract, value);
                SelectedLeaseContractChanged();
            }
        }

        public string _landlordName;
        public string LandlordName { get => _landlordName; set => Set(ref _landlordName, value); }

        public string _renterName;
        public string RenterName { get => _renterName; set => Set(ref _renterName, value); }

        public string _officesInfo;
        public string OfficesInfo { get => _officesInfo; set => Set(ref _officesInfo, value); }

        private decimal _newOfficePrice;
        public decimal NewOfficePrice { get => _newOfficePrice; set => Set(ref _newOfficePrice, value); }

        private decimal _newTotalOfficePrice;
        public decimal NewTotalOfficePrice { get => _newTotalOfficePrice; set => Set(ref _newTotalOfficePrice, value); }

        public ICommand LeaseContractsComboBoxKeyUpCommand => new RelayCommand<string>((s) => LeaseContractsComboBoxKeyUp(s));

        private bool _leaseContractsComboBoxDropDownOpen;
        public bool LeaseContractsComboBoxDropDownOpen { get => _leaseContractsComboBoxDropDownOpen; set => Set(ref _leaseContractsComboBoxDropDownOpen, value); }

        public AddEditChangeArendPriceDialogViewModel()
          : base(SimpleIoc.Default.GetInstance<AbstractValidator<AddEditChangeArendPriceDialogViewModel>>())
        {
        }

        public ICommand NewOfficePriceChangedCommand => new RelayCommand<string>((s) => NewOfficePriceChanged(s));

        private void SelectedLeaseContractChanged()
        {
            if (SelectedLeaseContract != null)
            {
                LandlordName = SelectedLeaseContract.Landlord.ShortName;
                RenterName = SelectedLeaseContract.Renter.ShortName;

                string numbers = SelectedLeaseContract.Offices.First().Number.ToString();

                for (int i = 1; i < SelectedLeaseContract.Offices.Length; i++)
                {
                    numbers += $", {SelectedLeaseContract.Offices[i].Number}";
                }

                OfficesInfo = SelectedLeaseContract.Building.Name + ", Офисы: " + numbers;

                Number = $"ИАП-{SelectedLeaseContract.Number}";

                NewTotalOfficePrice = SelectedLeaseContract.TotalPrice;

                NewOfficePrice = SelectedLeaseContract.PriceForOneSquareMeter;
            }
        }

        private void NewOfficePriceChanged(string text)
        {
            if (decimal.TryParse(text, out decimal price))
            {
                //NewOfficePrice = price;
                NewTotalOfficePrice = SelectedLeaseContract.TotalSquare * price;
            }
        }

        private void LeaseContractsComboBoxKeyUp(string text)
        {
            if (text.Length > 0)
            {
                CollectionView itemsViewOriginal = (CollectionView)CollectionViewSource.GetDefaultView(LeaseContracts);

                itemsViewOriginal.Filter = s =>
                {
                    return ((LeaseСontractDocument)s).Number.ToLower().IndexOf(text, StringComparison.CurrentCultureIgnoreCase) >= 0;
                };

                itemsViewOriginal.Refresh();

                LeaseContractsComboBoxDropDownOpen = true;
            }
        }
    }
} 
