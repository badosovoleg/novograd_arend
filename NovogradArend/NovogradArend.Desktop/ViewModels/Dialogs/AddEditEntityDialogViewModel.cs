﻿using FluentValidation;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using NovogradArend.Desktop.Contracts.Components;
using System;
using System.Windows.Input;

namespace NovogradArend.Desktop.ViewModels.Dialogs
{
    public sealed class AddEditEntityDialogViewModel : BaseDialogViewModel<AddEditEntityDialogViewModel>
    {
        #region Fields

        private string _name;
        private string _represented;
        private string _agreement;
        private string _shortName;
        private string _inn;
        private string _ogrn;
        private string _address;
        private string _rs;
        private string _bik;
        private string _email;
        private string _phone;
        private string _baseOn;
        private string _initials;
        private string _requisites;

        private RequisitesBuilder _requisitesBuilder;

        #endregion Fields

        #region Members

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                RaisePropertyChanged(() => Name);
                UpdateRequisitesHandler();
            }
        }

        public string Represented
        {
            get => _represented;
            set
            {
                _represented = value;
                RaisePropertyChanged(() => Represented);
                UpdateRequisitesHandler();
            }
        }

        public string Agreement
        {
            get => _agreement;
            set
            {
                _agreement = value;
                RaisePropertyChanged(() => Agreement);
                UpdateRequisitesHandler();
            }
        }

        public string ShortName
        {
            get => _shortName;
            set
            {
                _shortName = value;
                RaisePropertyChanged(() => ShortName);
                UpdateRequisitesHandler();
            }
        }

        public string INN
        {
            get => _inn;
            set
            {
                _inn = value;
                RaisePropertyChanged(() => INN);
                UpdateRequisitesHandler();
            }
        }

        public string OGRN
        {
            get => _ogrn;
            set
            {
                _ogrn = value;
                RaisePropertyChanged(() => OGRN);
                UpdateRequisitesHandler();
            }
        }

        public string Address
        {
            get => _address;
            set
            {
                _address = value;
                RaisePropertyChanged(() => Address);
                UpdateRequisitesHandler();
            }
        }

        public string RS
        {
            get => _rs;
            set
            {
                _rs = value;
                RaisePropertyChanged(() => RS);
                UpdateRequisitesHandler();
            }
        }

        public string BIK
        {
            get => _bik;
            set
            {
                _bik = value;
                RaisePropertyChanged(() => BIK);
                UpdateRequisitesHandler();
            }
        }

        public string Email
        {
            get => _email;
            set
            {
                _email = value;
                RaisePropertyChanged(() => Email);
                UpdateRequisitesHandler();
            }
        }

        public string Phone
        {
            get => _phone;
            set
            {
                _phone = value;
                RaisePropertyChanged(() => Phone);
                UpdateRequisitesHandler();
            }
        }

        public string BaseOn
        {
            get => _baseOn;
            set
            {
                _baseOn = value;
                RaisePropertyChanged(() => BaseOn);
                UpdateRequisitesHandler();
            }
        }

        public string Initials
        {
            get => _initials;
            set
            {
                _initials = value;
                RaisePropertyChanged(() => Initials);
                UpdateRequisitesHandler();
            }
        }

        public string Requisites { get => _requisites; set => Set(ref _requisites, value); }

        public string ShortRequisites => _requisitesBuilder.GetShortRequisites();

        #endregion Members

        #region Commands

        public ICommand UpdateRequisites => new RelayCommand(() => UpdateRequisitesHandler());

        public ICommand NameTextBoxKeyUpCommand => new RelayCommand<string>((s) => Name = s);

        public ICommand RepresentedTextBoxKeyUpCommand => new RelayCommand<string>((s) => Represented = s);

        public ICommand AgreementTextBoxKeyUpCommand => new RelayCommand<string>((s) => Agreement = s);

        public ICommand ShortNameTextBoxKeyUpCommand => new RelayCommand<string>((s) => ShortName = s);

        public ICommand INNTextBoxKeyUpCommand => new RelayCommand<string>((s) => INN = s);

        public ICommand OGRNTextBoxKeyUpCommand => new RelayCommand<string>((s) => OGRN = s);

        public ICommand AddressTextBoxKeyUpCommand => new RelayCommand<string>((s) => Address = s);

        public ICommand RSTextBoxKeyUpCommand => new RelayCommand<string>((s) => RS = s);

        public ICommand BIKTextBoxKeyUpCommand => new RelayCommand<string>((s) => BIK = s);

        public ICommand EmailTextBoxKeyUpCommand => new RelayCommand<string>((s) => Email = s);

        public ICommand PhoneTextBoxKeyUpCommand => new RelayCommand<string>((s) => Phone = s);

        public ICommand BaseOnTextBoxKeyUpCommand => new RelayCommand<string>((s) => BaseOn = s);

        public ICommand InitialsTextBoxKeyUpCommand => new RelayCommand<string>((s) => Initials = s);

        #endregion Commands

        public AddEditEntityDialogViewModel()
            : base(SimpleIoc.Default.GetInstance<AbstractValidator<AddEditEntityDialogViewModel>>())
        {
            UpdateRequisitesHandler();
        }

        private void UpdateRequisitesHandler()
            => Requisites = "Результирующие реквизиты:" + Environment.NewLine + (_requisitesBuilder = new RequisitesBuilder(this)).GetFullRequisites();
    }
}