﻿using FluentValidation;

namespace NovogradArend.Desktop.ViewModels.Dialogs.Validators
{
    public class AddEditEntityDialogViewModelValidator : AbstractValidator<AddEditEntityDialogViewModel>
    {
        public AddEditEntityDialogViewModelValidator()
        {
            RuleFor(x => x.Name)
        .NotEmpty()
        .NotEmptyMessageForField("Название организации или юр. лица");

            RuleFor(x => x.Represented)
.NotEmpty()
.NotEmptyMessageForField("В лице");

            RuleFor(x => x.Agreement)
.NotEmpty()
.NotEmptyMessageForField("На основании");

            RuleFor(x => x.ShortName)
.NotEmpty()
.NotEmptyMessageForField("Название организации (короткое)");

//            RuleFor(x => x.BaseOn)
//.NotEmpty()
//.NotEmptyMessageForField("На основании (для реквизитов)");

            RuleFor(x => x.Initials)
.NotEmpty()
.NotEmptyMessageForField("Инициалы");
        }
    }
}
