﻿using FluentValidation;

namespace NovogradArend.Desktop.ViewModels.Dialogs.Validators
{
    public static class RuleBuilderExtensions
    {
        public static IRuleBuilderOptions<T, PropType> NotEmptyMessageForField<T, PropType>(this IRuleBuilderOptions<T, PropType> builder, string fieldName)
            => builder.WithMessage($"Поле \"{fieldName}\" не может быть пустым!");

        public static IRuleBuilderOptions<T, PropType> NotEmptyMessageForComboBox<T, PropType>(this IRuleBuilderOptions<T, PropType> builder, string fieldName)
            => builder.WithMessage($"Поле \"{fieldName}\" должно быть выбрано из выпадающего списка!");

        public static IRuleBuilderOptions<T, PropType> MaxLengthMessageForField<T, PropType>(this IRuleBuilderOptions<T, PropType> builder, string fieldName, int maxLength)
            => builder.WithMessage($"Длина поля \"{fieldName}\" не может превышать {maxLength} символов!");
    }
}
