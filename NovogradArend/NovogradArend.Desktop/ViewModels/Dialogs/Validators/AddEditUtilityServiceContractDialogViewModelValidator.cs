﻿using FluentValidation;

namespace NovogradArend.Desktop.ViewModels.Dialogs.Validators
{
    public class AddEditUtilityServiceContractDialogViewModelValidator : AbstractValidator<AddEditUtilityServiceContractDialogViewModel>
    {
        public AddEditUtilityServiceContractDialogViewModelValidator()
        {
            RuleFor(x => x.SelectedLeaseContract)
              .NotEmpty()
              .NotEmptyMessageForComboBox("Номер договора аренды");

            RuleFor(x => x.SelectedOffices)
                .NotEmpty()
                 .NotEmptyMessageForField("Выбранные офисы");

            RuleFor(x => x.Number)
             .NotEmpty()
             .NotEmptyMessageForField("Номер");

            RuleFor(x => x.BeginDate)
               .NotEmpty()
               .NotEmptyMessageForField("Дата начала");

            RuleFor(x => x.PriceForOneMeter)
             .NotEmpty()
             .NotEmptyMessageForField("Цена обслуживания за кв. м.");
        }
    }
}
