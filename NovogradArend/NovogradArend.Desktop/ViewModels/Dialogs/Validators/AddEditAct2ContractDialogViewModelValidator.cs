﻿using FluentValidation;

namespace NovogradArend.Desktop.ViewModels.Dialogs.Validators
{
    public class AddEditAct2ContractDialogViewModelValidator : AbstractValidator<AddEditAct2ContractDialogViewModel>
    {
        public AddEditAct2ContractDialogViewModelValidator()
        {
            RuleFor(x => x.BeginDate)
              .NotEmpty()
              .NotEmptyMessageForField("Дата начала");

            RuleFor(x => x.ContractNumber)
               .NotEmpty()
               .NotEmptyMessageForField("Номер договора");

            RuleFor(x => x.SelectedLeaseContract)
               .NotEmpty()
               .NotEmptyMessageForComboBox("Номер договора аренды");
        }
    }
}
