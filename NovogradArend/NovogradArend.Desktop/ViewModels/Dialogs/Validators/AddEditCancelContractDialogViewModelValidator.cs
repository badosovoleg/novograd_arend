﻿using FluentValidation;

namespace NovogradArend.Desktop.ViewModels.Dialogs.Validators
{
    public class AddEditCancelContractDialogViewModelValidator : AbstractValidator<AddEditCancelContractDialogViewModel>
    {
        public AddEditCancelContractDialogViewModelValidator()
        {
            RuleFor(x => x.BeginDate)
              .NotEmpty()
              .NotEmptyMessageForField("Дата начала");

            RuleFor(x => x.MailDate)
             .NotEmpty()
             .NotEmptyMessageForField("Дата входящего письма");

            RuleFor(x => x.MailNumber)
               .NotEmpty()
               .NotEmptyMessageForField("Номер входящего письма");

            RuleFor(x => x.SelectedLeaseContract)
               .NotEmpty()
               .NotEmptyMessageForComboBox("Номер договора аренды");
        }
    }
}