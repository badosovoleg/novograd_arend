﻿using FluentValidation;

namespace NovogradArend.Desktop.ViewModels.Dialogs.Validators
{
    public class AddEditLeaseContractDialogViewModelValidator : AbstractValidator<AddEditLeaseContractDialogViewModel>
    {
        public AddEditLeaseContractDialogViewModelValidator()
        {
            RuleFor(x => x.SelectedOffices)
             .NotEmpty()
              .NotEmptyMessageForField("Выбранные офисы");


            RuleFor(x => x.Price)
             .NotEmpty()
              .NotEmptyMessageForField("Цена за кв. м.");

            RuleFor(x => x.SelectedRenter)
             .NotEmpty()
             .NotEmptyMessageForComboBox("Арендатор");

            RuleFor(x => x.SelectedCountLandlordDay)
             .NotEmpty()
             .NotEmptyMessageForComboBox("По инициативе аредодателя");


            RuleFor(x => x.SelectedCountRenterDay)
            .NotEmpty()
            .NotEmptyMessageForComboBox("По инициативе арендатора");

            RuleFor(x => x.BeginDate)
            .NotEmpty()
            .NotEmptyMessageForField("Дата начала");

            RuleFor(x => x.ForPlacement)
            .NotEmpty()
            .NotEmptyMessageForField("Для размещения");


            RuleFor(x => x.GaranteeContractNumber)
            .NotEmpty()
            .When(x => x.IsGarantee)
            .NotEmptyMessageForField("Номер договора для гарантийного письма");
        }
    }
}
