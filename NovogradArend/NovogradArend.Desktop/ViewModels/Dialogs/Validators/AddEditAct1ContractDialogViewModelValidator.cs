﻿using FluentValidation;

namespace NovogradArend.Desktop.ViewModels.Dialogs.Validators
{
    public class AddEditAct1ContractDialogViewModelValidator : AbstractValidator<AddEditAct1ContractDialogViewModel>
    {
        public AddEditAct1ContractDialogViewModelValidator()
        {
            RuleFor(x => x.BeginDate)
                .NotEmpty()
                .NotEmptyMessageForField("Дата начала");

            RuleFor(x => x.ContractNumber)
               .NotEmpty()
               .NotEmptyMessageForField("Номер договора");

            RuleFor(x => x.SelectedLeaseContract)
               .NotEmpty()
               .NotEmptyMessageForComboBox("Номер договора аренды");
        }
    }
}