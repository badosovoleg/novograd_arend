﻿using FluentValidation;

namespace NovogradArend.Desktop.ViewModels.Dialogs.Validators
{
    public class AddEditTerminationDialogViewModelValidator : AbstractValidator<AddEditTerminationDialogViewModel>
    {
        public AddEditTerminationDialogViewModelValidator()
        {
            RuleFor(x => x.BeginDate)
               .NotEmpty()
               .NotEmptyMessageForField("Дата начала");

            RuleFor(x => x.SelectedLeaseContract)
               .NotEmpty()
               .NotEmptyMessageForComboBox("Номер договора аренды");
        }
    }
}
