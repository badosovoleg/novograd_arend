﻿using FluentValidation;

namespace NovogradArend.Desktop.ViewModels.Dialogs.Validators
{
    public class AddEditChangeArendPriceDialogViewModelValidator : AbstractValidator<AddEditChangeArendPriceDialogViewModel>
    {
        public AddEditChangeArendPriceDialogViewModelValidator()
        {
            RuleFor(x => x.BeginDate)
           .NotEmpty()
           .NotEmptyMessageForField("Дата начала");

            RuleFor(x => x.Number)
             .NotEmpty()
             .NotEmptyMessageForField("Номер договора");

            RuleFor(x => x.NewOfficePrice)
               .NotEmpty()
               .NotEmptyMessageForField("Новая цена за кв. м.");

            RuleFor(x => x.NewTotalOfficePrice)
             .NotEmpty()
             .NotEmptyMessageForField("Общая новая цена");

            RuleFor(x => x.SelectedLeaseContract)
               .NotEmpty()
               .NotEmptyMessageForComboBox("Номер договора аренды");
        }
    }
}
