﻿using FluentValidation;

namespace NovogradArend.Desktop.ViewModels.Dialogs.Validators
{
    public class AddEditClaimContractDialogViewModelValidator : AbstractValidator<AddEditClaimContractDialogViewModel>
    {
        public AddEditClaimContractDialogViewModelValidator()
        {
            RuleFor(x => x.BeginDate)
         .NotEmpty()
         .NotEmptyMessageForField("Дата начала");

            RuleFor(x => x.Number)
             .NotEmpty()
             .NotEmptyMessageForField("Номер договора");

            RuleFor(x => x.ClaimDate)
               .NotEmpty()
               .NotEmptyMessageForField("Дата до которой необходимо погасить задолжность");

            RuleFor(x => x.SelectedLeaseContract)
               .NotEmpty()
               .NotEmptyMessageForComboBox("Номер договора аренды");


            RuleFor(x => x.SelectedUtilityServiceContract)
             .NotEmpty()
             .NotEmptyMessageForComboBox("Номер договора обслуживания");
        }
    }
}
