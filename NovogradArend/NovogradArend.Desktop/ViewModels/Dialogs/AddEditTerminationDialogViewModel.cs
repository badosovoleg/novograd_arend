﻿using FluentValidation;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using NovogradArend.Desktop.Contracts;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Data;
using System.Windows.Input;

namespace NovogradArend.Desktop.ViewModels.Dialogs
{
    public sealed class AddEditTerminationDialogViewModel : BaseDialogViewModel<AddEditTerminationDialogViewModel>
    {
        private DateTime _beginDate = DateTime.Today;
        public DateTime BeginDate { get => _beginDate; set => Set(ref _beginDate, value); }

        private ObservableCollection<LeaseСontractDocument> _leaseContracts;
        public ObservableCollection<LeaseСontractDocument> LeaseContracts { get => _leaseContracts; set => Set(ref _leaseContracts, value); }

        private LeaseСontractDocument _selectedLeaseContract;

        public LeaseСontractDocument SelectedLeaseContract
        {
            get => _selectedLeaseContract;
            set
            {
                Set(ref _selectedLeaseContract, value);
                SelectedLeaseContractChanged();
            }
        }

        public string _landlordName;
        public string LandlordName { get => _landlordName; set => Set(ref _landlordName, value); }

        public string _renterName;
        public string RenterName { get => _renterName; set => Set(ref _renterName, value); }

        public string _officesInfo;
        public string OfficesInfo { get => _officesInfo; set => Set(ref _officesInfo, value); }

        public ICommand LeaseContractsComboBoxKeyUpCommand => new RelayCommand<string>((s) => LeaseContractsComboBoxKeyUp(s));

        private bool _leaseContractsComboBoxDropDownOpen;
        public bool LeaseContractsComboBoxDropDownOpen { get => _leaseContractsComboBoxDropDownOpen; set => Set(ref _leaseContractsComboBoxDropDownOpen, value); }

        public AddEditTerminationDialogViewModel()
          : base(SimpleIoc.Default.GetInstance<AbstractValidator<AddEditTerminationDialogViewModel>>())
        {
        }

        private void SelectedLeaseContractChanged()
        {
            if (SelectedLeaseContract != null)
            {
                LandlordName = SelectedLeaseContract.Landlord.ShortName;
                RenterName = SelectedLeaseContract.Renter.ShortName;

                string numbers = SelectedLeaseContract.Offices.First().Number.ToString();

                for (int i = 1; i < SelectedLeaseContract.Offices.Length; i++)
                {
                    numbers += $", {SelectedLeaseContract.Offices[i].Number}";
                }

                OfficesInfo = SelectedLeaseContract.Building.Name + ", Офисы: " + numbers;
            }
        }

        private void LeaseContractsComboBoxKeyUp(string text)
        {
            if (text.Length > 0)
            {
                CollectionView itemsViewOriginal = (CollectionView)CollectionViewSource.GetDefaultView(LeaseContracts);

                itemsViewOriginal.Filter = s =>
                {
                    return ((LeaseСontractDocument)s).Number.ToLower().IndexOf(text, StringComparison.CurrentCultureIgnoreCase) >= 0;
                };

                itemsViewOriginal.Refresh();

                LeaseContractsComboBoxDropDownOpen = true;
            }
            else
            {
                CollectionView itemsViewOriginal = (CollectionView)CollectionViewSource.GetDefaultView(LeaseContracts);

                itemsViewOriginal.Filter = s => true;

                itemsViewOriginal.Refresh();

                LeaseContractsComboBoxDropDownOpen = true;
            }
        }
    }
}