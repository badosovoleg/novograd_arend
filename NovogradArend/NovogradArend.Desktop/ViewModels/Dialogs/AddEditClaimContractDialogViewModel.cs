﻿using FluentValidation;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using NovogradArend.Desktop.Contracts;
using NovogradArend.Model;
using System;
using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Windows.Input;

namespace NovogradArend.Desktop.ViewModels.Dialogs
{
    public sealed class AddEditClaimContractDialogViewModel : BaseDialogViewModel<AddEditClaimContractDialogViewModel>
    {

        private string _number = "б/н";
        public string Number { get => _number; set => Set(ref _number, value); }

        private DateTime _beginDate = DateTime.Today;
        public DateTime BeginDate { get => _beginDate; set => Set(ref _beginDate, value); }

        private DateTime _claimDate = DateTime.Today;
        public DateTime ClaimDate { get => _claimDate; set => Set(ref _claimDate, value); }

        private ObservableCollection<LeaseСontractDocument> _leaseContracts;
        public ObservableCollection<LeaseСontractDocument> LeaseContracts { get => _leaseContracts; set => Set(ref _leaseContracts, value); }

        private LeaseСontractDocument _selectedLeaseContract;
        public LeaseСontractDocument SelectedLeaseContract
        {
            get => _selectedLeaseContract;
            set
            {
                Set(ref _selectedLeaseContract, value);
                SelectedLeaseContractChanged();
            }
        }

        private ObservableCollection<UtilityServiceContract> _utilityServiceContracts;
        public ObservableCollection<UtilityServiceContract> UtilityServiceContracts { get => _utilityServiceContracts; set => Set(ref _utilityServiceContracts, value); }

        private UtilityServiceContract _selectedUtilityServiceContract;
        public UtilityServiceContract SelectedUtilityServiceContract { get => _selectedUtilityServiceContract; set => Set(ref _selectedUtilityServiceContract, value); }

        private decimal _arendDebt;
        public decimal ArendDebt { get => _arendDebt; set => Set(ref _arendDebt, value); }

        private decimal _utilityServiceDebt;
        public decimal UtilityServiceDebt { get => _utilityServiceDebt; set => Set(ref _utilityServiceDebt, value); }

        private string _renterName;
        public string RenterName { get => _renterName; set => Set(ref _renterName, value);  }

        private string _officesInfo;
        public string OfficesInfo { get => _officesInfo; set => Set(ref _officesInfo, value); }

        public ICommand LeaseContractsComboBoxKeyUpCommand => new RelayCommand<string>((s) => LeaseContractsComboBoxKeyUp(s));

        private bool _leaseContractsComboBoxDropDownOpen;
        public bool LeaseContractsComboBoxDropDownOpen { get => _leaseContractsComboBoxDropDownOpen; set => Set(ref _leaseContractsComboBoxDropDownOpen, value); }

        public ICommand UtilityServiceContractsComboBoxKeyUpCommand => new RelayCommand<string>((s) => UtilityServiceContractsComboBoxKeyUp(s));
        
        private bool _utilityServiceContractsComboBoxDropDownOpen;
        public bool UtilityServiceContractsComboBoxDropDownOpen { get => _utilityServiceContractsComboBoxDropDownOpen; set => Set(ref _utilityServiceContractsComboBoxDropDownOpen, value); }

        public AddEditClaimContractDialogViewModel() : base(SimpleIoc.Default.GetInstance<AbstractValidator<AddEditClaimContractDialogViewModel>>())
        {
        }

        private void SelectedLeaseContractChanged()
        {
            if (SelectedLeaseContract != null)
            {
                RenterName = SelectedLeaseContract.Renter.ShortName;

                string numbers = string.Empty;

                foreach (var office in SelectedLeaseContract.Offices)
                {
                    numbers += $"{office.Number}, ";
                }

                numbers = numbers.TrimEnd(',', ' ');

                OfficesInfo = SelectedLeaseContract.Building.Name + ", Офисы: " + numbers;
            }
        }

        private void LeaseContractsComboBoxKeyUp(string text)
        {
            if (text.Length > 0)
            {
                CollectionView itemsViewOriginal = (CollectionView)CollectionViewSource.GetDefaultView(LeaseContracts);

                itemsViewOriginal.Filter = s =>
                {
                    return ((LeaseСontractDocument)s).Number.ToLower().IndexOf(text, StringComparison.CurrentCultureIgnoreCase) >= 0;
                };

                itemsViewOriginal.Refresh();

                LeaseContractsComboBoxDropDownOpen = true;
            }
            else
            {
                CollectionView itemsViewOriginal = (CollectionView)CollectionViewSource.GetDefaultView(LeaseContracts);

                itemsViewOriginal.Filter = s => true;

                itemsViewOriginal.Refresh();

                LeaseContractsComboBoxDropDownOpen = true;
            }
        }

        private void UtilityServiceContractsComboBoxKeyUp(string text)
        {
            if (text.Length > 0)
            {
                CollectionView itemsViewOriginal = (CollectionView)CollectionViewSource.GetDefaultView(UtilityServiceContracts);

                itemsViewOriginal.Filter = s =>
                {
                    return ((UtilityServiceContract)s).Number.ToLower().IndexOf(text, StringComparison.CurrentCultureIgnoreCase) >= 0;
                };

                itemsViewOriginal.Refresh();

                UtilityServiceContractsComboBoxDropDownOpen = true;
            }
        }
    }
}