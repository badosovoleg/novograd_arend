﻿using FluentValidation;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;
using NovogradArend.Model;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace NovogradArend.Desktop.ViewModels.Dialogs
{
    public sealed class AddEditLeaseContractDialogViewModel : BaseDialogViewModel<AddEditLeaseContractDialogViewModel>
    {
        #region Fields

        private ObservableCollection<Office> _offices;
        private Office _selectedOffice;
        private ObservableCollection<Building> _buildings;
        private Building _selectedBuildbing;
        private ObservableCollection<Office> _selectedOffices;
        private ObservableCollection<Entity> _renters;
        private Entity _selectedRenter;
        private Office _selectedOfficeInTable;
        private string _contractNumber;
        private DateTime _beginDate = DateTime.Now;
        private DateTime _endDate = DateTime.Now;
        private decimal _price;
        private decimal _square;
        private decimal _totalPrice;
        private Entity _landlord;
        private int _selectedCountLandlordDay;
        private int _selectedCountRenterDay;
        private DateTime _act1Date = DateTime.Now;
        private bool _isGarantee = false;
        private string _garanteeContractNumber;
        private string _forPlacement;
        private DateTime _garanteeBeginDate = DateTime.Now;
        private bool _rentersComboBoxDropDownOpen;

        #endregion Fields

        #region Members

        public ObservableCollection<Office> Offices { get => _offices; set => Set(ref _offices, value); }

        public Office SelectedOffice
        {
            get => _selectedOffice;
            set
            {
                _selectedOffice = value;
                RaisePropertyChanged(() => SelectedOffice);
            }
        }

        public ObservableCollection<Building> Buildings
        {
            get => _buildings;
            set
            {
                _buildings = value;
                RaisePropertyChanged(() => Buildings);
            }
        }

        public Building SelectedBuildbing
        {
            get => _selectedBuildbing;
            set
            {
                _selectedBuildbing = value;
                FilterOffices(value.Id);
                RaisePropertyChanged(() => SelectedBuildbing);
            }
        }

        public ObservableCollection<Office> SelectedOffices
        {
            get => _selectedOffices;
            set
            {
                _selectedOffices = value;
                RaisePropertyChanged(() => SelectedOffices);
            }
        }

        public ObservableCollection<Entity> Renters
        {
            get => _renters;
            set
            {
                _renters = value;
                RaisePropertyChanged(() => Renters);
            }
        }

        public Entity SelectedRenter
        {
            get => _selectedRenter;
            set
            {
                _selectedRenter = value;
                RaisePropertyChanged(() => SelectedRenter);
            }
        }

        public Office SelectedOfficeInTable { get => _selectedOfficeInTable; set => Set(ref _selectedOfficeInTable, value); }

        public string ContractNumber
        {
            get => _contractNumber;
            set
            {
                _contractNumber = value;
                RaisePropertyChanged(() => ContractNumber);
            }
        }

        public DateTime BeginDate
        {
            get => _beginDate;
            set
            {
                _beginDate = value;
                RaisePropertyChanged(() => BeginDate);
            }
        }

        public DateTime EndDate
        {
            get => _endDate;
            set
            {
                _endDate = value;
                RaisePropertyChanged(() => EndDate);
            }
        }

        public decimal Price
        {
            get => _price;
            set
            {
                _price = value;
                RaisePropertyChanged(() => Price);
            }
        }

        public decimal Square
        {
            get => _square;
            set
            {
                _square = value;
                RaisePropertyChanged(() => Square);
            }
        }

        public decimal TotalPrice
        {
            get => _totalPrice;
            set
            {
                _totalPrice = value;
                RaisePropertyChanged(() => TotalPrice);
            }
        }

        public Entity Landlord
        {
            get => _landlord;
            set
            {
                _landlord = value;
                RaisePropertyChanged(() => Landlord);
            }
        }

        public ObservableCollection<int> CountLandlordDays => new ObservableCollection<int>(new[] { 15, 30, 45, 60 });

        public int SelectedCountLandlordDay
        {
            get => _selectedCountLandlordDay;
            set
            {
                _selectedCountLandlordDay = value;
                RaisePropertyChanged(() => SelectedCountLandlordDay);
            }
        }

        public ObservableCollection<int> CountRenterDays => new ObservableCollection<int>(new[] { 15, 30, 45, 60 });

        public int SelectedCountRenterDay
        {
            get => _selectedCountRenterDay;
            set
            {
                _selectedCountRenterDay = value;
                RaisePropertyChanged(() => SelectedCountRenterDay);
            }
        }

        public DateTime Act1Date
        {
            get => _act1Date;
            set
            {
                _act1Date = value;
                RaisePropertyChanged(() => Act1Date);
            }
        }

        public bool IsGarantee
        {
            get => _isGarantee;
            set
            {
                _isGarantee = value;
                RaisePropertyChanged(() => IsGarantee);
            }
        }

        public string GaranteeContractNumber
        {
            get => _garanteeContractNumber;
            set
            {
                _garanteeContractNumber = value;
                RaisePropertyChanged(() => GaranteeContractNumber);
            }
        }

        public string ForPlacement
        {
            get => _forPlacement;
            set
            {
                _forPlacement = value;
                RaisePropertyChanged(() => ForPlacement);
            }
        }

        public DateTime GaranteeBeginDate
        {
            get => _garanteeBeginDate;
            set
            {
                _garanteeBeginDate = value;
                RaisePropertyChanged(() => GaranteeBeginDate);
            }
        }

        public bool RentersComboBoxDropDownOpen { get => _rentersComboBoxDropDownOpen; set => Set(ref _rentersComboBoxDropDownOpen, value); }

        public Func<int, Entity> GetLandlord;

        #endregion Members

        #region Commands

        public ICommand AddOfficeCommand => new RelayCommand(() => AddOffice());

        public ICommand RemoveOfficeCommand => new RelayCommand(() => RemoveOffice());

        public ICommand RentersComboBoxKeyUpCommand => new RelayCommand<string>((s) => RentersComboBoxKeyUp(s));

        public ICommand PriceTextBoxKeyUpCommand => new RelayCommand<string>((s) => PriceTextBoxKeyUp(s));

        public ICommand SquareTextBoxKeyUpCommand => new RelayCommand<string>((s) => SquareTextBoxKeyUp(s));

        #endregion Commands

        public AddEditLeaseContractDialogViewModel()
            : base(SimpleIoc.Default.GetInstance<AbstractValidator<AddEditLeaseContractDialogViewModel>>())
        {
        }

        #region Methods

        private void UpdateContractInfo()
        {
            Price = Square = TotalPrice = decimal.Zero;
            ContractNumber = string.Empty;
            Landlord = null;
            FilterOffices(SelectedBuildbing.Id);

            if (SelectedOffices.Count > 0)
            {
                if (Landlord == null) Landlord = GetLandlord(SelectedOffices.First().EntityId);

                foreach (var office in SelectedOffices)
                {
                    Square += office.Square;
                    Price += office.Price;
                }

                Price = Price / SelectedOffices.Count;

                ContractNumber = $"ДА-{SelectedBuildbing.Code}/{SelectedOffices.First().Floor}/{SelectedOffices.First().Number}";

                TotalPrice = Price * Square;

                FilterOfficesByFloor(SelectedOffices.First().Floor);
            }
        }

        private void FilterOffices(int id)
        {
            CollectionView itemsViewOriginal = (CollectionView)CollectionViewSource.GetDefaultView(Offices);
            itemsViewOriginal.Filter = s => ((Office)s).BuildingId == id;
            itemsViewOriginal.Refresh();
        }

        private void FilterOfficesByFloor(int floor)
        {
            CollectionView itemsViewOriginal = (CollectionView)CollectionViewSource.GetDefaultView(Offices);
            itemsViewOriginal.Filter = s => ((Office)s).Floor == floor;
            itemsViewOriginal.Refresh();
        }

        #endregion Methods

        #region Command handlers

        private void AddOffice()
        {
            if (SelectedOffice == null || SelectedBuildbing == null)
                return;

            if (SelectedOffices == null)
                SelectedOffices = new ObservableCollection<Office>();

            if (SelectedOffices.Any(o => o.BuildingId != SelectedOffice.BuildingId))
                MessageBox.Show("Офисы должны находится в одном здании!");
            else if (SelectedOffices.Any(o => o.Floor != SelectedOffice.Floor))
                MessageBox.Show("Офисы должны находится на одном этаже!");
            else if (SelectedOffices.Contains(SelectedOffice))
                MessageBox.Show("Вы уже добавили данный офис!");
            else
                SelectedOffices.Add(SelectedOffice);

            UpdateContractInfo();
        }

        private void RemoveOffice()
        {
            if (SelectedOfficeInTable == null)
                MessageBox.Show("Выберите офис который хотите удалить!");
            else
                SelectedOffices.Remove(SelectedOfficeInTable);

            UpdateContractInfo();
        }

        #endregion Command handlers

        #region Event handlers

        private void RentersComboBoxKeyUp(string text)
        {
            CollectionView itemsViewOriginal = (CollectionView)CollectionViewSource.GetDefaultView(Renters);

            if (text.Length > 0)
                itemsViewOriginal.Filter = s => ((Entity)s).Name.ToLower().IndexOf(text, StringComparison.CurrentCultureIgnoreCase) >= 0;
            else
                itemsViewOriginal.Filter = s => true;

            itemsViewOriginal.Refresh();
            RentersComboBoxDropDownOpen = true;
        }

        private void PriceTextBoxKeyUp(string text)
        {
            var t = text.Replace('.', ',');

            if (t.Length > 0 && t.Last() != ',')
            {
                if (decimal.TryParse(t, out decimal newPrice))
                {
                    Price = newPrice;
                    TotalPrice = Price * Square;
                }
            }
        }

        private void SquareTextBoxKeyUp(string text)
        {
            var t = text.Replace('.', ',');

            if (t.Length > 0 && t.Last() != ',')
            {
                if (decimal.TryParse(t, out decimal newSquare))
                {
                    Square = newSquare;
                    TotalPrice = Price * Square;
                }
            }
        }

        #endregion Event handlers
    }
}