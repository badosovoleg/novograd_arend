﻿using FluentValidation;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MvvmDialogs;
using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace NovogradArend.Desktop.ViewModels.Dialogs
{
    public abstract class BaseDialogViewModel<T> :
        ViewModelBase, IModalDialogViewModel, IDataErrorInfo
        where T : class, IModalDialogViewModel
    {
        public ICommand AcceptButtonCommand { get; }
        private readonly AbstractValidator<T> _validator;

        private bool _hasErrors;

        public bool HasErrors
        {
            get { return _hasErrors; }
            set
            {
                if (_hasErrors != value)
                {
                    Set(ref _hasErrors, value);
                    ((RelayCommand)AcceptButtonCommand).RaiseCanExecuteChanged();
                }
            }
        }

        private bool? dialogResult;

        public bool? DialogResult
        {
            get => dialogResult;
            private set => Set(nameof(DialogResult), ref dialogResult, value);
        }

        private DialogTitle _dialogTitle = new DialogTitle().Default;

        public DialogTitle DialogTitle
        {
            get => _dialogTitle;
            set
            {
                _dialogTitle = value;
                RaisePropertyChanged(() => DialogTitle);
            }
        }

        private ImageSource _dialogIcon;

        public ImageSource DialogIcon
        {
            get => _dialogIcon;
            set
            {
                _dialogIcon = value;
                RaisePropertyChanged(() => DialogIcon);
            }
        }

        public void SetIcon(DialogIcon dialogIcon)
        {
            DialogIcon = BitmapFrame.Create(new Uri(@"pack://application:,,,/" + Assembly.GetExecutingAssembly().GetName().Name + ";component/" + dialogIcon, UriKind.Absolute));
        }

        protected BaseDialogViewModel(AbstractValidator<T> validator)
        {
            _validator = validator;
            AcceptButtonCommand = new RelayCommand(AcceptButtonCommandHandler, () => !HasErrors);

            SetIcon(Dialogs.DialogIcon.Default);
        }

        private void AcceptButtonCommandHandler()
        {
            if (Error == string.Empty)
                DialogResult = true;
            else
                MessageBox.Show(Error, $"{Constants.Error}!");
        }

        public string Error
        {
            get
            {
                var results = _validator?.Validate(this as T);
                if (results == null || !results.Errors.Any()) return string.Empty;
                var errors = string.Join(Environment.NewLine, results.Errors.Select(x => x.ErrorMessage).ToArray());
                return errors;
            }
        }

        public string this[string columnName]
        {
            get
            {
                var firstOrDefault = _validator?.Validate(this as T).Errors.FirstOrDefault(lol => lol.PropertyName == columnName);
                if (firstOrDefault != null)
                {
                    HasErrors = true;
                    return _validator != null ? firstOrDefault.ErrorMessage : "";
                }
                HasErrors = false;
                return "";
            }
        }
    }
}