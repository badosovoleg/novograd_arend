﻿using MvvmDialogs.DialogTypeLocators;
using System;
using System.ComponentModel;

namespace NovogradArend.Desktop.ViewModels.Dialogs.Components
{
    public class CustomDialogTypeLocator : IDialogTypeLocator
    {
        public Type Locate(INotifyPropertyChanged viewModel)
        {
            Type viewModelType = viewModel.GetType();
            string viewModelTypeName = viewModelType.FullName;

            var dialogTypeName = viewModelTypeName.Replace(".ViewModels.", ".Views.").Replace("ViewModel", string.Empty);

            return viewModelType.Assembly.GetType(dialogTypeName);
        }
    }
}
