﻿using GalaSoft.MvvmLight;
using MvvmDialogs;
using NovogradArend.Desktop.Contracts;
using NovogradArend.Desktop.Contracts.Components;
using NovogradArend.Desktop.Services;
using NovogradArend.Desktop.ViewModels.Dialogs.Components;
using NovogradArend.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace NovogradArend.Desktop.ViewModels.Dialogs
{
    public class NovogradDialogService
    {
        private readonly IDialogService _dialogService;
        private readonly IObjectsService _service;
        private readonly ViewModelBase _parent;

        public NovogradDialogService(ViewModelBase parent, IObjectsService service)
        {
            _parent = parent;
            _service = service;
            _dialogService = new DialogService(null, new CustomDialogTypeLocator());
        }

        private void ShowDialog<T>(Action<T> prepareModel, Action<T> processingDialogResult) where T : IModalDialogViewModel, new()
        {
            var dialogModel = new T();

            bool? success = false;

            prepareModel(dialogModel);

            success = _dialogService.ShowDialog(_parent, dialogModel);
            if (success == true)
            {
                processingDialogResult(dialogModel);
            }
        }

        private List<Entity> GetRenters() => _service.GetEntitiesByCondition(e => e.EntityTypeId == 2).Result;

        private List<Entity> GetEntities() => _service.GetEntitiesAsync().Result;

        private Func<int, Entity> GetLandlord => (id) => GetEntities().Single(e => e.Id == id);

        #region LeaseСontractDocument

        public void OpenAddLeaseContractDialog(
            Action<AddEditLeaseContractDialogViewModel> customPrepareAction = null,
            Action<AddEditLeaseContractDialogViewModel> customProcessingDialogResultAction = null)
        {
            // заполняем модель данными
            if (customPrepareAction == null)
                customPrepareAction = viewModel =>
                {
                    viewModel.DialogTitle = new LeaseContractDialogTitle().Add;

                    viewModel.Renters = new ObservableCollection<Entity>(GetRenters());
                    viewModel.Buildings = new ObservableCollection<Building>(_service.GetBuildingsAsync().Result);
                    viewModel.Offices = new ObservableCollection<Office>(ContractsExtensions.GetFreeOfices(_service));

                    viewModel.GetLandlord = GetLandlord;

                    viewModel.SetIcon(DialogIcon.Add);
                };

            // применяем логику к результату диалога
            if (customProcessingDialogResultAction == null)
                customProcessingDialogResultAction = viewModel =>
                {
                    var leaseContract = new LeaseContract()
                    {
                        Number = viewModel.ContractNumber,
                        BeginDate = viewModel.BeginDate.ToShortDateString(),
                        EndDate = viewModel.EndDate.ToShortDateString(),
                        LandLordId = viewModel.Landlord.Id,
                        RenterId = viewModel.SelectedRenter.Id,
                        ForPlacement = viewModel.ForPlacement,
                        TotalSquare = viewModel.Square,
                        TotalPrice = viewModel.TotalPrice,
                        Price = viewModel.Price,
                        BuildingId = viewModel.SelectedBuildbing.Id,
                        Act1BeginDate = viewModel.Act1Date.ToShortDateString(),
                        GaranteeNumber = viewModel.GaranteeContractNumber,
                        GaranteeDate = viewModel.GaranteeBeginDate.ToShortDateString(),
                        CountLandLordDay = viewModel.SelectedCountLandlordDay,
                        CountRenterDay = viewModel.SelectedCountRenterDay,
                        IsActual = true
                    };

                    var result = _service.AddLeaseContractAsync(leaseContract).Result;

                    leaseContract.Id =
                        _service
                        .GetLeaseContractsAsync()
                        .Result
                        .Single(c => c.Number == viewModel.ContractNumber && c.IsActual && c.BeginDate == viewModel.BeginDate.ToShortDateString()).Id;

                    _service.UpdateLeaseContractOffices(leaseContract, viewModel.SelectedOffices.ToArray());
                };

            ShowDialog(customPrepareAction, customProcessingDialogResultAction);
        }

        public void OpenEditContractDialog(
            LeaseСontractDocument SelectedContract,
            Action<AddEditLeaseContractDialogViewModel> customPrepareAction = null,
            Action<AddEditLeaseContractDialogViewModel> customProcessingDialogResultAction = null)
        {
            // заполняем модель данными
            if (customPrepareAction == null)
                customPrepareAction = viewModel =>
                {
                    viewModel.DialogTitle = new LeaseContractDialogTitle().Edit;

                    viewModel.Renters = new ObservableCollection<Entity>(GetRenters());
                    viewModel.SelectedRenter = viewModel.Renters.Single(r => r.Id == SelectedContract.Renter.Id);

                    viewModel.Offices = new ObservableCollection<Office>(ContractsExtensions.GetFreeOfices(_service));

                    viewModel.Buildings = new ObservableCollection<Building>(_service.GetBuildingsAsync().Result);
                    viewModel.SelectedBuildbing = viewModel.Buildings.Single(b => b.Id == SelectedContract.Building.Id);

                    viewModel.SelectedOffices = new ObservableCollection<Office>(SelectedContract.Offices);

                    viewModel.Landlord = SelectedContract.Landlord;

                    var garantee = SelectedContract.Garantee;

                    if (garantee != null)
                    {
                        viewModel.IsGarantee = true;
                        viewModel.GaranteeBeginDate = garantee.Date;
                        viewModel.GaranteeContractNumber = garantee.Number;
                    }

                    viewModel.GetLandlord = GetLandlord;

                    viewModel.ForPlacement = SelectedContract.ForPlacement;
                    viewModel.EndDate = SelectedContract.EndDate;
                    viewModel.SelectedCountLandlordDay = SelectedContract.CountLandlordDay;
                    viewModel.SelectedCountRenterDay = SelectedContract.CountRenterDay;

                    viewModel.TotalPrice = SelectedContract.TotalPrice;
                    viewModel.Price = SelectedContract.PriceForOneSquareMeter;
                    viewModel.Square = SelectedContract.TotalSquare;

                    viewModel.Act1Date = SelectedContract.Act1ContractDocument.BeginDate;

                    viewModel.ContractNumber = SelectedContract.Number;
                    viewModel.BeginDate = SelectedContract.BeginDate;

                    viewModel.SetIcon(DialogIcon.Edit);
                };

            // применяем логику к результату диалога
            if (customProcessingDialogResultAction == null)
                customProcessingDialogResultAction = viewModel =>
                {
                    var leaseContract = new LeaseContract()
                    {
                        Id = SelectedContract.Id,
                        Number = viewModel.ContractNumber,
                        BeginDate = viewModel.BeginDate.ToShortDateString(),
                        EndDate = viewModel.EndDate.ToShortDateString(),
                        LandLordId = viewModel.Landlord.Id,
                        RenterId = viewModel.SelectedRenter.Id,
                        ForPlacement = viewModel.ForPlacement,
                        TotalSquare = viewModel.Square,
                        TotalPrice = viewModel.TotalPrice,
                        Price = viewModel.Price,
                        BuildingId = viewModel.SelectedBuildbing.Id,
                        Act1BeginDate = viewModel.Act1Date.ToShortDateString(),
                        GaranteeNumber = viewModel.GaranteeContractNumber,
                        GaranteeDate = viewModel.GaranteeBeginDate.ToShortDateString(),
                        CountLandLordDay = viewModel.SelectedCountLandlordDay,
                        CountRenterDay = viewModel.SelectedCountRenterDay,
                        IsActual = true
                    };

                    var result = _service.EditLeaseContractAsync(leaseContract).Result;

                    _service.UpdateLeaseContractOffices(leaseContract, viewModel.SelectedOffices.ToArray());
                };

            ShowDialog(customPrepareAction, customProcessingDialogResultAction);
        }

        #endregion LeaseСontractDocument

        #region Act1ContractDocument

        public void OpenAddAct1ContractDialog(
            LeaseСontractDocument contract = null,
            Action<AddEditAct1ContractDialogViewModel> customPrepareAction = null,
            Action<AddEditAct1ContractDialogViewModel> customProcessingDialogResultAction = null)
        {
            // заполняем модель данными
            if (customPrepareAction == null)
                customPrepareAction = viewModel =>
               {
                   viewModel.DialogTitle = new Act1ContractDialogTitle().Add;

                   viewModel.LeaseContracts = new ObservableCollection<LeaseСontractDocument>(_service.GetLeaseContractsAsync().Result.Select(_service.GetLeaseСontractDocument));

                   if (contract != null) viewModel.SelectedLeaseContract = viewModel.LeaseContracts.Single(c => c.Id == contract.Id);

                   viewModel.SetIcon(DialogIcon.Add);
               };

            // применяем логику к результату диалога
            if (customProcessingDialogResultAction == null)
                customProcessingDialogResultAction = viewModel =>
               {
                   var newContract = new Act1Contract()
                   {
                       Number = viewModel.ContractNumber,
                       LeaseContractId = viewModel.SelectedLeaseContract.Id,
                       BeginDate = viewModel.BeginDate.ToShortDateString()
                   };

                   var result = _service.AddAct1ContractAsync(newContract).Result;
               };

            ShowDialog(customPrepareAction, customProcessingDialogResultAction);
        }

        public void OpenEditContractDialog(
            Act1ContractDocument contractDocument,
            Action<AddEditAct1ContractDialogViewModel> customPrepareAction = null,
            Action<AddEditAct1ContractDialogViewModel> customProcessingDialogResultAction = null)
        {
            // заполняем модель данными
            if (customPrepareAction == null)
                customPrepareAction = viewModel =>
                {
                    viewModel.DialogTitle = new Act1ContractDialogTitle().Edit;

                    viewModel.LeaseContracts = new ObservableCollection<LeaseСontractDocument>(_service.GetLeaseContractsAsync().Result.Select(_service.GetLeaseСontractDocument));
                    viewModel.SelectedLeaseContract = viewModel.LeaseContracts.Single(c => c.Id == contractDocument.LeaseContract.Id);

                    viewModel.BeginDate = contractDocument.BeginDate;
                    viewModel.ContractNumber = contractDocument.Number;

                    viewModel.SetIcon(DialogIcon.Edit);
                };

            // применяем логику к результату диалога
            if (customProcessingDialogResultAction == null)
                customProcessingDialogResultAction = viewModel =>
                {
                    var newContract = new Act1Contract()
                    {
                        Id = contractDocument.Id,
                        Number = viewModel.ContractNumber,
                        LeaseContractId = viewModel.SelectedLeaseContract.Id,
                        BeginDate = viewModel.BeginDate.ToShortDateString()
                    };

                    var result = _service.EditAct1ContractAsync(newContract).Result;
                };

            ShowDialog(customPrepareAction, customProcessingDialogResultAction);
        }

        #endregion Act1ContractDocument

        #region Act2ContractDocument

        public void OpenAddAct2ContractDialog(
            LeaseСontractDocument contract = null,
            Action<AddEditAct2ContractDialogViewModel> customPrepareAction = null,
            Action<AddEditAct2ContractDialogViewModel> customProcessingDialogResultAction = null)
        {
            // заполняем модель данными
            if (customPrepareAction == null)
                customPrepareAction = viewModel =>
                {
                    viewModel.DialogTitle = new Act2ContractDialogTitle().Add;

                    viewModel.LeaseContracts = new ObservableCollection<LeaseСontractDocument>(_service.GetLeaseContractsAsync().Result.Select(_service.GetLeaseСontractDocument));
                    if (contract != null) viewModel.SelectedLeaseContract = viewModel.LeaseContracts.Single(c => c.Id == contract.Id);
                };

            // применяем логику к результату диалога
            if (customProcessingDialogResultAction == null)
                customProcessingDialogResultAction = viewModel =>
                {
                    var newContract = new Act2Contract()
                    {
                        Number = viewModel.ContractNumber,
                        LeaseContractId = viewModel.SelectedLeaseContract.Id,
                        BeginDate = viewModel.BeginDate.ToShortDateString()
                    };

                    var result = _service.AddAct2ContractAsync(newContract).Result;
                };

            ShowDialog(customPrepareAction, customProcessingDialogResultAction);
        }

        public void OpenEditContractDialog(
            Act2ContractDocument contractDocument,
            Action<AddEditAct2ContractDialogViewModel> customPrepareAction = null,
            Action<AddEditAct2ContractDialogViewModel> customProcessingDialogResultAction = null)
        {
            // заполняем модель данными
            if (customPrepareAction == null)
                customPrepareAction = viewModel =>
                {
                    viewModel.DialogTitle = new Act2ContractDialogTitle().Edit;

                    viewModel.LeaseContracts = new ObservableCollection<LeaseСontractDocument>(_service.GetLeaseContractsAsync().Result.Select(_service.GetLeaseСontractDocument));
                    viewModel.SelectedLeaseContract = viewModel.LeaseContracts.Single(c => c.Id == contractDocument.LeaseContract.Id);

                    viewModel.BeginDate = contractDocument.BeginDate;
                    viewModel.ContractNumber = contractDocument.Number;
                };

            // применяем логику к результату диалога
            if (customProcessingDialogResultAction == null)
                customProcessingDialogResultAction = viewModel =>
                {
                    var newContract = new Act2Contract()
                    {
                        Id = contractDocument.Id,
                        Number = viewModel.ContractNumber,
                        LeaseContractId = viewModel.SelectedLeaseContract.Id,
                        BeginDate = viewModel.BeginDate.ToShortDateString()
                    };

                    var result = _service.EditAct2ContractAsync(newContract).Result;
                };

            ShowDialog(customPrepareAction, customProcessingDialogResultAction);
        }

        #endregion Act2ContractDocument

        #region UtilityServiceContractDocument

        private Func<int, string> GetCompanyFunc => (id) => GetEntities().Single(e => e.Id == id).ShortName;

        public void OpenAddUtilityServiceContractDialog(
          LeaseСontractDocument contract = null,
          Action<AddEditUtilityServiceContractDialogViewModel> customPrepareAction = null,
          Action<AddEditUtilityServiceContractDialogViewModel> customProcessingDialogResultAction = null)
        {
            // заполняем модель данными
            if (customPrepareAction == null)
                customPrepareAction = viewModel =>
                {
                    viewModel.DialogTitle = new UtilityServiceContractDialogTitle().Add;

                    viewModel.GetCompanyName = GetCompanyFunc;

                    viewModel.LeaseContracts = new ObservableCollection<LeaseСontractDocument>(
                        _service.GetLeaseContractsAsync().Result.Select(_service.GetLeaseСontractDocument).OrderByDescending(o => o.Id));

                    if (contract != null) viewModel.SelectedLeaseContract = viewModel.LeaseContracts.Single(c => c.Id == contract.Id);

                    viewModel.SetIcon(DialogIcon.Add);
                };

            // применяем логику к результату диалога
            if (customProcessingDialogResultAction == null)
                customProcessingDialogResultAction = viewModel =>
                {
                    var utilityServiceContract = new UtilityServiceContract()
                    {
                        BeginDate = viewModel.BeginDate.ToShortDateString(),
                        LeaseContractId = viewModel.SelectedLeaseContract.Id,
                        Number = viewModel.Number,
                        ManagerCompanyId = viewModel.SelectedLeaseContract.Building.ManagerCompanyId,
                        RenterId = viewModel.SelectedLeaseContract.Renter.Id,
                        UtilityServicePrice = viewModel.PriceForOneMeter,
                        UtilityServiceTotalPrice = viewModel.TotalPrice
                    };

                    var result = _service.AddUtilityServiceContractAsync(utilityServiceContract).Result;

                    utilityServiceContract.Id =
                        _service
                        .GetUtilityServiceContractsAsync()
                        .Result
                        .Single(c => c.Number == viewModel.Number && c.BeginDate == viewModel.BeginDate.ToShortDateString()).Id;

                    _service.UpdateUtilityServiceOffices(utilityServiceContract, viewModel.SelectedOffices.ToArray());
                };

            ShowDialog(customPrepareAction, customProcessingDialogResultAction);
        }

        public void OpenEditContractDialog(
            UtilityServiceContractDocument contractDocument,
            Action<AddEditUtilityServiceContractDialogViewModel> customPrepareAction = null,
            Action<AddEditUtilityServiceContractDialogViewModel> customProcessingDialogResultAction = null)
        {
            // заполняем модель данными
            if (customPrepareAction == null)
                customPrepareAction = viewModel =>
                {
                    viewModel.DialogTitle = new UtilityServiceContractDialogTitle().Edit;

                    viewModel.GetCompanyName = GetCompanyFunc;

                    viewModel.LeaseContracts = new ObservableCollection<LeaseСontractDocument>(
                        _service.GetLeaseContractsAsync().Result.Select(_service.GetLeaseСontractDocument).OrderByDescending(o => o.Id));

                    viewModel.SelectedLeaseContract = viewModel.LeaseContracts.Single(c => c.Id == contractDocument.LeaseContract.Id);

                    viewModel.BeginDate = contractDocument.BeginDate;
                    viewModel.Number = contractDocument.Number;

                    viewModel.SetIcon(DialogIcon.Edit);
                };

            // применяем логику к результату диалога
            if (customProcessingDialogResultAction == null)
                customProcessingDialogResultAction = viewModel =>
                {
                    var utilityServiceContract = new UtilityServiceContract()
                    {
                        Id = contractDocument.Id,
                        BeginDate = viewModel.BeginDate.ToShortDateString(),
                        LeaseContractId = viewModel.SelectedLeaseContract.Id,
                        Number = viewModel.Number,
                        ManagerCompanyId = viewModel.SelectedLeaseContract.Building.ManagerCompanyId,
                        RenterId = viewModel.SelectedLeaseContract.Renter.Id,
                        UtilityServicePrice = viewModel.PriceForOneMeter,
                        UtilityServiceTotalPrice = viewModel.TotalPrice
                    };

                    var result = _service.EditUtilityServiceContractAsync(utilityServiceContract).Result;

                    _service.UpdateUtilityServiceOffices(utilityServiceContract, viewModel.SelectedOffices.ToArray());
                };

            ShowDialog(customPrepareAction, customProcessingDialogResultAction);
        }

        #endregion UtilityServiceContractDocument

        #region ClaimContractDocument

        public void OpenAddClaimContractDialog(
           LeaseСontractDocument contract = null,
           Action<AddEditClaimContractDialogViewModel> customPrepareAction = null,
           Action<AddEditClaimContractDialogViewModel> customProcessingDialogResultAction = null)
        {
            // заполняем модель данными
            if (customPrepareAction == null)
                customPrepareAction = viewModel =>
                {
                    viewModel.DialogTitle = new ClaimContractDialogTitle().Add;

                    viewModel.LeaseContracts = new ObservableCollection<LeaseСontractDocument>(_service.GetLeaseContractsAsync().Result.Select(_service.GetLeaseСontractDocument));
                    if (contract != null) viewModel.SelectedLeaseContract = viewModel.LeaseContracts.Single(c => c.Id == contract.Id);

                    viewModel.UtilityServiceContracts = new ObservableCollection<UtilityServiceContract>(_service.GetUtilityServiceContractsAsync().Result);
                    if (contract != null) viewModel.SelectedUtilityServiceContract = viewModel.UtilityServiceContracts.SingleOrDefault(c => c.LeaseContractId == contract.Id);

                    viewModel.SetIcon(DialogIcon.Add);
                };

            // применяем логику к результату диалога
            if (customProcessingDialogResultAction == null)
                customProcessingDialogResultAction = viewModel =>
                {
                    var newContract = new ClaimContract()
                    {
                        Number = viewModel.Number,
                        LeaseContractId = viewModel.SelectedLeaseContract.Id,
                        BeginDate = viewModel.BeginDate.ToShortDateString(),
                        UtilityServiceContractId = viewModel.SelectedUtilityServiceContract.Id,
                        ArendDebt = viewModel.ArendDebt,
                        UtilityServiceDebt = viewModel.UtilityServiceDebt,
                        ClaimDate = viewModel.ClaimDate.ToShortDateString()
                    };

                    var result = _service.AddClaimContractAsync(newContract).Result;
                };

            ShowDialog(customPrepareAction, customProcessingDialogResultAction);
        }

        public void OpenEditContractDialog(
           ClaimContractDocument contractDocument,
           Action<AddEditClaimContractDialogViewModel> customPrepareAction = null,
           Action<AddEditClaimContractDialogViewModel> customProcessingDialogResultAction = null)
        {
            // заполняем модель данными
            if (customPrepareAction == null)
                customPrepareAction = viewModel =>
                {
                    viewModel.DialogTitle = new ClaimContractDialogTitle().Edit;

                    viewModel.LeaseContracts = new ObservableCollection<LeaseСontractDocument>(_service.GetLeaseContractsAsync().Result.Select(_service.GetLeaseСontractDocument));
                    viewModel.SelectedLeaseContract = viewModel.LeaseContracts.Single(c => c.Id == contractDocument.LeaseContract.Id);

                    viewModel.UtilityServiceContracts = new ObservableCollection<UtilityServiceContract>(_service.GetUtilityServiceContractsAsync().Result);
                    viewModel.SelectedUtilityServiceContract = viewModel.UtilityServiceContracts.Single(c => c.Id == contractDocument.UtilityServiceContract.Id);

                    viewModel.Number = contractDocument.Number;
                    viewModel.BeginDate = contractDocument.BeginDate;
                    viewModel.ArendDebt = contractDocument.ArendDebt;
                    viewModel.UtilityServiceDebt = contractDocument.UtilityServiceDebt;

                    viewModel.SetIcon(DialogIcon.Edit);
                };

            // применяем логику к результату диалога
            if (customProcessingDialogResultAction == null)
                customProcessingDialogResultAction = viewModel =>
                {
                    var newContract = new ClaimContract()
                    {
                        Id = contractDocument.Id,
                        Number = viewModel.Number,
                        LeaseContractId = viewModel.SelectedLeaseContract.Id,
                        BeginDate = viewModel.BeginDate.ToShortDateString(),
                        UtilityServiceContractId = viewModel.SelectedUtilityServiceContract.Id,
                        ArendDebt = viewModel.ArendDebt,
                        UtilityServiceDebt = viewModel.UtilityServiceDebt,
                        ClaimDate = viewModel.ClaimDate.ToShortDateString()
                    };

                    var result = _service.EditClaimContractAsync(newContract).Result;
                };

            ShowDialog(customPrepareAction, customProcessingDialogResultAction);
        }

        #endregion ClaimContractDocument

        #region ChangeArendPriceContractDocument

        public void OpenAddChangeArendPriceContractDialog(
           LeaseСontractDocument contract = null,
           Action<AddEditChangeArendPriceDialogViewModel> customPrepareAction = null,
           Action<AddEditChangeArendPriceDialogViewModel> customProcessingDialogResultAction = null)
        {
            // заполняем модель данными
            if (customPrepareAction == null)
                customPrepareAction = viewModel =>
                {
                    viewModel.DialogTitle = new ChangeArendPriceContractDialogTitle().Add;

                    viewModel.LeaseContracts = new ObservableCollection<LeaseСontractDocument>(_service.GetLeaseContractsAsync().Result.Select(_service.GetLeaseСontractDocument));
                    if (contract != null) viewModel.SelectedLeaseContract = viewModel.LeaseContracts.Single(c => c.Id == contract.Id);

                    viewModel.SetIcon(DialogIcon.Add);
                };

            // применяем логику к результату диалога
            if (customProcessingDialogResultAction == null)
                customProcessingDialogResultAction = viewModel =>
                {
                    var newContract = new ChangeArendPriceContract()
                    {
                        Number = viewModel.Number,
                        LeaseContractId = viewModel.SelectedLeaseContract.Id,
                        BeginDate = viewModel.BeginDate.ToShortDateString(),
                        NewOfficePrice = viewModel.NewOfficePrice,
                        NewOfficeTotalPrice = viewModel.NewTotalOfficePrice
                    };

                    var result = _service.AddChangeArendPriceContractAsync(newContract).Result;
                };

            ShowDialog(customPrepareAction, customProcessingDialogResultAction);
        }

        public void OpenEditContractDialog(
          ChangeArendPriceContractDocument contractDocument,
          Action<AddEditChangeArendPriceDialogViewModel> customPrepareAction = null,
          Action<AddEditChangeArendPriceDialogViewModel> customProcessingDialogResultAction = null)
        {
            // заполняем модель данными
            if (customPrepareAction == null)
                customPrepareAction = viewModel =>
                {
                    viewModel.DialogTitle = new ChangeArendPriceContractDialogTitle().Edit;

                    viewModel.LeaseContracts = new ObservableCollection<LeaseСontractDocument>(_service.GetLeaseContractsAsync().Result.Select(_service.GetLeaseСontractDocument));
                    viewModel.SelectedLeaseContract = viewModel.LeaseContracts.Single(c => c.Id == contractDocument.LeaseContract.Id);

                    viewModel.BeginDate = contractDocument.BeginDate;
                    viewModel.Number = contractDocument.Number;
                    viewModel.NewOfficePrice = contractDocument.NewOfficePrice;
                    viewModel.NewTotalOfficePrice = contractDocument.NewOfficeTotalPrice;

                    viewModel.SetIcon(DialogIcon.Edit);
                };

            // применяем логику к результату диалога
            if (customProcessingDialogResultAction == null)
                customProcessingDialogResultAction = viewModel =>
                {
                    var newContract = new ChangeArendPriceContract()
                    {
                        Id = contractDocument.Id,
                        Number = viewModel.Number,
                        LeaseContractId = viewModel.SelectedLeaseContract.Id,
                        BeginDate = viewModel.BeginDate.ToShortDateString(),
                        NewOfficePrice = viewModel.NewOfficePrice,
                        NewOfficeTotalPrice = viewModel.NewTotalOfficePrice
                    };

                    var result = _service.EditChangeArendPriceContractAsync(newContract).Result;
                };

            ShowDialog(customPrepareAction, customProcessingDialogResultAction);
        }

        #endregion ChangeArendPriceContractDocument

        #region TerminationContractDocument

        public void OpenAddTerminationContractDialog(
           LeaseСontractDocument contract = null,
           Action<AddEditTerminationDialogViewModel> customPrepareAction = null,
           Action<AddEditTerminationDialogViewModel> customProcessingDialogResultAction = null)
        {
            // заполняем модель данными
            if (customPrepareAction == null)
                customPrepareAction = viewModel =>
                {
                    viewModel.DialogTitle = new TerminationContractDialogTitle().Add;
                    viewModel.LeaseContracts = new ObservableCollection<LeaseСontractDocument>(_service.GetLeaseContractsAsync().Result.Select(_service.GetLeaseСontractDocument));
                    if (contract != null) viewModel.SelectedLeaseContract = viewModel.LeaseContracts.Single(c => c.Id == contract.Id);

                    viewModel.SetIcon(DialogIcon.Add);
                };

            // применяем логику к результату диалога
            if (customProcessingDialogResultAction == null)
                customProcessingDialogResultAction = viewModel =>
                {
                    var newContract = new TerminationContract()
                    {
                        LeaseContractId = viewModel.SelectedLeaseContract.Id,
                        BeginDate = viewModel.BeginDate.ToShortDateString()
                    };

                    var result = _service.AddTerminationContractAsync(newContract).Result;

                    viewModel.SelectedLeaseContract.IsActual = false;
                    var resultLeaseContract = _service.EditLeaseContractAsync(ContractsExtensions.DocumentToModel(viewModel.SelectedLeaseContract)).Result;
                };

            ShowDialog(customPrepareAction, customProcessingDialogResultAction);
        }

        public void OpenEditContractDialog(
          TerminationContractDocument contractDocument,
          Action<AddEditTerminationDialogViewModel> customPrepareAction = null,
          Action<AddEditTerminationDialogViewModel> customProcessingDialogResultAction = null)
        {
            // заполняем модель данными
            if (customPrepareAction == null)
                customPrepareAction = viewModel =>
                {
                    viewModel.DialogTitle = new TerminationContractDialogTitle().Edit;
                    viewModel.LeaseContracts = new ObservableCollection<LeaseСontractDocument>(_service.GetLeaseContractsAsync().Result.Select(_service.GetLeaseСontractDocument));
                    viewModel.SelectedLeaseContract = viewModel.LeaseContracts.Single(c => c.Id == contractDocument.LeaseContract.Id);

                    viewModel.BeginDate = contractDocument.BeginDate;

                    viewModel.SetIcon(DialogIcon.Edit);
                };

            // применяем логику к результату диалога
            if (customProcessingDialogResultAction == null)
                customProcessingDialogResultAction = viewModel =>
                {
                    var newContract = new TerminationContract()
                    {
                        Id = contractDocument.Id,
                        LeaseContractId = viewModel.SelectedLeaseContract.Id,
                        BeginDate = viewModel.BeginDate.ToShortDateString()
                    };

                    var result = _service.EditTerminationContractAsync(newContract).Result;
                };

            ShowDialog(customPrepareAction, customProcessingDialogResultAction);
        }

        #endregion TerminationContractDocument

        #region CancelContractDocument

        public void OpenAddCancelContractDialog(
           LeaseСontractDocument contract = null,
           Action<AddEditCancelContractDialogViewModel> customPrepareAction = null,
           Action<AddEditCancelContractDialogViewModel> customProcessingDialogResultAction = null)
        {
            // заполняем модель данными
            if (customPrepareAction == null)
                customPrepareAction = viewModel =>
                {
                    viewModel.DialogTitle = new CancelContractDialogTitle().Add;
                    viewModel.LeaseContracts = new ObservableCollection<LeaseСontractDocument>(_service.GetLeaseContractsAsync().Result.Select(_service.GetLeaseСontractDocument));
                    if (contract != null) viewModel.SelectedLeaseContract = viewModel.LeaseContracts.Single(c => c.Id == contract.Id);

                    viewModel.SetIcon(DialogIcon.Add);
                };

            // применяем логику к результату диалога
            if (customProcessingDialogResultAction == null)
                customProcessingDialogResultAction = viewModel =>
                {
                    var newContract = new CancelContract()
                    {
                        LeaseContractId = viewModel.SelectedLeaseContract.Id,
                        BeginDate = viewModel.BeginDate.ToShortDateString(),
                        LandLordId = viewModel.SelectedLeaseContract.Landlord.Id,
                        RenterId = viewModel.SelectedLeaseContract.Renter.Id,
                        BuildingId = viewModel.SelectedLeaseContract.Building.Id,
                        MailDate = viewModel.MailDate.ToShortDateString(),
                        MailNumber = viewModel.MailNumber
                    };

                    var result = _service.AddCancelContractAsync(newContract).Result;
                };

            ShowDialog(customPrepareAction, customProcessingDialogResultAction);
        }

        public void OpenEditContractDialog(
           CancelContractDocument contractDocument,
           Action<AddEditCancelContractDialogViewModel> customPrepareAction = null,
           Action<AddEditCancelContractDialogViewModel> customProcessingDialogResultAction = null)
        {
            // заполняем модель данными
            if (customPrepareAction == null)
                customPrepareAction = viewModel =>
                {
                    viewModel.DialogTitle = new CancelContractDialogTitle().Edit;
                    viewModel.LeaseContracts = new ObservableCollection<LeaseСontractDocument>(_service.GetLeaseContractsAsync().Result.Select(_service.GetLeaseСontractDocument));
                    viewModel.SelectedLeaseContract = viewModel.LeaseContracts.Single(c => c.Id == contractDocument.LeaseContract.Id);

                    viewModel.BeginDate = contractDocument.BeginDate;
                    viewModel.MailNumber = contractDocument.MailNumber;
                    viewModel.MailDate = contractDocument.MailDate;

                    viewModel.SetIcon(DialogIcon.Edit);
                };

            // применяем логику к результату диалога
            if (customProcessingDialogResultAction == null)
                customProcessingDialogResultAction = viewModel =>
                {
                    var newContract = new CancelContract()
                    {
                        Id = contractDocument.Id,
                        LeaseContractId = viewModel.SelectedLeaseContract.Id,
                        BeginDate = viewModel.BeginDate.ToShortDateString(),
                        LandLordId = viewModel.SelectedLeaseContract.Landlord.Id,
                        RenterId = viewModel.SelectedLeaseContract.Renter.Id,
                        BuildingId = viewModel.SelectedLeaseContract.Building.Id,
                        MailDate = viewModel.MailDate.ToShortDateString(),
                        MailNumber = viewModel.MailNumber
                    };

                    var result = _service.EditCancelContractAsync(newContract).Result;
                };

            ShowDialog(customPrepareAction, customProcessingDialogResultAction);
        }

        #endregion CancelContractDocument

        #region Entity

        public void OpenAddEntityDialog(
         Action<AddEditEntityDialogViewModel> customPrepareAction = null,
         Action<AddEditEntityDialogViewModel> customProcessingDialogResultAction = null)
        {
            // заполняем модель данными
            if (customPrepareAction == null)
                customPrepareAction = viewModel =>
                {
                    viewModel.DialogTitle = new EntityDialogTitle().Add;

                    viewModel.SetIcon(DialogIcon.Add);
                };

            // применяем логику к результату диалога
            if (customProcessingDialogResultAction == null)
                customProcessingDialogResultAction = viewModel =>
                {
                    var entity = new Entity()
                    {
                        Name = viewModel.Name,
                        Represented = viewModel.Represented,
                        Agreement = viewModel.Agreement,
                        ShortName = viewModel.ShortName,
                        INN = viewModel.INN,
                        OGRN = viewModel.OGRN,
                        Address = viewModel.Address,
                        RS = viewModel.RS,
                        BIK = viewModel.BIK,
                        Email = viewModel.Email,
                        Phone = viewModel.Phone,
                        BaseOn = viewModel.BaseOn,
                        Initials = viewModel.Initials,
                        EntityTypeId = 2,
                        Requisites = viewModel.Requisites,
                        ShortRequisites = viewModel.ShortRequisites
                    };

                    var result = _service.AddEntityAsync(entity).Result;
                };

            ShowDialog(customPrepareAction, customProcessingDialogResultAction);
        }

        public void OpenEditEntityDialog(
            Entity entity,
            Action<AddEditEntityDialogViewModel> customPrepareAction = null,
            Action<AddEditEntityDialogViewModel> customProcessingDialogResultAction = null)
        {
            // заполняем модель данными
            if (customPrepareAction == null)
                customPrepareAction = viewModel =>
                {
                    viewModel.DialogTitle = new EntityDialogTitle().Edit;
                    viewModel.Name = entity.Name;
                    viewModel.Represented = entity.Represented;
                    viewModel.Agreement = entity.Agreement;
                    viewModel.ShortName = entity.ShortName;
                    viewModel.INN = entity.INN;
                    viewModel.OGRN = entity.OGRN;
                    viewModel.Address = entity.Address;
                    viewModel.RS = entity.RS;
                    viewModel.BIK = entity.BIK;
                    viewModel.Email = entity.Email;
                    viewModel.Phone = entity.Phone;
                    viewModel.BaseOn = entity.BaseOn;
                    viewModel.Initials = entity.Initials;
                    viewModel.Requisites = entity.Requisites;

                    viewModel.SetIcon(DialogIcon.Edit);
                };

            // применяем логику к результату диалога
            if (customProcessingDialogResultAction == null)
                customProcessingDialogResultAction = viewModel =>
                {
                    var newEntity = new Entity()
                    {
                        Id = entity.Id,
                        Name = viewModel.Name,
                        Represented = viewModel.Represented,
                        Agreement = viewModel.Agreement,
                        ShortName = viewModel.ShortName,
                        INN = viewModel.INN,
                        OGRN = viewModel.OGRN,
                        Address = viewModel.Address,
                        RS = viewModel.RS,
                        BIK = viewModel.BIK,
                        Email = viewModel.Email,
                        Phone = viewModel.Phone,
                        BaseOn = viewModel.BaseOn,
                        Initials = viewModel.Initials,
                        Requisites = viewModel.Requisites,
                        ShortRequisites = viewModel.ShortRequisites,
                        EntityTypeId = 2
                    };

                    var result = _service.EditEntityAsync(newEntity).Result;
                };

            ShowDialog(customPrepareAction, customProcessingDialogResultAction);
        }

        #endregion Entity
    }
}