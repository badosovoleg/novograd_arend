﻿namespace NovogradArend.Desktop.ViewModels.Dialogs
{
    public sealed class DialogIcon
    {
        private readonly string path;
        private readonly int value;

        public static readonly DialogIcon Default =
            new DialogIcon(0, @"..\..\Resources\Icons\add.png");
        public static readonly DialogIcon Add =
            new DialogIcon(1, @"..\..\Resources\Icons\add.png");
        public static readonly DialogIcon Edit =
            new DialogIcon(2, @"..\..\Resources\Icons\edit.png");

        private DialogIcon(int value, string path)
        {
            this.path = path;
            this.value = value;
        }

        public override string ToString() => path;
    }
}
