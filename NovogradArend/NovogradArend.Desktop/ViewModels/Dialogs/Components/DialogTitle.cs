﻿using NovogradArend.Model;

namespace NovogradArend.Desktop.ViewModels.Dialogs
{
    public class DialogTitle
    {
        private readonly string name;
        private readonly int value;

        protected virtual string Target => "объекта";

        public DialogTitle Default => new DialogTitle(0, "Какой то заголовок");

        public DialogTitle Add => new DialogTitle(1, $"Добавление {Target}");

        public DialogTitle Edit => new DialogTitle(2, $"Редактирование {Target}");

        private DialogTitle(int value, string name)
        {
            this.name = name;
            this.value = value;
        }

        public DialogTitle() { }

        public sealed override string ToString() => name;
    }

    #region Заголовки

    public sealed class LeaseContractDialogTitle : DialogTitle
    {
        protected override string Target => ContractTypeEnum.LeaseDialogTitle;
    }

    public sealed class Act1ContractDialogTitle : DialogTitle
    {
        protected override string Target => ContractTypeEnum.ACT1DialogTitle;
    }

    public sealed class Act2ContractDialogTitle : DialogTitle
    {
        protected override string Target => ContractTypeEnum.ACT2DialogTitle;
    }

    public sealed class UtilityServiceContractDialogTitle : DialogTitle
    {
        protected override string Target => ContractTypeEnum.UtilityServiceDialogTitle;
    }

    public sealed class CancelContractDialogTitle : DialogTitle
    {
        protected override string Target => ContractTypeEnum.CancelDialogTitle;
    }

    public sealed class ChangeArendPriceContractDialogTitle : DialogTitle
    {
        protected override string Target => ContractTypeEnum.ChangeArendPriceDialogTitle;
    }

    public sealed class ClaimContractDialogTitle : DialogTitle
    {
        protected override string Target => ContractTypeEnum.ClaimDialogTitle;
    }

    public sealed class TerminationContractDialogTitle : DialogTitle
    {
        protected override string Target => ContractTypeEnum.TerminationDialogTitle;
    }

    public sealed class EntityDialogTitle : DialogTitle
    {
        protected override string Target => "организации (юр. лица)";
    }

    #endregion

}