﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using NovogradArend.Desktop.Contracts;
using NovogradArend.Desktop.Contracts.Components;
using NovogradArend.Desktop.Extensions;
using NovogradArend.Desktop.Services;
using NovogradArend.Desktop.ViewModels.Dialogs;
using NovogradArend.Model;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace NovogradArend.Desktop.ViewModels
{
    public class AdditionalContractsViewModel : ViewModelBase, IAdditionalContractsCommands
    {
        #region Fields

        private readonly IObjectsService _service;
        private readonly NovogradDialogService _dialogService;
        private ObservableCollection<ContractViewer> _contracts;
        private ContractViewer _selectedContract;

        private ObservableCollection<ContractTypeEnum> _contractTypes
           = new ObservableCollection<ContractTypeEnum>()
           {
                ContractTypeEnum.ACT1,
                ContractTypeEnum.ACT2,
                ContractTypeEnum.UtilityService,
                ContractTypeEnum.Claim,
                ContractTypeEnum.Cancel,
                ContractTypeEnum.Termination,
                ContractTypeEnum.ChangeArendPrice
           };

        private ContractTypeEnum _selectedContractType;
        private bool _isAddButtonEnabled = true;
        private bool _isEditButtonEnabled = false;
        private bool _isPrintButtonEnabled = false;

        #endregion Fields

        #region Members

        public ObservableCollection<ContractViewer> Contracts { get => _contracts; set => Set(ref _contracts, value); }

        public ContractViewer SelectedContract
        {
            get => _selectedContract; set
            {
                IsEditButtonEnabled = IsPrintButtonEnabled = true;
                Set(ref _selectedContract, value);
            }
        }

        public ObservableCollection<ContractTypeEnum> ContractTypes { get => _contractTypes; set => Set(ref _contractTypes, value); }

        public ContractTypeEnum SelectedContractType
        {
            get => _selectedContractType;
            set
            {
                Set(ref _selectedContractType, value);
                IsEditButtonEnabled = IsPrintButtonEnabled = false;
                SelectedContractTypeChanged();
            }
        }

        public string Header { get; set; } = "Договора";

        public bool IsAddButtonEnabled { get => _isAddButtonEnabled; set => Set(ref _isAddButtonEnabled, value); }

        public bool IsEditButtonEnabled
        {
            get => _isEditButtonEnabled;
            set
            {
                _isEditButtonEnabled = value;
                RaisePropertyChanged(nameof(IsEditButtonEnabled));
            }
        }

        public bool IsPrintButtonEnabled
        {
            get => _isPrintButtonEnabled;
            set
            {
                _isPrintButtonEnabled = value;
                RaisePropertyChanged(nameof(IsPrintButtonEnabled));
            }
        }

        #endregion Members

        #region Commands

        public ICommand EditContract => new RelayCommand(() => EditContractHandler());

        public ICommand PrintContract => new RelayCommand(() => PrintContractHandler());

        public ICommand AddAct1Contract => new RelayCommand(() => _dialogService.OpenAddAct1ContractDialog());

        public ICommand AddAct2Contract => new RelayCommand(() => _dialogService.OpenAddAct2ContractDialog());

        public ICommand AddChangeArendPriceContract => new RelayCommand(() => _dialogService.OpenAddChangeArendPriceContractDialog());

        public ICommand AddUtilityServiceContract => new RelayCommand(() => _dialogService.OpenAddUtilityServiceContractDialog());

        public ICommand AddCancelContract => new RelayCommand(() => _dialogService.OpenAddCancelContractDialog());

        public ICommand AddClaimContract => new RelayCommand(() => _dialogService.OpenAddClaimContractDialog());

        public ICommand AddTerminationContract => new RelayCommand(() => _dialogService.OpenAddTerminationContractDialog());

        #endregion Commands

        public AdditionalContractsViewModel(IObjectsService service)
        {
            _service = service;
            _dialogService = new NovogradDialogService(this, service);
            SelectedContractType = ContractTypes[0];
        }

        #region Methods

        private void SelectedContractTypeChanged()
        {
            var LeaseContract = new Func<int, LeaseContract>(id => _service.GetLeaseContractAsync(id).Result);

            if (SelectedContractType == ContractTypeEnum.ACT1)
                Contracts = new ObservableCollection<ContractViewer>(_service.GetAct1ContractsAsync().Result
                    .Select(contract =>
                    new ContractViewer(contract, LeaseContract(contract.LeaseContractId), _service.GetAct1ContractDocument(contract))));
            else if (SelectedContractType == ContractTypeEnum.ACT2)
                Contracts = new ObservableCollection<ContractViewer>(_service.GetAct2ContractsAsync().Result
                    .Select(contract =>
                    new ContractViewer(contract, LeaseContract(contract.LeaseContractId), _service.GetAct2ContractDocument(contract))));
            else if (SelectedContractType == ContractTypeEnum.UtilityService)
                Contracts = new ObservableCollection<ContractViewer>(_service.GetUtilityServiceContractsAsync().Result
                    .Select(contract =>
                    new ContractViewer(contract, LeaseContract(contract.LeaseContractId), _service.GetUtilityServiceContractDocument(contract))));
            else if (SelectedContractType == ContractTypeEnum.Claim)
                Contracts = new ObservableCollection<ContractViewer>(_service.GetClaimContractsAsync().Result
                    .Select(contract =>
                    new ContractViewer(contract, LeaseContract(contract.LeaseContractId), _service.GetClaimContractDocument(contract))));
            else if (SelectedContractType == ContractTypeEnum.ChangeArendPrice)
                Contracts = new ObservableCollection<ContractViewer>(_service.GetChangeArendPriceContractsAsync().Result
                    .Select(contract =>
                    new ContractViewer(contract, LeaseContract(contract.LeaseContractId), _service.GetChangeArendPriceContractDocument(contract))));
            else if (SelectedContractType == ContractTypeEnum.Termination)
                Contracts = new ObservableCollection<ContractViewer>(_service.GetTerminationContractsAsync().Result
                    .Select(contract =>
                    new ContractViewer(contract, LeaseContract(contract.LeaseContractId), _service.GetTerminationContractDocument(contract))));
            else if (SelectedContractType == ContractTypeEnum.Cancel)
                Contracts = new ObservableCollection<ContractViewer>(_service.GetCancelContractsAsync().Result
                    .Select(contract =>
                    new ContractViewer(contract, LeaseContract(contract.LeaseContractId), _service.GetCancelContractDocument(contract))));
            else Contracts = null;
        }

        private void EditContractHandler()
        {
            if (SelectedContractType == ContractTypeEnum.ACT1) _dialogService.OpenEditContractDialog((Act1ContractDocument)SelectedContract.Document);
            else if (SelectedContractType == ContractTypeEnum.ACT2) _dialogService.OpenEditContractDialog((Act2ContractDocument)SelectedContract.Document);
            else if (SelectedContractType == ContractTypeEnum.UtilityService) _dialogService.OpenEditContractDialog((UtilityServiceContractDocument)SelectedContract.Document);
            else if (SelectedContractType == ContractTypeEnum.Claim) _dialogService.OpenEditContractDialog((ClaimContractDocument)SelectedContract.Document);
            else if (SelectedContractType == ContractTypeEnum.ChangeArendPrice) _dialogService.OpenEditContractDialog((ChangeArendPriceContractDocument)SelectedContract.Document);
            else if (SelectedContractType == ContractTypeEnum.Termination) _dialogService.OpenEditContractDialog((TerminationContractDocument)SelectedContract.Document);
            else if (SelectedContractType == ContractTypeEnum.Cancel) _dialogService.OpenEditContractDialog((CancelContractDocument)SelectedContract.Document);
            else throw new Exception("Не выбран тип редактируемого объекта в выпадающем списке!");

            SelectedContractType = SelectedContractType;
        }

        private void PrintContractHandler() => SelectedContract.Document.OpenAsFinalWordDocument();

        #endregion Methods

        public class ContractViewer
        {
            public int Id { get; set; }
            public string Number { get; set; }
            public string BeginDate { get; set; }
            public string LeaseContractInfo { get; set; }
            public BaseContractDocument Document { get; set; }

            public ContractViewer(IModelWithLeaseContract contract, LeaseContract leaseContract, BaseContractDocument document)
            {
                Id = contract.Id;
                if (contract is IModelWithNumber) Number = ((IModelWithNumber)contract).Number;
                else Number = "б/н";
                BeginDate = contract.BeginDate;
                LeaseContractInfo = $"Договор № {leaseContract.Number} от {DateTime.Parse(leaseContract.BeginDate).ToFormatDate()}";
                Document = document;
            }
        }
    }
}