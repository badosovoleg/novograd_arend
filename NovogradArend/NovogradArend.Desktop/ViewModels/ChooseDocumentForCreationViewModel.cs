﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using NovogradArend.Desktop.Services;
using NovogradArend.Desktop.ViewModels.Dialogs;
using NovogradArend.Model;
using System.Windows.Input;

namespace NovogradArend.Desktop.ViewModels
{
    public class ChooseDocumentForCreationViewModel : ViewModelBase
    {
        private readonly NovogradDialogService _dialogService;

        public string CreateAct1ContractHeader => ContractTypeEnum.ACT1;
        public ICommand CreateAct1Contract => new RelayCommand(() => _dialogService.OpenAddAct1ContractDialog());

        public string CreateAct2ContractHeader => ContractTypeEnum.ACT2;
        public ICommand CreateAct2Contract => new RelayCommand(() => _dialogService.OpenAddAct2ContractDialog());

        public string CreateUtilityServiceContractHeader => ContractTypeEnum.UtilityService;
        public ICommand CreateUtilityServiceContract => new RelayCommand(() => _dialogService.OpenAddUtilityServiceContractDialog());

        public string CreateLeaseContractHeader => ContractTypeEnum.Lease;
        public ICommand CreateLeaseContract => new RelayCommand(() => _dialogService.OpenAddLeaseContractDialog());

        public string CreateChangeArendPriceContractHeader => ContractTypeEnum.ChangeArendPrice;
        public ICommand CreateChangeArendPriceContract => new RelayCommand(() => _dialogService.OpenAddChangeArendPriceContractDialog());

        public string CreateTerminationContractHeader => ContractTypeEnum.Termination;
        public ICommand CreateTerminationContract => new RelayCommand(() => _dialogService.OpenAddTerminationContractDialog());

        public string CreateClaimContractHeader => ContractTypeEnum.Claim;
        public ICommand CreateClaimContract => new RelayCommand(() => _dialogService.OpenAddClaimContractDialog());

        public string CreateCancelContractHeader => ContractTypeEnum.Cancel;
        public ICommand CreateCancelContract => new RelayCommand(() => _dialogService.OpenAddCancelContractDialog());

        public ChooseDocumentForCreationViewModel(IObjectsService service) => _dialogService = new NovogradDialogService(this, service);
    }
}