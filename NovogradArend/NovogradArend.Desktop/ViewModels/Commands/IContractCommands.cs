﻿using System.Windows.Input;

namespace NovogradArend.Desktop.ViewModels
{
    public interface IContractCommands
    {
        string Header { get; set; }

        bool IsAddButtonEnabled { get; set; }

        bool IsEditButtonEnabled { get; set; }

        bool IsPrintButtonEnabled { get; set; }

        ICommand AddContract { get; }

        ICommand EditContract { get; }

        ICommand PrintContract { get; }
    }
}