﻿using System.Windows.Input;

namespace NovogradArend.Desktop.ViewModels
{
    public interface IEntityCommands
    {
        string Header { get; set; }

        bool IsAddButtonEnabled { get; set; }

        bool IsEditButtonEnabled { get; set; }

        ICommand AddEntity { get; }

        ICommand EditEntity { get; }
    }
}