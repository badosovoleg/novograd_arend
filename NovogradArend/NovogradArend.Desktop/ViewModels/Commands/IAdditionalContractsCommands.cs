﻿using System.Windows.Input;

namespace NovogradArend.Desktop.ViewModels
{
    public interface IAdditionalContractsCommands
    {
        string Header { get; set; }

        bool IsAddButtonEnabled { get; set; }

        bool IsEditButtonEnabled { get; set; }

        bool IsPrintButtonEnabled { get; set; }

        ICommand AddAct1Contract { get; }

        ICommand AddAct2Contract { get; }

        ICommand AddChangeArendPriceContract { get; }

        ICommand AddUtilityServiceContract { get; }

        ICommand AddCancelContract { get; }

        ICommand AddClaimContract { get; }

        ICommand AddTerminationContract { get; }

        ICommand EditContract { get; }

        ICommand PrintContract { get; }
    }
}