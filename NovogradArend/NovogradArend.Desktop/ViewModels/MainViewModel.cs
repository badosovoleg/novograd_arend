using CommonServiceLocator;
using Fluent;
using GalaSoft.MvvmLight;
using System;

namespace NovogradArend.Desktop.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        #region Fields

        private ViewModelBase _primaryViewModel;
        private Lazy<AdditionalContractsViewModel> AdditionalContractsViewModel { get; set; }
        private Lazy<LeaseContractViewModel> LeaseContractViewModel { get; set; }
        private Lazy<EntitiesViewModel> EntitiesViewModel { get; set; }
        private RibbonTabItem _selectedRibbonTab;
        private IEntityCommands _entityCommands;
        private IContractCommands _contractCommands;
        private IAdditionalContractsCommands _additionalContractsCommands;

        #endregion Fields

        #region Members

        public ViewModelBase PrimaryViewModel { get => _primaryViewModel; set { Set(ref _primaryViewModel, value); } }

        public ChooseDocumentForCreationViewModel BackstageContent => ServiceLocator.Current.GetInstance<ChooseDocumentForCreationViewModel>();

        public MainViewModelRibbonTabName LeaseContractsTabName => MainViewModelRibbonTabName.LeaseContractsTabName;

        public MainViewModelRibbonTabName EntitiesTabName => MainViewModelRibbonTabName.EntitiesTabName;

        public MainViewModelRibbonTabName AdditionalContractsTabName => MainViewModelRibbonTabName.AdditionalContractsTabName;

        public RibbonTabItem SelectedRibbonTab
        {
            get => _selectedRibbonTab;
            set
            {
                _selectedRibbonTab = value;
                RibbonSelectedTabChangedHandler((MainViewModelRibbonTabName)value.Header);
                RaisePropertyChanged(() => SelectedRibbonTab);
            }
        }

        public IEntityCommands EntityCommands { get => _entityCommands; set { Set(ref _entityCommands, value); } }

        public IContractCommands ContractCommands { get => _contractCommands; set { Set(ref _contractCommands, value); } }

        public IAdditionalContractsCommands AdditionalContractsCommands { get => _additionalContractsCommands; set { Set(ref _additionalContractsCommands, value); } }

        #endregion Members

        public MainViewModel()
        {
            AdditionalContractsViewModel = new Lazy<AdditionalContractsViewModel>(() => ServiceLocator.Current.GetInstance<AdditionalContractsViewModel>());
            LeaseContractViewModel = new Lazy<LeaseContractViewModel>(() => ServiceLocator.Current.GetInstance<LeaseContractViewModel>());
            EntitiesViewModel = new Lazy<EntitiesViewModel>(() => ServiceLocator.Current.GetInstance<EntitiesViewModel>());
        }

        private void RibbonSelectedTabChangedHandler(MainViewModelRibbonTabName selectedRibbonTab)
        {
            if (selectedRibbonTab == LeaseContractsTabName) PrimaryViewModel = LeaseContractViewModel.Value;
            else if (selectedRibbonTab == EntitiesTabName) PrimaryViewModel = EntitiesViewModel.Value;
            else if (selectedRibbonTab == AdditionalContractsTabName) PrimaryViewModel = AdditionalContractsViewModel.Value;

            if (PrimaryViewModel is IEntityCommands)
                EntityCommands = (IEntityCommands)PrimaryViewModel;

            if (PrimaryViewModel is IContractCommands)
                ContractCommands = (IContractCommands)PrimaryViewModel;

            if (PrimaryViewModel is IAdditionalContractsCommands)
                AdditionalContractsCommands = (IAdditionalContractsCommands)PrimaryViewModel;
        }

        public sealed class MainViewModelRibbonTabName
        {
            private readonly string name;
            private readonly int value;

            public static readonly MainViewModelRibbonTabName LeaseContractsTabName =
                new MainViewModelRibbonTabName(1, "�������� ��������");

            public static readonly MainViewModelRibbonTabName EntitiesTabName =
                new MainViewModelRibbonTabName(2, $"����������� ����");

            public static readonly MainViewModelRibbonTabName AdditionalContractsTabName =
                new MainViewModelRibbonTabName(3, $"�������������� ��������");

            private MainViewModelRibbonTabName(int value, string name)
            {
                this.name = name;
                this.value = value;
            }

            public override string ToString()
            {
                return name;
            }
        }
    }
}