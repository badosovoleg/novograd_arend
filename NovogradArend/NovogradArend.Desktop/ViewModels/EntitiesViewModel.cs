﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using NovogradArend.Desktop.Services;
using NovogradArend.Desktop.ViewModels.Dialogs;
using NovogradArend.Model;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace NovogradArend.Desktop.ViewModels
{
    public class EntitiesViewModel : ViewModelBase, IEntityCommands
    {
        #region Fields

        private readonly IObjectsService _service;
        private readonly NovogradDialogService _dialogService;
        private ObservableCollection<EntityViewer> _entities;
        private EntityViewer _selectedEntity;
        private bool _isAddButtonEnabled = true;
        private bool _isEditButtonEnabled = false;
        private bool _isRemoveButtonEnabled = false;

        #endregion Fields

        #region Members

        public ObservableCollection<EntityViewer> Entities
        {
            get => _entities;
            set
            {
                _entities = value;
                RaisePropertyChanged(() => Entities);
            }
        }

        public EntityViewer SelectedEntity
        {
            get => _selectedEntity;
            set
            {
                _selectedEntity = value;
                SetButtonEnabled();
                RaisePropertyChanged(() => SelectedEntity);
            }
        }

        public string Header { get; set; } = "Работа с юр. лицами";

        public bool IsAddButtonEnabled
        {
            get => _isAddButtonEnabled;
            set
            {
                _isAddButtonEnabled = value;
                RaisePropertyChanged(() => IsAddButtonEnabled);
            }
        }

        public bool IsEditButtonEnabled
        {
            get => _isEditButtonEnabled;
            set
            {
                _isEditButtonEnabled = value;
                RaisePropertyChanged(() => IsEditButtonEnabled);
            }
        }

        public bool IsRemoveButtonEnabled
        {
            get => _isRemoveButtonEnabled;
            set
            {
                _isRemoveButtonEnabled = value;
                RaisePropertyChanged(() => IsRemoveButtonEnabled);
            }
        }

        #endregion Members

        #region Commands

        public ICommand AddEntity => new RelayCommand(() => AddEntityHandler());

        public ICommand EditEntity => new RelayCommand(() => EditEntityHandler());

        #endregion Commands

        public EntitiesViewModel(IObjectsService service)
        {
            _service = service;
            _dialogService = new NovogradDialogService(this, service);
            LoadEntities();
        }

        #region Methods

        private void LoadEntities()
        {
            var entityTypes = _service.GetEntityTypesAsync().Result;
            var entities =
                _service.GetEntitiesAsync().Result
                .Select(e => new EntityViewer(e, entityTypes.Single(t => t.Id == e.EntityTypeId).Type));

            Entities = new ObservableCollection<EntityViewer>(entities);
        }

        private void AddEntityHandler()
        {
            _dialogService.OpenAddEntityDialog();
            LoadEntities();
        }

        private void EditEntityHandler()
        {
            _dialogService.OpenEditEntityDialog(SelectedEntity);
            LoadEntities();
            SetButtonEnabled();
        }

        private void SetButtonEnabled() => IsEditButtonEnabled = IsRemoveButtonEnabled = SelectedEntity != null;

        public class EntityViewer : Entity
        {
            public string Type { get; set; }

            public EntityViewer(Entity entity, string type)
            {
                this.Name = entity.Name;
                this.Id = entity.Id;
                this.Represented = entity.Represented;
                this.Agreement = entity.Agreement;
                this.EntityTypeId = entity.EntityTypeId;
                this.Requisites = entity.Requisites;
                this.ShortRequisites = entity.ShortRequisites;
                this.INN = entity.INN;
                this.OGRN = entity.OGRN;
                this.RS = entity.RS;
                this.BIK = entity.BIK;
                this.Email = entity.Email;
                this.Phone = entity.Phone;
                this.ShortName = entity.ShortName;
                this.Initials = entity.Initials;
                this.BaseOn = entity.BaseOn;
                this.Address = entity.Address;
                this.Type = type;
            }
        }

        #endregion Methods
    }
}