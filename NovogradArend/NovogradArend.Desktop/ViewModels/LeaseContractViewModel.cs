﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using NovogradArend.Desktop.Contracts;
using NovogradArend.Desktop.Contracts.Components;
using NovogradArend.Desktop.Services;
using NovogradArend.Desktop.ViewModels.Dialogs;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace NovogradArend.Desktop.ViewModels
{
    public class LeaseContractViewModel : ViewModelBase, IContractCommands
    {
        #region Fields

        private readonly IObjectsService _service;
        private readonly NovogradDialogService _dialogService;
        private ObservableCollection<LeaseСontractDocument> _contracts;
        private LeaseСontractDocument _selectedContract;
        private bool _isAddButtonEnabled = true;
        private bool _isEditButtonEnabled = false;
        private bool _isPrintButtonEnabled = false;

        #endregion Fields

        #region Members

        public ObservableCollection<LeaseСontractDocument> Contracts
        {
            get => _contracts;
            set
            {
                _contracts = value;
                RaisePropertyChanged(() => Contracts);
            }
        }

        public LeaseСontractDocument SelectedContract
        {
            get => _selectedContract;
            set
            {
                _selectedContract = value;
                RaisePropertyChanged(() => SelectedContract);
                IsEditButtonEnabled = IsPrintButtonEnabled = true;
            }
        }

        public string Header { get; set; } = "Договора";

        public bool IsAddButtonEnabled { get => _isAddButtonEnabled; set => Set(ref _isAddButtonEnabled, value); }

        public bool IsEditButtonEnabled { get => _isEditButtonEnabled; set => Set(ref _isEditButtonEnabled, value); }

        public bool IsPrintButtonEnabled { get => _isPrintButtonEnabled; set => Set(ref _isPrintButtonEnabled, value); }

        #endregion Members

        #region Commands

        public ICommand AddContract => new RelayCommand(() => AddLeaseContractHandler());

        public ICommand EditContract => new RelayCommand(() => EditLeaseContractHandler());

        public ICommand PrintContract => new RelayCommand(() => SelectedContract.OpenAsFinalWordDocument());

        public ICommand CreateAct1ContractCommand
            => new RelayCommand(() => OpenDialogWithRefresh(() => _dialogService.OpenAddAct1ContractDialog(SelectedContract)));

        public ICommand CreateAct2ContractCommand
            => new RelayCommand(() => OpenDialogWithRefresh(() => _dialogService.OpenAddAct2ContractDialog(SelectedContract)));

        public ICommand CreateUtiltityServiceContractCommand
            => new RelayCommand(() => OpenDialogWithRefresh(() => _dialogService.OpenAddUtilityServiceContractDialog(SelectedContract)));

        public ICommand CreateClaimContractCommand
            => new RelayCommand(() => OpenDialogWithRefresh(() => _dialogService.OpenAddClaimContractDialog(SelectedContract)));

        public ICommand CreateChangeArendPriceContractCommand
            => new RelayCommand(() => OpenDialogWithRefresh(() => _dialogService.OpenAddChangeArendPriceContractDialog(SelectedContract)));

        public ICommand CreateTerminationContractCommand
            => new RelayCommand(() => OpenDialogWithRefresh(() => _dialogService.OpenAddTerminationContractDialog(SelectedContract)));

        public ICommand CreateCancelContractCommand
            => new RelayCommand(() => OpenDialogWithRefresh(() => _dialogService.OpenAddCancelContractDialog(SelectedContract)));

        #endregion Commands

        public LeaseContractViewModel(IObjectsService service)
        {
            _service = service;
            _dialogService = new NovogradDialogService(this, _service);
            LoadContacts();
        }

        #region Methods

        private void OpenDialogWithRefresh(Action openDialog)
        {
            openDialog();
            LoadContacts();
        }

        private void LoadContacts()
        {
            Contracts = new ObservableCollection<LeaseСontractDocument>(_service.GetLeaseContractsAsync().Result.Select(_service.GetLeaseСontractDocument));
            IsEditButtonEnabled = IsPrintButtonEnabled = false;
        }

        private void AddLeaseContractHandler()
        {
            _dialogService.OpenAddLeaseContractDialog();
            LoadContacts();
        }

        private void EditLeaseContractHandler()
        {
            _dialogService.OpenEditContractDialog(SelectedContract);
            LoadContacts();
        }

        #endregion Methods
    }
}