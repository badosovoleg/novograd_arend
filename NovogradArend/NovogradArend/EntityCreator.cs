﻿using NovogradArend.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovogradArend
{
    public partial class EntityCreator : Form
    {
        private Action<Entity> createEntity;

        public EntityCreator(Action<Entity> func)
        {
            InitializeComponent();
            name.TextChanged += CreateExampleRequisites;
            short_name.TextChanged += CreateExampleRequisites;
            inn.TextChanged += CreateExampleRequisites;
            ogrn.TextChanged += CreateExampleRequisites;
            organization_address.TextChanged += CreateExampleRequisites;
            rs.TextChanged += CreateExampleRequisites;
            bik.TextChanged += CreateExampleRequisites;
            email.TextChanged += CreateExampleRequisites;
            phone.TextChanged += CreateExampleRequisites;
            base_on.TextChanged += CreateExampleRequisites;
            owner_short_name.TextChanged += CreateExampleRequisites;
            represented.TextChanged += CreateExampleRequisites;
            agreement.TextChanged += CreateExampleRequisites;
            createEntity = func;
        }

        private string ShortRequisites { get; set; }

        private void addEntityButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();

            if (name.Text.Length == 0)
            {
                MessageBox.Show("Введите поле 'Название организации'");
            }
            else if (short_name.Text.Length == 0)
            {
                MessageBox.Show("Введите поле 'Название (короткое)'");
            }
            else if (inn.Text.Length == 0)
            {
                MessageBox.Show("Введите поле 'ИНН'");
            }
            else if (ogrn.Text.Length == 0)
            {
                MessageBox.Show("Введите поле 'ОГРН'");
            }
            else if (organization_address.Text.Length == 0)
            {
                MessageBox.Show("Введите поле 'Адрес'");
            }
            else if (rs.Text.Length == 0)
            {
                MessageBox.Show("Введите поле 'Расчетный счет'");
            }
            else if (bik.Text.Length == 0)
            {
                MessageBox.Show("Введите поле 'БИК'");
            }
            else if (email.Text.Length == 0)
            {
                MessageBox.Show("Введите поле 'E-mail'");
            }
            else if (phone.Text.Length == 0)
            {
                MessageBox.Show("Введите поле 'Телефон'");
            }
            else if (base_on.Text.Length == 0)
            {
                MessageBox.Show("Введите поле 'На основании'");
            }
            else if (owner_short_name.Text.Length == 0)
            {
                MessageBox.Show("Введите поле 'Инициалы'");
            }
            else if (represented.Text.Length == 0)
            {
                MessageBox.Show("Введите поле 'В лице'");
            }
            else if (agreement.Text.Length == 0)
            {
                MessageBox.Show("Введите поле 'На основании'");
            }

            createEntity(
                new Entity()
                {
                    Name = name.Text,
                    Agreement = agreement.Text,
                    Organization_address = organization_address.Text,
                    Email = email.Text,
                    Bik = bik.Text,
                    Base_On = base_on.Text,
                    Short_Name = short_name.Text,
                    Owner_Short_Name = owner_short_name.Text,
                    Ogrn = ogrn.Text,
                    Inn = inn.Text,
                    Phone = phone.Text,
                    Represented = represented.Text,
                    Requisites = requisites.Text,
                    Rs = rs.Text,
                    ShortRequisites = ShortRequisites,
                    Entity_Type_Id = 3
                });
        }

        private void CreateExampleRequisites(object sender, EventArgs e)
        {
            requisites.Text =
                (short_name.Text.Length > 0 ? $"{short_name.Text}" + Environment.NewLine : "") +
                (inn.Text.Length > 0 ? $"ИНН {inn.Text}" + Environment.NewLine : "") +
                (ogrn.Text.Length > 0 ? $"ОРГН {ogrn.Text}" + System.Environment.NewLine : "") +
                (organization_address.Text.Length > 0 ? $"{organization_address.Text}" + System.Environment.NewLine : "") +
                (rs.Text.Length > 0 ? $"р/с {rs.Text}" + Environment.NewLine : "") +
                (bik.Text.Length > 0 ? $"БИК {bik.Text}" + System.Environment.NewLine : "") +
                (email.Text.Length > 0 ? $"E-mail: {email.Text}" + System.Environment.NewLine : "") +
                (phone.Text.Length > 0 ? $"Тел. {phone.Text}" + System.Environment.NewLine : "") +
                (base_on.Text.Length > 0 ? $"{base_on.Text}" + System.Environment.NewLine : "") +
                (owner_short_name.Text.Length > 0 ? $"________________/{owner_short_name.Text}/" + System.Environment.NewLine : "") +
                "м.п";
            CreateExampleShortRequisites();
        }
        private void CreateExampleShortRequisites()
        {
            ShortRequisites =
                (short_name.Text.Length > 0 ? $"{short_name.Text}" + Environment.NewLine : "") +
                (base_on.Text.Length > 0 ? $"{base_on.Text}" + System.Environment.NewLine : "") +
                (owner_short_name.Text.Length > 0 ? $"________________/{owner_short_name.Text}/" + System.Environment.NewLine : "") +
                "м.п";
        }
    }
}
