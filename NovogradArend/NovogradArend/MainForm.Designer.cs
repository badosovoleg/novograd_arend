﻿namespace NovogradArend
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.buildingsComboBox = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.officesComboBox = new System.Windows.Forms.ComboBox();
            this.isLeaseContactCheckBox = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.beginContractDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.leaseContractGroupBox = new System.Windows.Forms.GroupBox();
            this.forPlacementTextBox = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.CountRenterDayComboBox = new System.Windows.Forms.ComboBox();
            this.CountLandlordDayComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.isGarantMailCheckBox = new System.Windows.Forms.CheckBox();
            this.act1Date = new System.Windows.Forms.DateTimePicker();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.garanteeDate = new System.Windows.Forms.DateTimePicker();
            this.garanteeNumber = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.leaseContractNumberTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.totalPriceLabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.priceForOfficeTextBox = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dateTimePicker5 = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.dateTimePicker6 = new System.Windows.Forms.DateTimePicker();
            this.label16 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.officeSquareLabel = new System.Windows.Forms.Label();
            this.officeLandlordLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbStatusText = new System.Windows.Forms.Label();
            this.renterComboBox = new System.Windows.Forms.ComboBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label27 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.юрЛицаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.офисыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addOfficeButton = new System.Windows.Forms.Button();
            this.removeSelectedOfficeButton = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.leaseContractGroupBox.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(14, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Выбор здания";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // buildingsComboBox
            // 
            this.buildingsComboBox.FormattingEnabled = true;
            this.buildingsComboBox.Location = new System.Drawing.Point(18, 64);
            this.buildingsComboBox.Name = "buildingsComboBox";
            this.buildingsComboBox.Size = new System.Drawing.Size(385, 21);
            this.buildingsComboBox.TabIndex = 1;
            this.buildingsComboBox.SelectedIndexChanged += new System.EventHandler(this.buildingsComboBox_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Location = new System.Drawing.Point(632, 689);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(143, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Загрузить документы";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(436, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Офис";
            // 
            // officesComboBox
            // 
            this.officesComboBox.FormattingEnabled = true;
            this.officesComboBox.Location = new System.Drawing.Point(426, 64);
            this.officesComboBox.Name = "officesComboBox";
            this.officesComboBox.Size = new System.Drawing.Size(121, 21);
            this.officesComboBox.TabIndex = 4;
            this.officesComboBox.SelectedIndexChanged += new System.EventHandler(this.officesComboBox_SelectedIndexChanged);
            // 
            // isLeaseContactCheckBox
            // 
            this.isLeaseContactCheckBox.AutoSize = true;
            this.isLeaseContactCheckBox.Location = new System.Drawing.Point(6, 17);
            this.isLeaseContactCheckBox.Name = "isLeaseContactCheckBox";
            this.isLeaseContactCheckBox.Size = new System.Drawing.Size(63, 17);
            this.isLeaseContactCheckBox.TabIndex = 5;
            this.isLeaseContactCheckBox.Text = "Аренда";
            this.isLeaseContactCheckBox.UseVisualStyleBackColor = true;
            this.isLeaseContactCheckBox.CheckedChanged += new System.EventHandler(this.isLeaseContactCheckBox_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(6, 17);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(125, 17);
            this.checkBox2.TabIndex = 6;
            this.checkBox2.Text = "Тех. Обслуживание";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(6, 17);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(135, 17);
            this.checkBox3.TabIndex = 7;
            this.checkBox3.Text = "Акт приема-передачи";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(6, 17);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(111, 17);
            this.checkBox4.TabIndex = 8;
            this.checkBox4.Text = "Доп соглашение";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(5, 17);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(94, 17);
            this.checkBox5.TabIndex = 9;
            this.checkBox5.Text = "Расторжение";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // beginContractDateTimePicker
            // 
            this.beginContractDateTimePicker.Location = new System.Drawing.Point(92, 63);
            this.beginContractDateTimePicker.Name = "beginContractDateTimePicker";
            this.beginContractDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.beginContractDateTimePicker.TabIndex = 10;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.dateTimePicker4);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.checkBox5);
            this.groupBox1.Location = new System.Drawing.Point(632, 518);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(149, 165);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 110);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(71, 13);
            this.label14.TabIndex = 24;
            this.label14.Text = "Дата начала";
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.Location = new System.Drawing.Point(3, 130);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.Size = new System.Drawing.Size(131, 20);
            this.dateTimePicker4.TabIndex = 23;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label12.Location = new System.Drawing.Point(1, 80);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(143, 26);
            this.label12.TabIndex = 13;
            this.label12.Text = "№ КР-ПР-ДЦ/2э/07 \r\nот «30» апреля  2018 года.";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(1, 57);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 12;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(2, 37);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(97, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "Договору аренды";
            // 
            // leaseContractGroupBox
            // 
            this.leaseContractGroupBox.Controls.Add(this.forPlacementTextBox);
            this.leaseContractGroupBox.Controls.Add(this.label26);
            this.leaseContractGroupBox.Controls.Add(this.CountRenterDayComboBox);
            this.leaseContractGroupBox.Controls.Add(this.CountLandlordDayComboBox);
            this.leaseContractGroupBox.Controls.Add(this.label4);
            this.leaseContractGroupBox.Controls.Add(this.isGarantMailCheckBox);
            this.leaseContractGroupBox.Controls.Add(this.act1Date);
            this.leaseContractGroupBox.Controls.Add(this.label25);
            this.leaseContractGroupBox.Controls.Add(this.label24);
            this.leaseContractGroupBox.Controls.Add(this.label23);
            this.leaseContractGroupBox.Controls.Add(this.label22);
            this.leaseContractGroupBox.Controls.Add(this.label21);
            this.leaseContractGroupBox.Controls.Add(this.garanteeDate);
            this.leaseContractGroupBox.Controls.Add(this.garanteeNumber);
            this.leaseContractGroupBox.Controls.Add(this.label8);
            this.leaseContractGroupBox.Controls.Add(this.leaseContractNumberTextBox);
            this.leaseContractGroupBox.Controls.Add(this.label3);
            this.leaseContractGroupBox.Controls.Add(this.totalPriceLabel);
            this.leaseContractGroupBox.Controls.Add(this.label7);
            this.leaseContractGroupBox.Controls.Add(this.priceForOfficeTextBox);
            this.leaseContractGroupBox.Controls.Add(this.isLeaseContactCheckBox);
            this.leaseContractGroupBox.Controls.Add(this.beginContractDateTimePicker);
            this.leaseContractGroupBox.Location = new System.Drawing.Point(12, 183);
            this.leaseContractGroupBox.Name = "leaseContractGroupBox";
            this.leaseContractGroupBox.Size = new System.Drawing.Size(818, 329);
            this.leaseContractGroupBox.TabIndex = 13;
            this.leaseContractGroupBox.TabStop = false;
            // 
            // forPlacementTextBox
            // 
            this.forPlacementTextBox.Location = new System.Drawing.Point(108, 216);
            this.forPlacementTextBox.Name = "forPlacementTextBox";
            this.forPlacementTextBox.Size = new System.Drawing.Size(184, 20);
            this.forPlacementTextBox.TabIndex = 39;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 216);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(96, 13);
            this.label26.TabIndex = 38;
            this.label26.Text = "Для размещения";
            // 
            // CountRenterDayComboBox
            // 
            this.CountRenterDayComboBox.FormattingEnabled = true;
            this.CountRenterDayComboBox.Items.AddRange(new object[] {
            "15",
            "30",
            "45",
            "60"});
            this.CountRenterDayComboBox.Location = new System.Drawing.Point(208, 152);
            this.CountRenterDayComboBox.Name = "CountRenterDayComboBox";
            this.CountRenterDayComboBox.Size = new System.Drawing.Size(135, 21);
            this.CountRenterDayComboBox.TabIndex = 37;
            // 
            // CountLandlordDayComboBox
            // 
            this.CountLandlordDayComboBox.FormattingEnabled = true;
            this.CountLandlordDayComboBox.Items.AddRange(new object[] {
            "15",
            "30",
            "45",
            "60"});
            this.CountLandlordDayComboBox.Location = new System.Drawing.Point(208, 123);
            this.CountLandlordDayComboBox.Name = "CountLandlordDayComboBox";
            this.CountLandlordDayComboBox.Size = new System.Drawing.Size(135, 21);
            this.CountLandlordDayComboBox.TabIndex = 36;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 30;
            this.label4.Text = "Дата начала";
            // 
            // isGarantMailCheckBox
            // 
            this.isGarantMailCheckBox.AutoSize = true;
            this.isGarantMailCheckBox.Location = new System.Drawing.Point(9, 244);
            this.isGarantMailCheckBox.Name = "isGarantMailCheckBox";
            this.isGarantMailCheckBox.Size = new System.Drawing.Size(132, 17);
            this.isGarantMailCheckBox.TabIndex = 29;
            this.isGarantMailCheckBox.Text = "Гарантийное письмо";
            this.isGarantMailCheckBox.UseVisualStyleBackColor = true;
            // 
            // act1Date
            // 
            this.act1Date.Location = new System.Drawing.Point(228, 183);
            this.act1Date.Name = "act1Date";
            this.act1Date.Size = new System.Drawing.Size(200, 20);
            this.act1Date.TabIndex = 26;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 189);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(216, 13);
            this.label25.TabIndex = 24;
            this.label25.Text = "Срок подписания АКТа приема-передачи";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(37, 152);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(145, 13);
            this.label24.TabIndex = 22;
            this.label24.Text = "По инициативе арендатора";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(37, 128);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(157, 13);
            this.label23.TabIndex = 21;
            this.label23.Text = "По инициативе арендодателя";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 101);
            this.label22.Name = "label22";
            this.label22.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label22.Size = new System.Drawing.Size(255, 13);
            this.label22.TabIndex = 19;
            this.label22.Text = "Кол-во дней до прекращения действия договора";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(25, 298);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(91, 13);
            this.label21.TabIndex = 18;
            this.label21.Text = "Номер договора";
            // 
            // garanteeDate
            // 
            this.garanteeDate.Location = new System.Drawing.Point(228, 295);
            this.garanteeDate.Name = "garanteeDate";
            this.garanteeDate.Size = new System.Drawing.Size(200, 20);
            this.garanteeDate.TabIndex = 17;
            // 
            // garanteeNumber
            // 
            this.garanteeNumber.Location = new System.Drawing.Point(122, 295);
            this.garanteeNumber.Name = "garanteeNumber";
            this.garanteeNumber.Size = new System.Drawing.Size(100, 20);
            this.garanteeNumber.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 268);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(377, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Внесение сведений об адресе в учередительные документы арендатора";
            // 
            // leaseContractNumberTextBox
            // 
            this.leaseContractNumberTextBox.Location = new System.Drawing.Point(86, 37);
            this.leaseContractNumberTextBox.Name = "leaseContractNumberTextBox";
            this.leaseContractNumberTextBox.Size = new System.Drawing.Size(100, 20);
            this.leaseContractNumberTextBox.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "№ Догвора";
            // 
            // totalPriceLabel
            // 
            this.totalPriceLabel.AutoSize = true;
            this.totalPriceLabel.Location = new System.Drawing.Point(479, 109);
            this.totalPriceLabel.Name = "totalPriceLabel";
            this.totalPriceLabel.Size = new System.Drawing.Size(47, 13);
            this.totalPriceLabel.TabIndex = 8;
            this.totalPriceLabel.Text = "Сумма: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(476, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Цена за м2";
            // 
            // priceForOfficeTextBox
            // 
            this.priceForOfficeTextBox.Location = new System.Drawing.Point(478, 82);
            this.priceForOfficeTextBox.Name = "priceForOfficeTextBox";
            this.priceForOfficeTextBox.Size = new System.Drawing.Size(100, 20);
            this.priceForOfficeTextBox.TabIndex = 6;
            this.priceForOfficeTextBox.TextChanged += new System.EventHandler(this.priceForOfficeTextBox_TextChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.dateTimePicker3);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.textBox6);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.checkBox2);
            this.groupBox3.Location = new System.Drawing.Point(185, 518);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(143, 155);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 110);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "Дата начала";
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Location = new System.Drawing.Point(5, 130);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(131, 20);
            this.dateTimePicker3.TabIndex = 28;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label17.Location = new System.Drawing.Point(3, 80);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(143, 26);
            this.label17.TabIndex = 27;
            this.label17.Text = "№ КР-ПР-ДЦ/2э/07 \r\nот «30» апреля  2018 года.";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(3, 57);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(100, 20);
            this.textBox6.TabIndex = 26;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(4, 37);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(97, 13);
            this.label18.TabIndex = 25;
            this.label18.Text = "Договору аренды";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.dateTimePicker5);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.textBox3);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.checkBox3);
            this.groupBox4.Location = new System.Drawing.Point(334, 518);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(143, 155);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 110);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 29;
            this.label9.Text = "Дата начала";
            // 
            // dateTimePicker5
            // 
            this.dateTimePicker5.Location = new System.Drawing.Point(3, 130);
            this.dateTimePicker5.Name = "dateTimePicker5";
            this.dateTimePicker5.Size = new System.Drawing.Size(131, 20);
            this.dateTimePicker5.TabIndex = 28;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label10.Location = new System.Drawing.Point(1, 80);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(143, 26);
            this.label10.TabIndex = 27;
            this.label10.Text = "№ КР-ПР-ДЦ/2э/07 \r\nот «30» апреля  2018 года.";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(1, 57);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 26;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(2, 37);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(97, 13);
            this.label19.TabIndex = 25;
            this.label19.Text = "Договору аренды";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.dateTimePicker6);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this.textBox5);
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this.checkBox4);
            this.groupBox5.Location = new System.Drawing.Point(483, 518);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(143, 155);
            this.groupBox5.TabIndex = 14;
            this.groupBox5.TabStop = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(5, 110);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 13);
            this.label15.TabIndex = 29;
            this.label15.Text = "Дата начала";
            // 
            // dateTimePicker6
            // 
            this.dateTimePicker6.Location = new System.Drawing.Point(2, 130);
            this.dateTimePicker6.Name = "dateTimePicker6";
            this.dateTimePicker6.Size = new System.Drawing.Size(131, 20);
            this.dateTimePicker6.TabIndex = 28;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label16.Location = new System.Drawing.Point(0, 80);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(143, 26);
            this.label16.TabIndex = 27;
            this.label16.Text = "№ КР-ПР-ДЦ/2э/07 \r\nот «30» апреля  2018 года.";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(0, 57);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 20);
            this.textBox5.TabIndex = 26;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(1, 37);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(97, 13);
            this.label20.TabIndex = 25;
            this.label20.Text = "Договору аренды";
            // 
            // officeSquareLabel
            // 
            this.officeSquareLabel.AutoSize = true;
            this.officeSquareLabel.Location = new System.Drawing.Point(18, 97);
            this.officeSquareLabel.Name = "officeSquareLabel";
            this.officeSquareLabel.Size = new System.Drawing.Size(60, 13);
            this.officeSquareLabel.TabIndex = 15;
            this.officeSquareLabel.Text = "Площадь: ";
            // 
            // officeLandlordLabel
            // 
            this.officeLandlordLabel.AutoSize = true;
            this.officeLandlordLabel.Location = new System.Drawing.Point(18, 117);
            this.officeLandlordLabel.Name = "officeLandlordLabel";
            this.officeLandlordLabel.Size = new System.Drawing.Size(85, 13);
            this.officeLandlordLabel.TabIndex = 16;
            this.officeLandlordLabel.Text = "Арендодатель: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 145);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Арендатор: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 598);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Status:";
            // 
            // lbStatusText
            // 
            this.lbStatusText.AutoSize = true;
            this.lbStatusText.Location = new System.Drawing.Point(93, 599);
            this.lbStatusText.Name = "lbStatusText";
            this.lbStatusText.Size = new System.Drawing.Size(25, 13);
            this.lbStatusText.TabIndex = 20;
            this.lbStatusText.Text = "Null";
            // 
            // renterComboBox
            // 
            this.renterComboBox.FormattingEnabled = true;
            this.renterComboBox.Location = new System.Drawing.Point(91, 142);
            this.renterComboBox.Name = "renterComboBox";
            this.renterComboBox.Size = new System.Drawing.Size(312, 21);
            this.renterComboBox.TabIndex = 21;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(582, 57);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(248, 82);
            this.listBox1.TabIndex = 29;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(579, 41);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(130, 13);
            this.label27.TabIndex = 30;
            this.label27.Text = "Снимаемые помещения";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.юрЛицаToolStripMenuItem,
            this.офисыToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1022, 24);
            this.menuStrip1.TabIndex = 31;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // юрЛицаToolStripMenuItem
            // 
            this.юрЛицаToolStripMenuItem.Name = "юрЛицаToolStripMenuItem";
            this.юрЛицаToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.юрЛицаToolStripMenuItem.Text = "Юр. лица";
            this.юрЛицаToolStripMenuItem.Click += new System.EventHandler(this.юрЛицаToolStripMenuItem_Click);
            // 
            // офисыToolStripMenuItem
            // 
            this.офисыToolStripMenuItem.Name = "офисыToolStripMenuItem";
            this.офисыToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.офисыToolStripMenuItem.Text = "Офисы";
            // 
            // addOfficeButton
            // 
            this.addOfficeButton.Location = new System.Drawing.Point(859, 61);
            this.addOfficeButton.Name = "addOfficeButton";
            this.addOfficeButton.Size = new System.Drawing.Size(75, 23);
            this.addOfficeButton.TabIndex = 32;
            this.addOfficeButton.Text = "Добавить";
            this.addOfficeButton.UseVisualStyleBackColor = true;
            this.addOfficeButton.Click += new System.EventHandler(this.addOfficeButton_Click);
            // 
            // removeSelectedOfficeButton
            // 
            this.removeSelectedOfficeButton.Location = new System.Drawing.Point(859, 90);
            this.removeSelectedOfficeButton.Name = "removeSelectedOfficeButton";
            this.removeSelectedOfficeButton.Size = new System.Drawing.Size(75, 23);
            this.removeSelectedOfficeButton.TabIndex = 33;
            this.removeSelectedOfficeButton.Text = "Удалить";
            this.removeSelectedOfficeButton.UseVisualStyleBackColor = true;
            this.removeSelectedOfficeButton.Click += new System.EventHandler(this.removeSelectedOfficeButton_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(426, 140);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 35;
            this.button4.Text = "Добавить";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1022, 732);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.removeSelectedOfficeButton);
            this.Controls.Add(this.addOfficeButton);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.renterComboBox);
            this.Controls.Add(this.lbStatusText);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.officeLandlordLabel);
            this.Controls.Add(this.officeSquareLabel);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.leaseContractGroupBox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.officesComboBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buildingsComboBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "Новоград аренда";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.leaseContractGroupBox.ResumeLayout(false);
            this.leaseContractGroupBox.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox buildingsComboBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox officesComboBox;
        private System.Windows.Forms.CheckBox isLeaseContactCheckBox;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.DateTimePicker beginContractDateTimePicker;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox leaseContractGroupBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label officeSquareLabel;
        private System.Windows.Forms.Label officeLandlordLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbStatusText;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox priceForOfficeTextBox;
        private System.Windows.Forms.Label totalPriceLabel;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dateTimePicker5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DateTimePicker dateTimePicker6;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox renterComboBox;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.DateTimePicker act1Date;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DateTimePicker garanteeDate;
        private System.Windows.Forms.TextBox garanteeNumber;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox leaseContractNumberTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox isGarantMailCheckBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Button addOfficeButton;
        private System.Windows.Forms.Button removeSelectedOfficeButton;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox CountLandlordDayComboBox;
        private System.Windows.Forms.ComboBox CountRenterDayComboBox;
        private System.Windows.Forms.TextBox forPlacementTextBox;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ToolStripMenuItem юрЛицаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem офисыToolStripMenuItem;
    }
}

