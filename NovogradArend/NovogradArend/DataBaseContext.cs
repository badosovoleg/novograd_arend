﻿using Microsoft.EntityFrameworkCore;
using NovogradArend.Models;
using NovogradArend.Models.Configurations;

namespace NovogradArend
{
    public class SQLiteDatabaseContext : DbContext, IDatabaseContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data Source=..\..\novograd_arend.db");
        }

        public DbSet<Entity> Entities { get; set; }

        public DbSet<EntityType> EntityTypes { get; set; }

        public DbSet<Building> Buildings { get; set; }

        public DbSet<Contract> Contracts { get; set; }

        public DbSet<ContractType> ContractTypes { get; set; }

        public DbSet<ActiveContract> ActiveContracts { get; set; }

        public DbSet<Office> Offices { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new EntityConfiguration());
            modelBuilder.ApplyConfiguration(new EntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new BuildingConfiguration());
            modelBuilder.ApplyConfiguration(new ContractConfiguration());
            modelBuilder.ApplyConfiguration(new ContractTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ActiveContractConfiguration());
            modelBuilder.ApplyConfiguration(new OfficeConfiguration());
        }
    }

    public interface IDatabaseContext
    {

    }

    public static class UserRepository
    {
        private static IDatabaseContext databaseContext; 

        static UserRepository()
        {

        }
    }
}