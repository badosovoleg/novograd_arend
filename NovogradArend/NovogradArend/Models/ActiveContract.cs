﻿namespace NovogradArend.Models
{
    /// <summary>
    /// Модель данных заключенного договора
    /// </summary>
    public class ActiveContract
    {
        /// <summary>
        /// Ид в бд
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Ид помещения из бд
        /// </summary>
        public int Office_Id { get; set; }

        /// <summary>
        /// Ид договора из бд
        /// </summary>
        public int Contract_Id { get; set; }

        /// <summary>
        /// Итоговая цена
        /// </summary>
        public int? Price { get; set; }
        
        /// <summary>
        /// Для чего предоставляется помещение
        /// Помещение передается Арендатору для размещения
        /// </summary>
        public string ForPlacement { get; set; }

        /// <summary>
        /// Доп. описание
        /// </summary>
        public string Description { get; set; }
    }
}
