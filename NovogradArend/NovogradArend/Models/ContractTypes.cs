﻿namespace NovogradArend.Models
{
    public sealed class ContractTypes
    {
        private readonly string name;
        private readonly int value;

        public int Id => value;

        /// <summary>
        /// "Договор аренды"
        /// </summary>
        public static ContractTypes ARENDCONTRACT => new ContractTypes(1, "Договор аренды");

        /// <summary>
        /// "АКТ приема-передачи №1"
        /// </summary>
        public static readonly ContractTypes ACT1 = new ContractTypes(2, "АКТ приема-передачи №1");

        /// <summary>
        /// "АКТ приема-передачи №2"
        /// </summary>
        public static readonly ContractTypes ACT2 = new ContractTypes(2, "АКТ приема-передачи №2");

        /// <summary>
        /// "Договор изменения арендной платы"
        /// </summary>
        public static readonly ContractTypes CHANGEARENDPRICE = new ContractTypes(2, "Договор изменения арендной платы");

        /// <summary>
        /// "Договор коммунальное обслуживание"
        /// </summary>
        public static readonly ContractTypes UTILITYSERVICE = new ContractTypes(2, "Договор коммунальное обслуживание");

        /// <summary>
        /// "Документ отказа"
        /// </summary>
        public static readonly ContractTypes CANCELCONTRACT = new ContractTypes(2, "Документ отказа");

        /// <summary>
        /// "Краткое содержание"
        /// </summary>
        public static readonly ContractTypes SHORTCONTENT = new ContractTypes(2, "Краткое содержание");

        /// <summary>
        /// "Претензия"
        /// </summary>
        public static readonly ContractTypes CLAIM = new ContractTypes(2, "Претензия");

        /// <summary>
        /// "Соглашение о расторжении"
        /// </summary>
        public static readonly ContractTypes TERMINATION = new ContractTypes(2, "Соглашение о расторжении");

        private ContractTypes(int value, string name)
        {
            this.name = name;
            this.value = value;
        }

        public override string ToString()
        {
            return name;
        }

        public static ContractTypes Parse(string value)
        {
            if (value == ARENDCONTRACT.name) return ARENDCONTRACT;
            else if (value == ACT1.name) return ACT1;
            else if (value == ACT2.name) return ACT2;
            else if (value == CHANGEARENDPRICE.name) return CHANGEARENDPRICE;
            else if (value == UTILITYSERVICE.name) return UTILITYSERVICE;
            else if (value == CANCELCONTRACT.name) return CANCELCONTRACT;
            else if (value == SHORTCONTENT.name) return SHORTCONTENT;
            else if (value == CLAIM.name) return CLAIM;
            else if (value == TERMINATION.name) return TERMINATION;
            return null;
        }
    }
}