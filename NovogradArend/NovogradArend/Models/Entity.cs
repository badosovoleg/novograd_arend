﻿namespace NovogradArend.Models
{
    /// <summary>
    /// Модель данных юр. лица
    /// </summary>
    public class Entity
    {
        /// <summary>
        /// Ид в бд
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// "В лице <see cref="Represented"/>"
        /// </summary>
        public string Represented { get; set; }

        /// <summary>
        /// "Действующего на основании <see cref="Agreement"/>"
        /// </summary>
        public string Agreement { get; set; }

        /// <summary>
        /// Ид типа юр. лица из бд
        /// </summary>
        public int Entity_Type_Id { get; set; }

        /// <summary>
        /// Реквизиты
        /// </summary>
        public string Requisites { get; set; }

        public string ShortRequisites { get; set; }

        public string Inn { get; set; }

        public string Ogrn { get; set; }

        public string Rs { get; set; }

        public string Bik { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Short_Name { get; set; }

        public string Owner_Short_Name { get; set; }

        public string Base_On { get; set; }

        public string Organization_address { get; set; }
    }
}
