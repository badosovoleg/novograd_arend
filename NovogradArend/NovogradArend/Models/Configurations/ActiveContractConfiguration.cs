﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace NovogradArend.Models.Configurations
{
    class ActiveContractConfiguration : IEntityTypeConfiguration<ActiveContract>
    {
        public void Configure(EntityTypeBuilder<ActiveContract> builder)
        {
            builder.ToTable("ActiveContracts");
        }
    }
}
