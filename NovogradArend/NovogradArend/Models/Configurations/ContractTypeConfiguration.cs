﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace NovogradArend.Models.Configurations
{
    class ContractTypeConfiguration : IEntityTypeConfiguration<ContractType>
    {
        public void Configure(EntityTypeBuilder<ContractType> builder)
        {
            builder.ToTable("ContractTypes");

            builder.Property(e => e.Type).HasConversion(e => e.ToString(), e => ContractTypes.Parse(e));
        }
    }
}
