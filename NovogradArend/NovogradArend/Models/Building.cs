﻿namespace NovogradArend.Models
{
    /// <summary>
    /// Модель данных здания
    /// </summary>
    public class Building
    {
        /// <summary>
        /// Ид в бд
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        public string Address { get; set; }

        public string Code { get; set; }
    }
}
