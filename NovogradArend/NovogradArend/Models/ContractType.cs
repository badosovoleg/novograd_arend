﻿namespace NovogradArend.Models
{
    /// <summary>
    /// Модель типа договора
    /// </summary>
    public class ContractType
    {
        /// <summary>
        /// Ид в бд
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Тип договора <see cref="ContractTypes"/>
        /// </summary>
        public ContractTypes Type { get; set; }

        public string Code { get; set; }
    }

}