﻿namespace NovogradArend.Models
{
    /// <summary>
    /// Модель типа юр. лица
    /// </summary>
    public class EntityType
    {
        /// <summary>
        /// Ид в бд
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Тип юр. лица <see cref="EntityTypeEnum"/>
        /// </summary>
        public string Type { get; set; }
    }

    public enum EntityTypeEnum
    {
        Арендодатель,
        Арендатор
    };
}
