﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NovogradArend.Models
{
    /// <summary>
    /// Модель данных офиса
    /// </summary>
    public class Office
    {
        /// <summary>
        /// Ид в бд
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Ид здания из бд
        /// </summary>
        public int Building_Id { get; set; }

        /// <summary>
        /// Номер помещения
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Этаж
        /// </summary>
        public int Floor { get; set; }

        /// <summary>
        /// Номер помещения на поэтажном плане
        /// </summary>
        public int? Number_On_Floor_Plan { get; set; }

        /// <summary>
        /// Право собственности
        /// </summary>
        public string Ownership { get; set; }

        /// <summary>
        /// Ид собственника из бд
        /// </summary>
        public int Entity_Id { get; set; }

        /// <summary>
        /// Площадь в кв. м.
        /// </summary>
        public int Square { get; set; }

        /// <summary>
        /// Цена за кв. м.
        /// </summary>
        public int? Price { get; set; }

        /// <summary>
        /// Изображение-схема помещения
        /// </summary>
        [NotMapped]
        public string ImagePath { get; set; }
    }
}
