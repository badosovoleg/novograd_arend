﻿namespace NovogradArend.Models
{
    /// <summary>
    /// Модель данных договора
    /// </summary>
    public class Contract
    {
        /// <summary>
        /// Ид в бд
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Номер договора
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Дата начала
        /// </summary>
        public string Begin_Date { get; set; }

        /// <summary>
        /// Дата окончания
        /// </summary>
        public string End_Date { get; set; }

        /// <summary>
        /// Ид арендодателя из бд
        /// </summary>
        public int Landlord_Id { get; set; }

        /// <summary>
        /// Ид арендатора из бд
        /// </summary>
        public int Renter_Id { get; set; }

        /// <summary>
        /// Ид типа договора из бд
        /// </summary>
        public int Contract_Type_Id { get; set; }
    }
}
