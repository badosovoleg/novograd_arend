﻿namespace NovogradArend
{
    public class ReplacebelTextByBookmark : IReplacableBookmark
    {
        public string Bookmark { get; set; }
        public string Text;

        public ReplacebelTextByBookmark(string bookmark, string text)
        {
            Bookmark = bookmark;
            Text = text;
        }
    }
}
