﻿using System;

namespace NovogradArend
{
    public static class DateTimeExtensions
    {
        public static string ToFormatDate(this DateTime date)
        {
            var result = date.ToString("D");
            result = result.TrimStart('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' ');

            //var index = 0;
            //for (index = 0; result[index] != ' '; index++) ;
            //result = result.Substring(index);

            var newResult = $"\u00AB{date.Day}\u00BB {result}";

            return newResult;
        }
    }
}
