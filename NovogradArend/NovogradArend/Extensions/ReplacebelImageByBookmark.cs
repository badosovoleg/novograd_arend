﻿namespace NovogradArend
{
    public class ReplacebelImageByBookmark : IReplacableBookmark
    {
        public string Bookmark { get; set; }
        public string ImagePath;

        public ReplacebelImageByBookmark(string bookmark, string imagePath)
        {
            Bookmark = bookmark;
            ImagePath = imagePath;
        }
    }
}
