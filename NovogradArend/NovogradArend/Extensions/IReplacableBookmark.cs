﻿namespace NovogradArend
{
    public interface IReplacableBookmark
    {
        string Bookmark { get; set; }
    }
}
