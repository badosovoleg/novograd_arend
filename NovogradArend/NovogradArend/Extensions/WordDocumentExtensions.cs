﻿using Microsoft.Office.Interop.Word;
using System.Collections.Generic;

namespace NovogradArend
{
    public static class WordDocumentExtensions
    {
        private static void SetTextByBookmarkName(this Document document, string bookmarkName, string text) 
            => document.Bookmarks.get_Item(bookmarkName).Range.Text = text;

        private static void SetImageByBookmarkName(this Document document, string bookmarkName, string imagePath) 
            => document.Bookmarks.get_Item(bookmarkName).Range.InlineShapes.AddPicture(imagePath);

        public static void SetTextByBookmarkName(this Document document, IEnumerable<IReplacableBookmark> items)
        {
            foreach (var item in items)
            {
                if (item is ReplacebelImageByBookmark)
                    document.SetImageByBookmarkName(item.Bookmark, ((ReplacebelImageByBookmark)item).ImagePath);
                if (item is ReplacebelTextByBookmark)
                    document.SetTextByBookmarkName(item.Bookmark, ((ReplacebelTextByBookmark)item).Text);
            }
        }
    }
}
