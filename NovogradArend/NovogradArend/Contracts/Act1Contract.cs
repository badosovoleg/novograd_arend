﻿using NovogradArend.Models;
using System;
using System.Collections.Generic;

namespace NovogradArend.Contracts
{
    /// <summary>
    /// АКТ №1 приема-передачи нежилого помещения к Договору аренды
    /// </summary>
    internal class Act1Contract : BaseContract
    {
        public string Number { get; set; }

        public DateTime BeginDate { get; set; } = DateTime.MinValue;

        public DateTime ActBeginDate { get; set; }

        public Entity Landlord { get; set; }

        public Entity Renter { get; set; }

        public string OfficeFloor { get; set; }

        public string TotalOfficeSquare { get; set; }

        public Building Building { get; set; }

        public string ForPlacement { get; set; }

        private string _nameOfDOCXTemplate = null;
        protected override string NameOfDOCXTemplate
        {
            get
            {
                if (_nameOfDOCXTemplate == null) _nameOfDOCXTemplate = "Act1ContractTemplate";
                return _nameOfDOCXTemplate;
            }
        }

        public override ContractTypes ContractType => ContractTypes.ACT1;

        public override IEnumerable<IReplacableBookmark> GetParametresForReplace()
        {
            var beginDate = BeginDate.ToFormatDate();

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractNumberBookmarkCaption, Number);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractBeginDateBookmarkCaption, beginDate);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.Act1LeaseContractNumberBookmarkCaption, Number);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.Act1LeaseContractBeginDateBookmarkCaption, beginDate);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractAct1BeginDateBookmarkCaption, ActBeginDate.ToFormatDate());

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractLandlordNameBookmarkCaption, Landlord.Name);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractLandlordRepresentedBookmarkCaption, Landlord.Represented);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractLandlordAgreementBookmarkCaption, Landlord.Agreement);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractRenterNameBookmarkCaption, Renter.Name);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractRenterRepresentedBookmarkCaption, Renter.Represented);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractRenterAgreementBookmarkCaption, Renter.Agreement);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractBuildingAddressBookmarkCaption, Building.Address);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractOfficeFloorBookmarkCaption, OfficeFloor);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractOfficeSquareBookmarkCaption, TotalOfficeSquare);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractOfficeForPlacementBookmarkCaption, ForPlacement);

            var landlordRequisites = new RequisitesBuilder(Landlord);
            var renterRequisites = new RequisitesBuilder(Renter);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractLandlordRequisitesBookmarkCaption, landlordRequisites.HeaderOfRequisites);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractLandlordRequisitesEndBookmarkCaption, landlordRequisites.EndOfRequisites);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractRenterRequisitesBookmarkCaption, renterRequisites.HeaderOfRequisites);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractRenterRequisitesEndBookmarkCaption, renterRequisites.EndOfRequisites);
        }
    }
}