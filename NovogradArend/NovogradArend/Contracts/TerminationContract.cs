﻿using NovogradArend.Models;
using System;
using System.Collections.Generic;

namespace NovogradArend.Contracts
{
    /// <summary>
    /// Соглашение о расторжении
    /// </summary>
    internal class TerminationContract : BaseContract
    {
        public override ContractTypes ContractType => ContractTypes.TERMINATION;

        private string _nameOfDOCXTemplate = null;
        protected override string NameOfDOCXTemplate
        {
            get
            {
                if (_nameOfDOCXTemplate == null) _nameOfDOCXTemplate = "TerminationContractTemplate";
                return _nameOfDOCXTemplate;
            }
        }

        public override IEnumerable<IReplacableBookmark> GetParametresForReplace()
        {
            throw new NotImplementedException();
        }
    }
}