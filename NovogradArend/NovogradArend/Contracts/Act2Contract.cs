﻿using NovogradArend.Models;
using System;
using System.Collections.Generic;

namespace NovogradArend.Contracts
{
    /// <summary>
    /// АКТ №2 приема-передачи нежилого помещения к Договору аренды
    /// </summary>
    internal class Act2Contract : BaseContract
    {
        public override ContractTypes ContractType => ContractTypes.ACT2;

        private string _nameOfDOCXTemplate = null;
        protected override string NameOfDOCXTemplate
        {
            get
            {
                if (_nameOfDOCXTemplate == null) _nameOfDOCXTemplate = "Act2ContractTemplate";
                return _nameOfDOCXTemplate;
            }
        }

        public string Number { get; set; }

        public DateTime BeginDate { get; set; } = DateTime.MinValue;

        public DateTime ActBeginDate { get; set; }

        public Entity Landlord { get; set; }

        public Entity Renter { get; set; }

        public DateTime LeaseContractEndDate { get; set; }

        public override IEnumerable<IReplacableBookmark> GetParametresForReplace()
        {
            var beginDate = BeginDate.ToFormatDate();

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractNumberBookmarkCaption, Number);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractBeginDateBookmarkCaption, beginDate);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.Act1LeaseContractNumberBookmarkCaption, Number);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.Act1LeaseContractBeginDateBookmarkCaption, beginDate);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractAct1BeginDateBookmarkCaption, ActBeginDate.ToFormatDate());

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractLandlordNameBookmarkCaption, Landlord.Name);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractLandlordRepresentedBookmarkCaption, Landlord.Represented);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractLandlordAgreementBookmarkCaption, Landlord.Agreement);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractRenterNameBookmarkCaption, Renter.Name);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractRenterRepresentedBookmarkCaption, Renter.Represented);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractRenterAgreementBookmarkCaption, Renter.Agreement);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.Act2LeaseContractEndDateBookmarkCaption, LeaseContractEndDate.ToShortDateString());

            var landlordRequisites = new RequisitesBuilder(Landlord);
            var renterRequisites = new RequisitesBuilder(Renter);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractLandlordRequisitesBookmarkCaption, landlordRequisites.HeaderOfRequisites);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractLandlordRequisitesEndBookmarkCaption, landlordRequisites.EndOfRequisites);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractRenterRequisitesBookmarkCaption, renterRequisites.HeaderOfRequisites);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractRenterRequisitesEndBookmarkCaption, renterRequisites.EndOfRequisites);

        }
    }
}