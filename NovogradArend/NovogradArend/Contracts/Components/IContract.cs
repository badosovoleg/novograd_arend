﻿namespace NovogradArend.Contracts
{
    public interface IContract
    {
        void SaveAsFinalWordDocument(string path);
    }
}
