﻿namespace NovogradArend.Contracts
{
    internal sealed class ContractBooknmarksConstant
    {
        internal const string LeaseContractNumberBookmarkCaption = "number";

        internal const string LeaseContractCountLandlordDayBookmarkCaption = "CountLandlordDay";

        internal const string LeaseContractCountLandlordDayBookmarkCaption2 = "CountLandlordDay2";

        internal const string LeaseContractCountRenterDayBookmarkCaption = "CountRenterDay";

        internal const string LeaseContractCountRenterDay2BookmarkCaption = "CountRenterDay2";

        internal const string LeaseContractBeginDateBookmarkCaption = "beginDate";

        internal const string LeaseContractLandlordNameBookmarkCaption = "landlordName";

        internal const string LeaseContractLandlordRepresentedBookmarkCaption = "landlordRepresented";

        internal const string LeaseContractLandlordAgreementBookmarkCaption = "landlordAgreement";

        internal const string LeaseContractRenterNameBookmarkCaption = "renterName";

        internal const string LeaseContractRenterRepresentedBookmarkCaption = "renterRepresented";

        internal const string LeaseContractRenterAgreementBookmarkCaption = "renterAgreement";

        internal const string LeaseContractBuildingAddressBookmarkCaption = "buildingAddress";

        internal const string LeaseContractOfficeFloorBookmarkCaption = "officeFloor";

        internal const string LeaseContractOfficeNumberOnFloorPlanBookmarkCaption = "officeNumberOnFloorPlan";

        internal const string LeaseContractOfficeNumberBookmarkCaption = "officeNumber";

        internal const string LeaseContractOfficeSquareBookmarkCaption = "officeSquare";

        internal const string LeaseContractOfficeForPlacementBookmarkCaption = "officeForPlacement";

        internal const string LeaseContractOfficeOwnershipBookmarkCaption = "officeOwnership";

        internal const string LeaseContractOfficePriceBookmarkCaption = "officePrice";

        internal const string LeaseContractTotalPriceBookmarkCaption = "totalPrice";

        internal const string LeaseContractLandlordRequisitesBookmarkCaption = "landlordRequisites";

        internal const string LeaseContractLandlordRequisitesEndBookmarkCaption = "landlordRequisitesEnd";

        internal const string LeaseContractRenterRequisitesBookmarkCaption = "renterRequisites";

        internal const string LeaseContractRenterRequisitesEndBookmarkCaption = "renterRequisitesEnd";

        internal const string LeaseContractAct1BeginDateBookmarkCaption = "act1Date";

        internal const string LeaseContractGaranteeBookmarkCaption = "garantee";

        internal const string LeaseContractAdditinalDocumentHeaderBookmarkCaption = "additinalDocumentHeader";

        internal const string LeaseContractLandlordAdditinalDocumentRequisitesBookmarkCaption = "landlordAdditinalDocumentRequisites";

        internal const string LeaseContractLandlordAdditinalDocumentRequisitesEndBookmarkCaption = "landlordAdditinalDocumentRequisitesEnd";

        internal const string LeaseContractRenterAdditinalDocumentRequisitesBookmarkCaption = "renterAdditinalDocumentRequisites";

        internal const string LeaseContractRenterAdditinalDocumentRequisitesEndBookmarkCaption = "renterAdditinalDocumentRequisitesEnd";

        internal const string LeaseContractImagePlaceBookmarkCaption = "imagePlace";

        internal const string Act1LeaseContractNumberBookmarkCaption = "number2";

        internal const string Act1LeaseContractBeginDateBookmarkCaption = "beginDate2";

        internal const string Act1LeaseContractBeginDate2BookmarkCaption = "beginDate3";

        internal const string Act2LeaseContractEndDateBookmarkCaption = "leaseContractEndDate";
    }
}
