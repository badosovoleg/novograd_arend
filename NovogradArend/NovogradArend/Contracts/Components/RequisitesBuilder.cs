﻿using NovogradArend.Models;
using System;

namespace NovogradArend.Contracts
{
    public class RequisitesBuilder
    {
        public string HeaderOfRequisites { get; }

        public string HeaderOfShortRequisites { get; }

        public string EndOfRequisites { get; }

        public string EndOfShortRequisites { get; }

        public RequisitesBuilder(Entity entity)
        {
            HeaderOfRequisites =
                $"{entity.Short_Name}" + Environment.NewLine +
                (entity.Inn != null ? $"ИНН {entity.Inn}" + Environment.NewLine : "") +
                (entity.Ogrn != null ? $"ОРГН {entity.Ogrn}" + Environment.NewLine : "") +
                (entity.Organization_address != null ? $"{entity.Organization_address}" + Environment.NewLine : "") +
                (entity.Rs != null ? $"р/с {entity.Rs}" + Environment.NewLine : "") +
                (entity.Bik != null ? $"БИК {entity.Bik}" + Environment.NewLine : "") +
                (entity.Email != null ? $"E-mail: {entity.Email}" + Environment.NewLine : "") +
                (entity.Phone != null ? $"Тел. {entity.Phone}" + Environment.NewLine : "");

            EndOfRequisites =
                $"{entity.Base_On}" + Environment.NewLine +
                $"________________/{entity.Owner_Short_Name}/" + Environment.NewLine + "м.п";

            HeaderOfShortRequisites =
                $"{entity.Short_Name}" + Environment.NewLine +
                $"{entity.Base_On}" + Environment.NewLine;

            EndOfShortRequisites = $"________________/{entity.Owner_Short_Name}/" + Environment.NewLine + "м.п";
        }
    }
}