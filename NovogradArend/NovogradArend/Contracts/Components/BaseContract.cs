﻿using Microsoft.Office.Interop.Word;
using NovogradArend.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace NovogradArend.Contracts
{
    public abstract class BaseContract : IContract
    {
        public void SaveAsFinalWordDocument(string path)
        {
            Application application;
            Document document = null;
            object _missingObj = System.Reflection.Missing.Value;
            object trueObj = true;
            object falseObj = false;

            //создаем обьект приложения word
            application = new Application();
            // создаем путь к файлу

            var templatePathObj = Path.GetFullPath($"{Environment.CurrentDirectory}\\..\\..\\WordDocs\\{NameOfDOCXTemplate}.docx");

            if (!File.Exists(templatePathObj))
                return;

            application.Visible = true;

            // если вылетим не этом этапе, приложение останется открытым
            try
            {
                document = application.Documents.Add(templatePathObj, ref _missingObj, ref _missingObj, ref _missingObj);
            }
            catch (Exception error)
            {
                if (document != null)
                {
                    document.Close(ref falseObj, ref _missingObj, ref _missingObj);
                    application.Quit(ref _missingObj, ref _missingObj, ref _missingObj);
                }

                document = null;
                application = null;
                throw error;
            }

            document.SetTextByBookmarkName(GetParametresForReplace());
        }

        protected abstract string NameOfDOCXTemplate { get; }

        public abstract ContractTypes ContractType { get; }

        public abstract IEnumerable<IReplacableBookmark> GetParametresForReplace();
    }

   
}
