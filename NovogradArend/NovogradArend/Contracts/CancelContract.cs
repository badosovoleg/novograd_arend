﻿using NovogradArend.Models;
using System;
using System.Collections.Generic;

namespace NovogradArend.Contracts
{
    /// <summary>
    /// Документ отказа
    /// </summary>
    internal class CancelContract : BaseContract
    {
        public override ContractTypes ContractType => ContractTypes.CANCELCONTRACT;

        private string _nameOfDOCXTemplate = null;
        protected override string NameOfDOCXTemplate
        {
            get
            {
                if (_nameOfDOCXTemplate == null) _nameOfDOCXTemplate = "CancelContractTemplate";
                return _nameOfDOCXTemplate;
            }
        }

        public override IEnumerable<IReplacableBookmark> GetParametresForReplace()
        {
            throw new NotImplementedException();
        }
    }    
}