﻿using NovogradArend.Models;
using System;
using System.Collections.Generic;

namespace NovogradArend.Contracts
{
    /// <summary>
    /// Договор изменения арендной платы
    /// </summary>
    internal class ChangeArendPriceContract : BaseContract
    {
        public override ContractTypes ContractType => ContractTypes.CHANGEARENDPRICE;

        private string _nameOfDOCXTemplate = null;
        protected override string NameOfDOCXTemplate
        {
            get
            {
                if (_nameOfDOCXTemplate == null) _nameOfDOCXTemplate = "ChangeArendPriceContractTemplate";
                return _nameOfDOCXTemplate;
            }
        }

        public override IEnumerable<IReplacableBookmark> GetParametresForReplace()
        {
            throw new NotImplementedException();
        }
    }
}