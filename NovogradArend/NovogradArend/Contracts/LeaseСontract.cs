﻿using NovogradArend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NovogradArend.Contracts
{
    /// <summary>
    /// Договор аренды
    /// </summary>
    internal class LeaseСontractDocument : BaseContract
    {
        private string _number = null; 
        private string Number
        {
            get => _number ?? $"{Type.Code}-{Building.Code}/{Offices[0].Floor}/{Offices[0].Number}";
            set => _number = value;
        }

        public DateTime BeginDate { get; set; } = DateTime.MinValue;

        public DateTime EndDate
        {
            get
            {
                if (BeginDate == DateTime.MinValue)
                    return DateTime.MinValue;
                else
                {
                    var date = BeginDate;
                    date.AddMonths(11);
                    return date;
                }
            }
        }

        public Entity Landlord { get; set; }

        public Entity Renter { get; set; }

        public Office[] Offices { get; set; }

        public Building Building { get; set; }

        public ContractType Type { get; set; }

        public int CountLandlordDay { get; set; }

        public int CountRenterDay { get; set; }

        public Garantee Garantee { get; set; } = null;

        private string _nameOfDOCXTemplate = null;
        protected override string NameOfDOCXTemplate {
            get {
                if (_nameOfDOCXTemplate == null)
                    if (Offices.Length > 1) _nameOfDOCXTemplate = "LeaseContractTemplateForMany";
                    else _nameOfDOCXTemplate = "LeaseContractTemplate";
                return _nameOfDOCXTemplate;
            }
        }

        public string ForPlacement { get; set; }

        public int TotalPrice { get; set; }

        public int TotalSquare { get; set; }

        public DateTime Act1BeginDate { get; set; }

        public override ContractTypes ContractType => ContractTypes.ARENDCONTRACT;

        public override IEnumerable<IReplacableBookmark> GetParametresForReplace()
        {
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractNumberBookmarkCaption, Number);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractBeginDateBookmarkCaption, BeginDate.ToFormatDate());

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractLandlordNameBookmarkCaption, Landlord.Name);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractLandlordRepresentedBookmarkCaption, Landlord.Represented);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractLandlordAgreementBookmarkCaption, Landlord.Agreement);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractRenterNameBookmarkCaption, Renter.Name);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractRenterRepresentedBookmarkCaption, Renter.Represented);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractRenterAgreementBookmarkCaption, Renter.Agreement);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractBuildingAddressBookmarkCaption, Building.Address);

            string floor = Offices[0].Floor.ToString();

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractOfficeFloorBookmarkCaption, Offices[0].Floor.ToString());

            string numbersOnFloorPlan = Offices.First().Number_On_Floor_Plan.ToString();

            for (int i = 1; i < Offices.Length; i++)
            {
                numbersOnFloorPlan += $", {Offices[i].Number_On_Floor_Plan}";
            }

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractOfficeNumberOnFloorPlanBookmarkCaption, numbersOnFloorPlan);

            string numbers = Offices.First().Number.ToString();

            for (int i = 1; i < Offices.Length; i++)
            {
                numbers += $", {Offices[i].Number}";
            }

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractOfficeNumberBookmarkCaption, numbers);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractOfficeSquareBookmarkCaption, TotalSquare.ToString());

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractOfficeForPlacementBookmarkCaption, ForPlacement);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractOfficeOwnershipBookmarkCaption, Offices[0].Ownership);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractOfficePriceBookmarkCaption, RusNumber.ForrmatedString(TotalPrice / TotalSquare));

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractTotalPriceBookmarkCaption, RusNumber.ForrmatedString(TotalPrice));

            var landlordRequisites = new RequisitesBuilder(Landlord);
            var renterRequisites = new RequisitesBuilder(Renter);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractLandlordRequisitesBookmarkCaption, landlordRequisites.HeaderOfRequisites);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractLandlordRequisitesEndBookmarkCaption, landlordRequisites.EndOfRequisites);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractRenterRequisitesBookmarkCaption, renterRequisites.HeaderOfRequisites);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractRenterRequisitesEndBookmarkCaption, renterRequisites.EndOfRequisites);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractCountRenterDayBookmarkCaption, CountRenterDay.ToString());

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractCountRenterDay2BookmarkCaption, CountRenterDay.ToString());

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractCountLandlordDayBookmarkCaption2, CountLandlordDay.ToString());

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractCountLandlordDayBookmarkCaption, CountLandlordDay.ToString());

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractAct1BeginDateBookmarkCaption, Act1BeginDate.ToFormatDate());

            if (Garantee != null)
                yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractGaranteeBookmarkCaption, Garantee.Result);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractAdditinalDocumentHeaderBookmarkCaption, $@"Приложение №1
к Договору аренды № {Number}
 от {BeginDate.ToFormatDate()}
");
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractLandlordAdditinalDocumentRequisitesBookmarkCaption, landlordRequisites.HeaderOfShortRequisites);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractLandlordAdditinalDocumentRequisitesEndBookmarkCaption, landlordRequisites.EndOfShortRequisites);

            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractRenterAdditinalDocumentRequisitesBookmarkCaption, renterRequisites.HeaderOfShortRequisites);
            yield return new ReplacebelTextByBookmark(ContractBooknmarksConstant.LeaseContractRenterAdditinalDocumentRequisitesEndBookmarkCaption, renterRequisites.EndOfShortRequisites);

            yield return new ReplacebelImageByBookmark(ContractBooknmarksConstant.LeaseContractImagePlaceBookmarkCaption, $"{Environment.CurrentDirectory}\\..\\..\\Images\\test.jpg");
        }
    }
}