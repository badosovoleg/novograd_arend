﻿using NovogradArend.Models;
using System;
using System.Collections.Generic;

namespace NovogradArend.Contracts
{
    /// <summary>
    /// Договор коммунальное обслуживание
    /// </summary>
    internal class UtilityServiceContract : BaseContract
    {
        public override ContractTypes ContractType => ContractTypes.UTILITYSERVICE;

        private string _nameOfDOCXTemplate = null;
        protected override string NameOfDOCXTemplate
        {
            get
            {
                if (_nameOfDOCXTemplate == null) _nameOfDOCXTemplate = "UtilityServiceContractTemplate";
                return _nameOfDOCXTemplate;
            }
        }

        public override IEnumerable<IReplacableBookmark> GetParametresForReplace()
        {
            throw new NotImplementedException();
        }
    }
}