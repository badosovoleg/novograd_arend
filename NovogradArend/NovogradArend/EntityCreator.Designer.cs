﻿namespace NovogradArend
{
    partial class EntityCreator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.short_name = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.inn = new System.Windows.Forms.TextBox();
            this.ogrn = new System.Windows.Forms.TextBox();
            this.organization_address = new System.Windows.Forms.TextBox();
            this.rs = new System.Windows.Forms.TextBox();
            this.bik = new System.Windows.Forms.TextBox();
            this.email = new System.Windows.Forms.TextBox();
            this.phone = new System.Windows.Forms.TextBox();
            this.base_on = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.owner_short_name = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.addEntityButton = new System.Windows.Forms.Button();
            this.requisites = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.represented = new System.Windows.Forms.TextBox();
            this.agreement = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(178, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Название организации (юр. лица)";
            // 
            // name
            // 
            this.name.Location = new System.Drawing.Point(219, 19);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(311, 20);
            this.name.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 187);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Название (короткое)";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label17.Location = new System.Drawing.Point(24, 43);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(162, 26);
            this.label17.TabIndex = 28;
            this.label17.Text = "Общество с ограниченной \r\nответственностью «Вояж-ДВ»";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label3.Location = new System.Drawing.Point(24, 213);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 29;
            this.label3.Text = "ООО «Вояж-ДВ»";
            // 
            // short_name
            // 
            this.short_name.Location = new System.Drawing.Point(219, 175);
            this.short_name.Name = "short_name";
            this.short_name.Size = new System.Drawing.Size(311, 20);
            this.short_name.TabIndex = 30;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 237);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 31;
            this.label4.Text = "ИНН";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 263);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "ОГРН";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 289);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 33;
            this.label6.Text = "Адрес";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 315);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 13);
            this.label7.TabIndex = 34;
            this.label7.Text = "Расчетный счет";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(24, 341);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 13);
            this.label9.TabIndex = 36;
            this.label9.Text = "БИК";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(24, 367);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(104, 13);
            this.label10.TabIndex = 37;
            this.label10.Text = "Электронная почта";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(24, 393);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 13);
            this.label11.TabIndex = 38;
            this.label11.Text = "Телефон";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(24, 469);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 39;
            this.label8.Text = "Инициалы";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(24, 419);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 13);
            this.label12.TabIndex = 40;
            this.label12.Text = "На основании";
            // 
            // inn
            // 
            this.inn.Location = new System.Drawing.Point(219, 225);
            this.inn.Name = "inn";
            this.inn.Size = new System.Drawing.Size(311, 20);
            this.inn.TabIndex = 41;
            // 
            // ogrn
            // 
            this.ogrn.Location = new System.Drawing.Point(219, 251);
            this.ogrn.Name = "ogrn";
            this.ogrn.Size = new System.Drawing.Size(311, 20);
            this.ogrn.TabIndex = 42;
            // 
            // organization_address
            // 
            this.organization_address.Location = new System.Drawing.Point(218, 277);
            this.organization_address.Name = "organization_address";
            this.organization_address.Size = new System.Drawing.Size(311, 20);
            this.organization_address.TabIndex = 43;
            // 
            // rs
            // 
            this.rs.Location = new System.Drawing.Point(219, 303);
            this.rs.Name = "rs";
            this.rs.Size = new System.Drawing.Size(311, 20);
            this.rs.TabIndex = 44;
            // 
            // bik
            // 
            this.bik.Location = new System.Drawing.Point(219, 329);
            this.bik.Name = "bik";
            this.bik.Size = new System.Drawing.Size(311, 20);
            this.bik.TabIndex = 45;
            // 
            // email
            // 
            this.email.Location = new System.Drawing.Point(218, 355);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(311, 20);
            this.email.TabIndex = 46;
            // 
            // phone
            // 
            this.phone.Location = new System.Drawing.Point(218, 384);
            this.phone.Name = "phone";
            this.phone.Size = new System.Drawing.Size(311, 20);
            this.phone.TabIndex = 47;
            // 
            // base_on
            // 
            this.base_on.Location = new System.Drawing.Point(219, 410);
            this.base_on.Name = "base_on";
            this.base_on.Size = new System.Drawing.Size(311, 20);
            this.base_on.TabIndex = 48;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label13.Location = new System.Drawing.Point(24, 443);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(125, 13);
            this.label13.TabIndex = 49;
            this.label13.Text = "Генеральный директор";
            // 
            // owner_short_name
            // 
            this.owner_short_name.Location = new System.Drawing.Point(219, 457);
            this.owner_short_name.Name = "owner_short_name";
            this.owner_short_name.Size = new System.Drawing.Size(311, 20);
            this.owner_short_name.TabIndex = 50;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label14.Location = new System.Drawing.Point(24, 498);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(102, 13);
            this.label14.TabIndex = 51;
            this.label14.Text = "И. Л. Пупырышкин";
            // 
            // addEntityButton
            // 
            this.addEntityButton.Location = new System.Drawing.Point(27, 552);
            this.addEntityButton.Name = "addEntityButton";
            this.addEntityButton.Size = new System.Drawing.Size(932, 23);
            this.addEntityButton.TabIndex = 53;
            this.addEntityButton.Text = "button1";
            this.addEntityButton.UseVisualStyleBackColor = true;
            this.addEntityButton.Click += new System.EventHandler(this.addEntityButton_Click);
            // 
            // requisites
            // 
            this.requisites.Enabled = false;
            this.requisites.Location = new System.Drawing.Point(559, 18);
            this.requisites.Multiline = true;
            this.requisites.Name = "requisites";
            this.requisites.Size = new System.Drawing.Size(359, 363);
            this.requisites.TabIndex = 54;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(24, 85);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 13);
            this.label15.TabIndex = 55;
            this.label15.Text = "В лице";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(24, 127);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 13);
            this.label16.TabIndex = 56;
            this.label16.Text = "На основании";
            // 
            // represented
            // 
            this.represented.Location = new System.Drawing.Point(218, 85);
            this.represented.Name = "represented";
            this.represented.Size = new System.Drawing.Size(311, 20);
            this.represented.TabIndex = 57;
            // 
            // agreement
            // 
            this.agreement.Location = new System.Drawing.Point(218, 120);
            this.agreement.Name = "agreement";
            this.agreement.Size = new System.Drawing.Size(311, 20);
            this.agreement.TabIndex = 58;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label18.Location = new System.Drawing.Point(24, 107);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(124, 13);
            this.label18.TabIndex = 59;
            this.label18.Text = "в родительном падеже";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label19.Location = new System.Drawing.Point(24, 150);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(192, 13);
            this.label19.TabIndex = 60;
            this.label19.Text = "например, устава или доверенности";
            // 
            // EntityCreator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(971, 587);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.agreement);
            this.Controls.Add(this.represented);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.requisites);
            this.Controls.Add(this.addEntityButton);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.owner_short_name);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.base_on);
            this.Controls.Add(this.phone);
            this.Controls.Add(this.email);
            this.Controls.Add(this.bik);
            this.Controls.Add(this.rs);
            this.Controls.Add(this.organization_address);
            this.Controls.Add(this.ogrn);
            this.Controls.Add(this.inn);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.short_name);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.name);
            this.Controls.Add(this.label1);
            this.Name = "EntityCreator";
            this.Text = "Добавление организации (юр. лица)";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox short_name;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox inn;
        private System.Windows.Forms.TextBox ogrn;
        private System.Windows.Forms.TextBox organization_address;
        private System.Windows.Forms.TextBox rs;
        private System.Windows.Forms.TextBox bik;
        private System.Windows.Forms.TextBox email;
        private System.Windows.Forms.TextBox phone;
        private System.Windows.Forms.TextBox base_on;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox owner_short_name;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button addEntityButton;
        private System.Windows.Forms.TextBox requisites;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox represented;
        private System.Windows.Forms.TextBox agreement;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
    }
}