﻿using NovogradArend.Contracts;
using NovogradArend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace NovogradArend
{
    public partial class MainForm : Form
    {
        private SQLiteDatabaseContext DataBaseContext;

        public MainForm()
        {
            InitializeComponent();
            DataBaseContext = new SQLiteDatabaseContext();

            officesComboBox.DisplayMember = nameof(Office.Number);
            buildingsComboBox.DisplayMember = nameof(Building.Name);
            listBox1.DisplayMember = (nameof(Office.Number));

            renterComboBox.DisplayMember = nameof(Entity.Name);
            
            buildingsComboBox.SelectedIndexChanged += buildingsComboBox_SelectedIndexChanged;

            renterComboBox.DataSource = DataBaseContext.Entities.ToList();

            buildingsComboBox.DataSource = DataBaseContext.Buildings.ToList();

            CountRenterDayComboBox.SelectedValue = CountRenterDayComboBox.Items[2];
            CountLandlordDayComboBox.SelectedValue = CountLandlordDayComboBox.Items[1];
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

            buildingsComboBox.Update();
            buildingsComboBox.Refresh();
        }

        private void buildingsdataBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!int.TryParse(priceForOfficeTextBox.Text, out int totalPrice))
            {
                MessageBox.Show("В поле цены должно быть указано число за кв. м.!");
                return;
            }

            if (forPlacementTextBox.Text.Length == 0)
            {
                MessageBox.Show("В поле \"Для размещения\" должны быть указаны данные!");
            }

            var currentRenter = renterComboBox.SelectedItem as Entity;

            var offices = new List<Office>();
            for (var i = 0; i < listBox1.Items.Count; i++)
            {
                offices.Add(listBox1.Items[i] as Office);
            }
            var building = buildingsComboBox.SelectedItem as Building;

            var square = offices.Select(o => o.Square).Sum();

            var landlord = DataBaseContext.Entities.Single(en => en.Id == offices[0].Entity_Id);

            var contractType = DataBaseContext.ContractTypes.Single(t => t.Type == ContractTypes.ARENDCONTRACT);

            if (isLeaseContactCheckBox.Checked)
            {
                var leaseContract = new LeaseСontractDocument()
                {
                    BeginDate = beginContractDateTimePicker.Value,
                    Landlord = landlord,
                    Renter = currentRenter,
                    Offices = offices.ToArray(),
                    ForPlacement = forPlacementTextBox.Text,
                    TotalSquare = square,
                    TotalPrice = totalPrice * square,
                    Building = building,
                    Type = contractType,
                    Act1BeginDate = act1Date.Value,
                    Garantee = isGarantMailCheckBox.Checked 
                        ? new Garantee(garanteeNumber.Text, garanteeDate.Value.ToFormatDate(), building.Address, square.ToString()) 
                        : null,
                    CountLandlordDay = int.Parse(CountLandlordDayComboBox.Text),
                    CountRenterDay = int.Parse(CountRenterDayComboBox.Text),
                };

                leaseContract.SaveAsFinalWordDocument("");
            }
        }

        private void buildingsComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var building = buildingsComboBox.SelectedItem as Building;
            var targetOffices = DataBaseContext.Offices.Where(o => o.Building_Id == building.Id).ToList();
            officesComboBox.DataSource = targetOffices;
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void officesComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var office = officesComboBox.SelectedItem as Office;
            officeSquareLabel.Text = $"Площадь: {office.Square}";
            officeLandlordLabel.Text = $"Арендодатель: {DataBaseContext.Entities.Single(en => en.Id == office.Entity_Id).Name}";
        }

        private void isLeaseContactCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (isLeaseContactCheckBox.Checked)
            {
                var totalPrice = 0;

                for (int i = 0; i < officesComboBox.Items.Count; i++)
                {
                    totalPrice += ((Office)officesComboBox.Items[i]).Square;
                }

                SetPrice(totalPrice);
            }
        }

        private void priceForOfficeTextBox_TextChanged(object sender, EventArgs e)
        {
            if (int.TryParse(priceForOfficeTextBox.Text, out int price))
            {
                SetPrice(price);
                return;
            }
        }

        private void SetPrice(int price)
        {
            var totalSquare = 0;

            for (int i = 0; i < officesComboBox.Items.Count; i++)
            {
                totalSquare += ((Office)officesComboBox.Items[i]).Square;
            }

            totalPriceLabel.Text = $"Сумма: {price * totalSquare}";
        }

        private void addOfficeButton_Click(object sender, EventArgs e)
        {
            var targetOffice = officesComboBox.SelectedItem as Office;

            var isCanceled = false;

            if (listBox1.Items.Count > 0)
            {
                var a = listBox1.Items[0] as Office;
                
                if (isCanceled = a.Entity_Id != targetOffice.Entity_Id)
                {
                    MessageBox.Show("Оффисы должны принадлежать одному лицу!");
                }
                else if (isCanceled = a.Building_Id != targetOffice.Building_Id)
                {
                    MessageBox.Show("Оффисы должны находится в одном здании!");
                }
                else if (isCanceled = a.Floor != targetOffice.Floor)
                {
                    MessageBox.Show("Оффисы должны находится на одном этаже!");
                }
            }

            if (!isCanceled)
                listBox1.Items.Add(targetOffice);
        }

        private void removeSelectedOfficeButton_Click(object sender, EventArgs e)
        {
            var a = listBox1.SelectedItem;
            listBox1.Items.Remove(a);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var entityCreator = 
                new EntityCreator(new Action<Entity>(ent => DataBaseContext.Entities.Add(ent)));
            entityCreator.ShowDialog();
            DataBaseContext.SaveChanges();
        }

        private void юрЛицаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var entityCreator =
                new EntityCreator(new Action<Entity>(ent => DataBaseContext.Entities.Add(ent)));
            entityCreator.ShowDialog();
            DataBaseContext.SaveChanges();
        }
    }
}
