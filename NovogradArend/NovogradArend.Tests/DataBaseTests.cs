﻿using Microsoft.EntityFrameworkCore;
using NovogradArend.EF;
using NovogradArend.Model;
using NUnit.Framework;
using System.Linq;

namespace NovogradArend.Tests
{
    [TestFixture]
    class DataBaseTests
    {
        SQLiteDatabaseContext _context;

        Entity entity1;
        Entity entity2;

        LeaseContract leaseContract1;
        LeaseContract leaseContract2;

        LeaseContractOffice leaseContractOffice1;
        LeaseContractOffice leaseContractOffice2;

        [SetUp]
        public void SetUp()
        {
            ClearTables();

            entity2 = new Entity()
            {
                Name = "testEntity2Name",
                Agreement = "testEntity2Agreement",
                Represented = "testEntity2Represented",
                Requisites = "testEntity2Requisites",
                ShortRequisites = "testEntity2ShortRequisites",
                Email = "testEntity2Email",
                Address = "testEntity2Address",
                BaseOn = "testEntity2BaseOn",
                BIK = "testEntity2BIK",
                EntityTypeId = 0,
                Initials = "testEntity2Initials",
                INN = "testEntity2INN",
                OGRN = "testEntity2OGRN",
                Phone = "testEntity2Phone",
                RS = "testEntity2RS",
                ShortName = "testEntity2ShortName",
            };

            entity1 = new Entity()
            {
                Name = "testEntity1Name",
                Agreement = "testEntity1Agreement",
                Represented = "testEntity1Represented",
                Requisites = "testEntity1Requisites",
                ShortRequisites = "testEntity1ShortRequisites",
                Email = "testEntity1Email",
                Address = "testEntity1Address",
                BaseOn = "testEntity1BaseOn",
                BIK = "testEntity1BIK",
                EntityTypeId = 0,
                Initials = "testEntity1Initials",
                INN = "testEntity1INN",
                OGRN = "testEntity1OGRN",
                Phone = "testEntity1Phone",
                RS = "testEntity1RS",
                ShortName = "testEntity1ShortName",
            };

            leaseContract1 = new LeaseContract
            {
                Act1BeginDate = "test1Act1BeginDate",
                IsActual = true,
                BeginDate = "test1BeginDate",
                BuildingId = 1,
                CountLandLordDay = 15,
                CountRenterDay = 16,
                EndDate = "test1EndDate",
                ForPlacement = "test1ForPlacement",
                GaranteeDate = "test1GaranteeDate",
                GaranteeNumber = "test1GaranteeNumber",
                LandLordId = 1,
                Number = "test1Number",
                Price = 15,
                RenterId = 1,
                TotalPrice = 153,
                TotalSquare = 221,
            };

            leaseContract2 = new LeaseContract
            {
                Act1BeginDate = "test2Act1BeginDate",
                IsActual = true,
                BeginDate = "test2BeginDate",
                BuildingId = 1,
                CountLandLordDay = 15,
                CountRenterDay = 16,
                EndDate = "test2EndDate",
                ForPlacement = "test2ForPlacement",
                GaranteeDate = "test2GaranteeDate",
                GaranteeNumber = "test2GaranteeNumber",
                LandLordId = 1,
                Number = "test2Number",
                Price = 15,
                RenterId = 1,
                TotalPrice = 153,
                TotalSquare = 221,
            };

            leaseContractOffice1 = new LeaseContractOffice
            {
                LeaseContractId = 1,
                OfficeId = 1
            };

            leaseContractOffice2 = new LeaseContractOffice
            {
                LeaseContractId = 2,
                OfficeId = 2
            };
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            var optionsBuilder = new DbContextOptionsBuilder<SQLiteDatabaseContext>();
            optionsBuilder.UseSqlite(@"Data Source=..\..\novograd_arend2.db");
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            _context = new SQLiteDatabaseContext(optionsBuilder.Options);
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            ClearTables();
            _context.Dispose();
        }

        private void ClearTables()
        {
            var query = string.Empty;

            query = $"delete from {Model.ModelExtensions.GetTableNameForType(nameof(Entity))};";
            _context.Database.ExecuteSqlCommand(query);

            query = $"delete from {Model.ModelExtensions.GetTableNameForType(nameof(LeaseContract))};";
            _context.Database.ExecuteSqlCommand(query);

            query = $"delete from {Model.ModelExtensions.GetTableNameForType(nameof(UtilityServiceContract))};";
            _context.Database.ExecuteSqlCommand(query);

            query = $"delete from {Model.ModelExtensions.GetTableNameForType(nameof(Act1Contract))};";
            _context.Database.ExecuteSqlCommand(query);

            query = $"delete from {Model.ModelExtensions.GetTableNameForType(nameof(Act2Contract))};";
            _context.Database.ExecuteSqlCommand(query);

            query = $"delete from {Model.ModelExtensions.GetTableNameForType(nameof(CancelContract))};";
            _context.Database.ExecuteSqlCommand(query);

            query = $"delete from {Model.ModelExtensions.GetTableNameForType(nameof(ChangeArendPriceContract))};";
            _context.Database.ExecuteSqlCommand(query);

            query = $"delete from {Model.ModelExtensions.GetTableNameForType(nameof(ClaimContract))};";
            _context.Database.ExecuteSqlCommand(query);

            query = $"delete from {Model.ModelExtensions.GetTableNameForType(nameof(TerminationContract))};";
            _context.Database.ExecuteSqlCommand(query);

            query = $"delete from {Model.ModelExtensions.GetTableNameForType(nameof(EntityType))};";
            _context.Database.ExecuteSqlCommand(query);

            query = $"delete from {Model.ModelExtensions.GetTableNameForType(nameof(Building))};";
            _context.Database.ExecuteSqlCommand(query);

            query = $"delete from {Model.ModelExtensions.GetTableNameForType(nameof(Office))};";
            _context.Database.ExecuteSqlCommand(query);

            query = $"delete from {Model.ModelExtensions.GetTableNameForType(nameof(LeaseContractOffice))};";
            _context.Database.ExecuteSqlCommand(query);

            query = $"delete from {Model.ModelExtensions.GetTableNameForType(nameof(UtilityServiceContractOffice))};";
            _context.Database.ExecuteSqlCommand(query);
        }

        #region Entity

        [Test]
        public void AddEntityTest()
        {
            var result = _context.AddEntityAsync(entity1).Result;

            Assert.That(result, Is.EqualTo(1));
        }

        [Test]
        public void AddEntityManyTest()
        {
            var result1 = _context.AddEntityAsync(entity1).Result;
            var result2 = _context.AddEntityAsync(entity2).Result;

            Assert.That(result1, Is.EqualTo(1));
            Assert.That(result2, Is.EqualTo(1));
        }

        [Test]
        public void AddUpdateEntityTest()
        {
            var result1 = _context.AddEntityAsync(entity1).Result;

            Assert.That(result1, Is.EqualTo(1));

            var id = _context.GetEntitiesAsync().Result.Single(e => e.Name == entity1.Name).Id;

            entity2.Id = id;

            var result2 = _context.EditEntityAsync(entity2).Result;

            Assert.That(result2, Is.EqualTo(1));
        }

        [Test]
        public void AddUpdateEntityManyTest()
        {
            var result1 = _context.AddEntityAsync(entity1).Result;
            var result2 = _context.AddEntityAsync(entity2).Result;

            Assert.That(result1, Is.EqualTo(1));
            Assert.That(result2, Is.EqualTo(1));

            var id1 = _context.GetEntitiesAsync().Result.Single(e => e.Name == entity1.Name).Id;
            var id2 = _context.GetEntitiesAsync().Result.Single(e => e.Name == entity2.Name).Id;

            entity1.Id = id1;
            entity1.Name = $"NEW_{entity1.Name}";

            entity2.Id = id2;
            entity2.Name = $"NEW_{entity2.Name}";

            var result3 = _context.EditEntityAsync(entity1).Result;
            var result4 = _context.EditEntityAsync(entity2).Result;


            Assert.That(result3, Is.EqualTo(1));
            Assert.That(result4, Is.EqualTo(1));
        }

        [Test]
        public void AddUpdateEntityManySequenceTest()
        {
            var result1 = _context.AddEntityAsync(entity1).Result;
            Assert.That(result1, Is.EqualTo(1));

            entity1.Id = _context.GetEntitiesAsync().Result.Single(e => e.Name == entity1.Name).Id;
            entity1.Name = $"NEW_{entity1.Name}";

            var result3 = _context.EditEntityAsync(entity1).Result;
            Assert.That(result3, Is.EqualTo(1));

            var result2 = _context.AddEntityAsync(entity2).Result;
            Assert.That(result2, Is.EqualTo(1));

            entity2.Id = _context.GetEntitiesAsync().Result.Single(e => e.Name == entity2.Name).Id;
            entity2.Name = $"NEW_{entity2.Name}";

            var result4 = _context.EditEntityAsync(entity2).Result;
            Assert.That(result4, Is.EqualTo(1));
        }

        #endregion

        #region LeaseContract

        [Test]
        public void AddLeaseContractTest()
        {
            var result = _context.AddLeaseContractAsync(leaseContract1).Result;

            Assert.That(result, Is.EqualTo(1));
        }

        [Test]
        public void AddLeaseContractManyTest()
        {
            var result1 = _context.AddLeaseContractAsync(leaseContract1).Result;
            var result2 = _context.AddLeaseContractAsync(leaseContract2).Result;

            Assert.That(result1, Is.EqualTo(1));
            Assert.That(result2, Is.EqualTo(1));
        }

        [Test]
        public void AddUpdateLeaseContractTest()
        {
            var result1 = _context.AddLeaseContractAsync(leaseContract1).Result;

            Assert.That(result1, Is.EqualTo(1));

            var id = _context.GetLeaseContractsAsync().Result.Single(e => e.Number == leaseContract1.Number).Id;

            leaseContract2.Id = id;

            var result2 = _context.EditLeaseContractAsync(leaseContract2).Result;

            Assert.That(result2, Is.EqualTo(1));
        }

        [Test]
        public void AddUpdateLeaseContractManyTest()
        {
            var result1 = _context.AddLeaseContractAsync(leaseContract1).Result;
            var result2 = _context.AddLeaseContractAsync(leaseContract2).Result;

            Assert.That(result1, Is.EqualTo(1));
            Assert.That(result2, Is.EqualTo(1));

            var id1 = _context.GetLeaseContractsAsync().Result.Single(e => e.Number == leaseContract1.Number).Id;
            var id2 = _context.GetLeaseContractsAsync().Result.Single(e => e.Number == leaseContract2.Number).Id;

            leaseContract1.Id = id1;
            leaseContract1.Number = $"NEW_{leaseContract1.Number}";

            leaseContract2.Id = id2;
            leaseContract2.Number = $"NEW_{leaseContract2.Number}";

            var result3 = _context.EditLeaseContractAsync(leaseContract1).Result;
            var result4 = _context.EditLeaseContractAsync(leaseContract2).Result;


            Assert.That(result3, Is.EqualTo(1));
            Assert.That(result4, Is.EqualTo(1));
        }

        [Test]
        public void AddUpdateLeaseContractManySequenceTest()
        {
            var result1 = _context.AddLeaseContractAsync(leaseContract1).Result;
            Assert.That(result1, Is.EqualTo(1));

            leaseContract1.Id = _context.GetLeaseContractsAsync().Result.Single(e => e.Number == leaseContract1.Number).Id;
            leaseContract1.Number = $"NEW_{leaseContract1.Number}";

            var result3 = _context.EditLeaseContractAsync(leaseContract1).Result;
            Assert.That(result3, Is.EqualTo(1));

            var result2 = _context.AddLeaseContractAsync(leaseContract2).Result;
            Assert.That(result2, Is.EqualTo(1));

            leaseContract2.Id = _context.GetLeaseContractsAsync().Result.Single(e => e.Number == leaseContract2.Number).Id;
            leaseContract2.Number = $"NEW_{leaseContract2.Number}";

            var result4 = _context.EditLeaseContractAsync(leaseContract2).Result;
            Assert.That(result4, Is.EqualTo(1));
        }

        #endregion

        [Test]
        public void AddLeaseContractOfficeOfficeTest()
        {
            var result = _context.AddLeaseContractOfficeAsync(leaseContractOffice1).Result;

            Assert.That(result, Is.EqualTo(1));
        }

        [Test]
        public void AddLeaseContractOfficeManyTest()
        {
            var result1 = _context.AddLeaseContractOfficeAsync(leaseContractOffice1).Result;
            var result2 = _context.AddLeaseContractOfficeAsync(leaseContractOffice2).Result;

            Assert.That(result1, Is.EqualTo(1));
            Assert.That(result2, Is.EqualTo(1));
        }

        [Test]
        public void AddUpdateLeaseContractOfficeTest()
        {
            var result1 = _context.AddLeaseContractOfficeAsync(leaseContractOffice1).Result;

            Assert.That(result1, Is.EqualTo(1));

            var id = _context.GetLeaseContractOfficesAsync().Result.Single(e => e.LeaseContractId == leaseContractOffice1.LeaseContractId).Id;

            leaseContractOffice2.Id = id;

            var result2 = _context.RemoveLeaseContractOfficeAsync(leaseContractOffice2).Result;

            Assert.That(result2, Is.EqualTo(1));
        }

        [Test]
        public void AddUpdateLeaseContractOfficeManyTest()
        {
            var result1 = _context.AddLeaseContractOfficeAsync(leaseContractOffice1).Result;
            var result2 = _context.AddLeaseContractOfficeAsync(leaseContractOffice2).Result;

            Assert.That(result1, Is.EqualTo(1));
            Assert.That(result2, Is.EqualTo(1));

            var id1 = _context.GetLeaseContractOfficesAsync().Result.Single(e => e.LeaseContractId == leaseContractOffice1.LeaseContractId).Id;
            var id2 = _context.GetLeaseContractOfficesAsync().Result.Single(e => e.LeaseContractId == leaseContractOffice2.LeaseContractId).Id;

            leaseContractOffice1.Id = id1;
            leaseContractOffice1.OfficeId = 41;

            leaseContractOffice2.Id = id2;
            leaseContractOffice2.OfficeId = 412;

            var result3 = _context.RemoveLeaseContractOfficeAsync(leaseContractOffice1).Result;
            var result4 = _context.RemoveLeaseContractOfficeAsync(leaseContractOffice2).Result;


            Assert.That(result3, Is.EqualTo(1));
            Assert.That(result4, Is.EqualTo(1));
        }

        [Test]
        public void AddUpdateLeaseContractOfficeManySequenceTest()
        {
            var result1 = _context.AddLeaseContractOfficeAsync(leaseContractOffice1).Result;
            Assert.That(result1, Is.EqualTo(1));

            leaseContractOffice1.Id = _context.GetLeaseContractOfficesAsync().Result.Single(e => e.LeaseContractId == leaseContractOffice1.LeaseContractId).Id;
            leaseContractOffice1.OfficeId = 41;

            var result3 = _context.RemoveLeaseContractOfficeAsync(leaseContractOffice1).Result;
            Assert.That(result3, Is.EqualTo(1));

            var result2 = _context.AddLeaseContractOfficeAsync(leaseContractOffice2).Result;
            Assert.That(result2, Is.EqualTo(1));

            leaseContractOffice2.Id = _context.GetLeaseContractOfficesAsync().Result.Single(e => e.LeaseContractId == leaseContractOffice2.LeaseContractId).Id;
            leaseContractOffice2.OfficeId = 41;

            var result4 = _context.RemoveLeaseContractOfficeAsync(leaseContractOffice2).Result;
            Assert.That(result4, Is.EqualTo(1));
        }

    }
}