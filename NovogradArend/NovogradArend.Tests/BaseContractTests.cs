using NovogradArend.Desktop.Contracts;
using NovogradArend.Model;
using NUnit.Framework;
using System;
using System.Linq;

namespace NovogradArend.Tests
{
    public class BaseContractTests
    {
        Entity LandLord;
        Entity Renter;
        Building Building;
        Lease�ontractDocument Lease�ontract;
        Lease�ontractDocument Lease�ontractForMany;
        UtilityServiceContractDocument UtilityServiceContractDocument;
        Office Office;
        Office Office2;

        [SetUp]
        public void Setup()
        {
            LandLord = new Entity()
            {
                Id = 1,
                Name = "landlordName",
                Represented = "landlordRepresented",
                Agreement = "landlordAgreement",
                EntityTypeId = 1,
                Requisites = "landlordRequisites",
                INN = "landlordINN",
                OGRN = "landlordOGRN",
                RS = "landlordRS",
                BIK = "landlordBIK",
                Address = "landlordAddress",
                Email = "landlordEmail",
                Phone = "landlordPhone",
                ShortName = "landlordShortName",
                BaseOn = "landlordBaseOn",
                Initials = "landlordInitials",
                ShortRequisites = "landlordShortRequisites"
            };

            Renter = new Entity()
            {
                Id = 3,
                Name = "renterName",
                Represented = "renterRepresented",
                Agreement = "renterAgreement",
                EntityTypeId = 2,
                Requisites = "renterRequisites",
                INN = "renterINN",
                OGRN = "renterOGRN",
                RS = "renterRS",
                BIK = "renterBIK",
                Address = "renterAddress",
                Email = "renterEmail",
                Phone = "renterPhone",
                ShortName = "renterShortName",
                BaseOn = "renterBaseOn",
                Initials = "renterInitials",
                ShortRequisites = "renterShortRequisites"
            };

            Building = new Building()
            {
                Id = 1,
                Address = "buildingAddress",
                Name = "buildingName",
                Code = "BUBUN"
            };

            Office = new Office()
            {
                Id = 1,
                BuildingId = Building.Id,
                EntityId = LandLord.Id,
                Number = "1",
                Floor = 1,
                NumberOnFloorPlan = "1",
                �adastralNumber = "22",
                Ownership = "1officeOwnership",
                Price = 2000,
                Square = 32,
                ImagePath = "Novograd/11.jpg"
            };

            Office2 = new Office()
            {
                Id = 2,
                BuildingId = Building.Id,
                EntityId = LandLord.Id,
                Number = "2",
                Floor = 1,
                NumberOnFloorPlan = "2",
                �adastralNumber = "33",
                Ownership = "2officeOwnership",
                Price = 4599,
                Square = 12.5m,
                ImagePath = "Novograd/12.jpg"
            };

            Lease�ontract = new Lease�ontractDocument()
            {
                Number = "leaseContractNumber",
                BeginDate = DateTime.Today,
                EndDate = DateTime.Today,
                Landlord = LandLord,
                Renter = Renter,
                Building = Building,
                CountLandlordDay = 15,
                CountRenterDay = 45,
                Garantee = new Desktop.Contracts.Components.Garantee("gNumber", DateTime.Today, "gAddress", 15.34m),
                ForPlacement = "lcForPlacement",
                TotalPrice = 1515151515,
                TotalSquare = 1515151515,
                Act1ContractDocument = new Act1ContractDocument() { BeginDate = DateTime.Today },
                Offices = new[]
                {
                    Office
                }
            };


            UtilityServiceContractDocument = new UtilityServiceContractDocument()
            {
                Id = 1,
                Number = "UtilityServiceContractDocumentNumber",
                LeaseContract = Lease�ontract,
                BeginDate = DateTime.Today,
                ManagerCompany = LandLord,
                Renter = Renter,
                UtilityServiceOffices = new Office[] { Office },
                UtilityServicePrice = 1212,
                UtilityServiceTotalPrice = 221213231,
            };

            var offices = new[] { Office, Office2 };

            Lease�ontractForMany = new Lease�ontractDocument()
            {
                Number = "leaseContractNumber",
                BeginDate = DateTime.Today,
                EndDate = DateTime.Today,
                Landlord = LandLord,
                Renter = Renter,
                Building = Building,
                CountLandlordDay = 15,
                CountRenterDay = 45,
                Garantee = new Desktop.Contracts.Components.Garantee("gNumber", DateTime.Today, "gAddress", 123.5m),
                ForPlacement = "lcForPlacement",
                TotalPrice = offices.Last().Price,
                TotalSquare = offices.Select(o => o.Square).Sum(),
                Act1ContractDocument = new Act1ContractDocument() { BeginDate = DateTime.Today },
                Offices = offices
            };
        }

        [Test]
        public void CreateLeaseContractForOneOffice()
        {
            Lease�ontract.OpenAsFinalWordDocument();
        }

        [Test]
        public void CreateLeaseContractForManyOffices()
        {
            Lease�ontractForMany.OpenAsFinalWordDocument();
        }

        [Test]
        public void CreateAct1Contract()
        {
            var contract = new Act1ContractDocument()
            {
                Id = 1,
                Number = "act1ContractNumber",
                BeginDate = DateTime.Today,
                LeaseContract = Lease�ontract
            };

            contract.OpenAsFinalWordDocument();
        }

        [Test]
        public void CreateAct2Contract()
        {
            var contract = new Act2ContractDocument()
            {
                Id = 1,
                Number = "act2ContractNumber",
                BeginDate = DateTime.Today,
                LeaseContract = Lease�ontract
            };

            contract.OpenAsFinalWordDocument();
        }

        [Test]
        public void CreateChangeArendPriceContract()
        {
            var contract = new ChangeArendPriceContractDocument()
            {
                Id = 1,
                Number = "act2ContractNumber",
                BeginDate = DateTime.Today,
                LeaseContract = Lease�ontract,
                NewOfficePrice = 22,
                NewOfficeTotalPrice = 22121
            };

            contract.OpenAsFinalWordDocument();
        }

        [Test]
        public void CreateChangeArendPriceContractForMany()
        {
            var contract = new ChangeArendPriceContractDocument()
            {
                Id = 1,
                Number = "act2ContractNumber",
                BeginDate = DateTime.Today,
                LeaseContract = Lease�ontractForMany,
                NewOfficePrice = 22,
                NewOfficeTotalPrice = 22121
            };

            contract.OpenAsFinalWordDocument();
        }

        [Test]
        public void CreateTerminationContract()
        {
            var contract = new TerminationContractDocument()
            {
                Id = 1,
                BeginDate = DateTime.Today,
                LeaseContract = Lease�ontract
            };

            contract.OpenAsFinalWordDocument();
        }

        [Test]
        public void CreateCancelContract()
        {
            var contract = new CancelContractDocument()
            {
                Id = 1,
                BeginDate = DateTime.Today,
                MailNumber = "mailNumber",
                MailDate = DateTime.Today,
                Landlord = LandLord,
                Renter = Renter,
                Building = Building
            };

            contract.OpenAsFinalWordDocument();
        }

        [Test]
        public void CreateUtilityServiceContractForOneOffice()
        {
            UtilityServiceContractDocument.OpenAsFinalWordDocument();
        }

        [Test]
        public void CreateUtilityServiceContractForManyOffice()
        {
            var contract = new UtilityServiceContractDocument()
            {
                Id = 1,
                Number = "UtilityServiceContractDocumentNumber",
                LeaseContract = Lease�ontract,
                BeginDate = DateTime.Today,
                ManagerCompany = LandLord,
                Renter = Renter,
                UtilityServiceOffices = new Office[] { Office, Office2 },
                UtilityServicePrice = 1212,
                UtilityServiceTotalPrice = 221213231,
            };

            contract.OpenAsFinalWordDocument();
        }

        [Test]
        public void CreateClaimContract()
        {
            var contract = new ClaimContractDocument()
            {
                Id = 1,
                Number = "ClaimContractDocumentNumber",
                BeginDate = DateTime.Today,
                LeaseContract = Lease�ontract,
                UtilityServiceContract = UtilityServiceContractDocument,
                ArendDebt = 143.3m,
                UtilityServiceDebt = 22.6m
            };

            contract.OpenAsFinalWordDocument();
        }
    }
}