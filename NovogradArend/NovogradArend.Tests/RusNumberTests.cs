﻿using NovogradArend.Desktop.Contracts.Components;
using NUnit.Framework;

namespace NovogradArend.Tests
{
    public class RusNumberTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void BasicTestForUintNumbers()
        {
            decimal first = 22;
            decimal second = 1000000;

            var firstResult = RusNumber.BuildString(first);
            var secondReslt = RusNumber.BuildString(second);

            Assert.That(firstResult, Is.EqualTo("22 (двадцать два) рубля 0 коп."));
            Assert.That(secondReslt, Is.EqualTo("1000000 (один миллион) рублей 0 коп."));
        }

        [Test]
        public void BasicTestForDecimalNumbers()
        {
            decimal first = 22.3m;
            decimal second = 143.6m;

            var firstResult = RusNumber.BuildString(first);
            var secondReslt = RusNumber.BuildString(second);

            Assert.That(firstResult, Is.EqualTo("22 (двадцать два) рубля 30 коп."));
            Assert.That(secondReslt, Is.EqualTo("143 (сто сорок три) рубля 60 коп."));
        }
    }
}